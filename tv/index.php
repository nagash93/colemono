

<?php
/**
* @author :
*/


require('../wp-blog-header.php');
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>Colemono TV</title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>		<?php flush(); ?>

	<script type="text/javascript">
				var n = jQuery.noConflict();
				n(document).ready(function () {
				
				    n('#df').click(function () {
				        n('#tv').width("665px").height("378px");
				    });
				    n('#am').click(function () {
				        n('#tv').width("1015px").height("571px");
				    });
				});
	</script>

	<style>
	#tv{float: left;}
	#chat{float: right;}
	#cont{width: 625px; padding: 20px;overflow: auto; float: left;	}
	#social{width: 300px;float: left;}
	#cont a:nth-child(2) {float: left; }
	#menu{font-family: 'gotham_boldregular'; height: 50px; font-size: 25px;color: #FFB400;margin-top: 10px; }
	#menu div{float: left; }
	#menu img{float: left;margin: 0 10px 0 10px;}	
	#df{color: #fff; }
	#twitch{width: 99px; margin:10px auto 0 auto;}
	#twitch img{width: 99px;}
	#background1{background-image: url(images/fondo.jpg); }
	#wrap-container{background: none;}
	#stream{float: left;width: 700px;}
	#chat{float: left;width: 300px;}
	#chattwitch{float: left;margin-top: 20px;}

	</style>

</head>
<body <?php body_class();?>>

	<!-- Full Screen Background -->
	<div class="full-screen-bg"><div class="screen-inner"></div></div>	
	<!-- HEADER -->
	<div class="header-nav">
					<div id="headernav" class="container">
						<?php wp_nav_menu( array('theme_location' => 'HeaderNav', 'menu_class'  => 'clearfix','fallback_cb' => 'van_nav_alert') ); ?>
					</div>	
				</div>
	<header id="main-header">	
		<div id="top-bar">
			<div id="wlogo" class="container clearfix">
				<?php van_logo();?>	
				<div id="banner-top"></div>			
				<?php if ( van_get_option("header_social") ): ?>
				<?php if (is_home()){ echo '';}?>
					<div id="header-social">						
						<?php van_social_networks(); ?>
					</div><!-- #header-social -->						
				<?php endif; ?>
			</div><!-- .container -->
		</div><!-- #top-bar -->

	</header>


		
	<nav>		
		<div id="main-nav-wrap" class="container content clearfix <?php echo !van_get_option("sticky_nav") ? 'disabled-sticky' : ''; ?>" >
			<nav id="main-navigation" role="navigation">
				<div class="mobile-nav">
					<?php van_menu_select(array( 'menu_name' => 'PrimaryNav', 'id' => 'PrimaryNav' )); ?>
				</div>				
				<div class="main-nav">
					<?php wp_nav_menu( array('theme_location' => 'PrimaryNav', 'menu_class'  => 'clearfix','fallback_cb' => 'van_nav_alert') ); ?>
				</div>
			</nav><!-- #main-navigation -->
			<?php if ( van_get_option("nav_search") ): ?>
				<div id="header-search">
					<form method="get" class="searchform clearfix" action="<?php echo esc_url( home_url() ); ?>/" role="search">
						<a href="#" class="search-icn icon icon-search"></a>
						<input type="text" name="s" placeholder="<?php _e('Type and hit enter to search...','van') ?>">
					</form>            	
				</div><!-- #header-search -->					
			<?php endif; ?>
		</div><!-- #main-nav-wrap -->
		
	</nav><!-- #main-header -->
	<!-- MAIN -->
			
	 
<div id="background1">	
<div id="wrap-container">	
<div id="main-wrap" style="width:100%;padding:0;margin:0;max-width:1015px;"class="container <?php echo van_sidebar_layout(); ?>">

<div id="menu">
	<div id="txt">#PREMIOSCOLEMONO</div>
	<img src="images/14.png">
	<div id="df">EN VIVO Y EN DIRECTO , 31 DE ENERO 20:30hrs.</div>
	
	
	

</div>

<div id="stream">
		<iframe id="tv"style="float:left;"src="http://www.twitch.tv/colemonotv/embed" frameborder="0" scrolling="no" height="382" width="679"></iframe>
		<a href="http://facebook.com/colemono"  target="_blank"><img src="images/hashtag.png"></a>
</div>
<div id="chat">
<a class="twitter-timeline" href="https://twitter.com/hashtag/PremiosColemono" data-widget-id="561352352318648320">Tweets sobre #PremiosColemono</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<div id="twitch"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitch-col.png"></div>
</div>


<div id="cont" >

	
	<div id="social">
		<a href="http://colemono.com/donaciones/" target="_blank"><img src="images/donacion2.png"></a>
		<a href="http://facebook.com/colemono"  target="_blank"><img src="images/fb.png"></a>
		<a href="http://twitter.com/cole_mono"  target="_blank"><img src="images/twit.png"></a>
	</div>

	<a href="http://mangacorta.cl/shops/colemono" target="_blank"><img src="images/poleras.jpg"></a>



</div>
<div id="chattwitch">

<iframe  src="http://www.twitch.tv/colemonotv/chat?popout=" frameborder="0" scrolling="no" height="400" width="350"></iframe>


</div>


<?php get_footer(); ?>