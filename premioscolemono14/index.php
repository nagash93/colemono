<?php

/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */



/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */





/** Loads the WordPress Environment and Template */

require('../wp-blog-header.php');


get_header(premios14); 


$van_page_type = van_page_type(); 
?>



	


	

		<!-- BEGIN MAIN -->
			
	<div id="main-content" style="width: 100%; background:#FFFFFF; text-decoration:none;" >

		<div id="sec1">
			<div id="fecha">
				<p>31 DE ENERO</p>
				<p>TRANSMISIÓN ESPECIAL</p>
				<p>VÍA<p/>	
				<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitch-superior.png" alt="twitch" >
			</div>

			<div id="votaaqui">
				<a href="http://colemono.com/premioscolemono14/votacion" title="Colemono" >
							<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/votar.png" alt="Premios Colemono" >
						</a>
			</div>


		</div>		
		<div id="sec2">
			<div id="comparte">
					<p>COMPARTE Y COMENTA</p>
					<p>EN REDES SOCIALES</p>
					<p>#PremiosColemono</p>
			</div>
			<div id="redes">

				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://colemono.com/premioscolemono14/" data-via="Cole_mono" data-hashtags="PremiosColemono">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitter.png">
				</a>
				<a href="http://www.facebook.com/sharer.php?u=http://colemono.com/premioscolemono14/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/fb.png">
				</a>


			</div>

			<div id="texto">

				<p>	Por 2da vez consecutiva tenemos la premiación más esperada por el público que visita nuestro portal, los Premios Colemono
					en su edición 2014 (en 2015). Que en esta ocasión llega junto a un buen puñado de buenos juegos que aparecieron durante el año
					y con todo el ánimo que nos caracteriza para desarrollar un show que guste a gran parte de la comunidad, grandes colaboradores y
					toda la buena onda que ponemos de nuestra parte.
				</p>
				<p>
					Para este año contamos con menos categorías, todo con la intención de poder hacer un show más movido y digerible a nuestro 
					público votante... como también poder invertir buen tiempo de nuestro show en vivo en otras actividades que iremos dando a conocer
					con el pasar de los días.
				</p>
				<p>
					Para todos aquellos que gustan de nuestro programa #AfterMono, tendrán el placer de poder ver la premiación completamente
					en vivo, junto al estilo con el cual hemos cautivado a muchos de ustedes. Todo esto vía nuestro canal de Twitch, con el que pensamos 
					llegar a todo Chile y el mundo.
				</p>
				<p>
					No esperes más y vota, que este año se viene increíble el certamen  :D
				</p>

			</div>	


		</div>
		
		<div id="sec3">
			<div id="present">
				<p>PRESENTA</p>
			<p>Y PRODUCE</p>

			</div>
			<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/logo-colemono.png">
			<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/egames.png">	


		</div>

		<div id="sec4">

			<div id="colaboran">
				<p>COLABORAN</p>		

			</div>
			<a href="http://blog.latam.playstation.com/"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/play.png"></a>	
			<a href="http://twitch.tv"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitch-col.png"></a>
			<!--
			<img src="<?php #echo get_template_directory_uri(); ?>/images/premios14/movistar.png">
			<img src="<?php #echo get_template_directory_uri(); ?>/images/premios14/nintendo.png">	
			
			<img src="<?php #echo get_template_directory_uri(); ?>/images/premios14/festigame.png">	
			-->					
			<a href="http://www.nintendo.com"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/ninty.png"></a>
			
			<a href="http://www.razerzone.com/es-es/store"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/razer.png"></a>
			<a href="http://www.xbox.cl"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/xbox.png"></a>
			<a href="http://www.ea.com/"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/ea.png"></a><br/>
			<a href="http://8bitdo.com/"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/8bit.png"></a>
			<a href="http://klu.cl/"  target="_blank"><img id="klu"src="<?php echo get_template_directory_uri(); ?>/images/premios14/klu.png"></a>
			<a href="http://mangacorta.cl/"  target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/premios14/manga.png"></a>

		</div>

		<div id="sec5">

					<div id="not">NOTICIAS</div>


				<div id="Noticias">
						
				
							  <?php 
         
             query_posts("cat=3784&"); //ID de la categoria 
				            if (have_posts()) : while (have_posts()) : the_post() ?>         
				      
				         <div id="post">
				                     
				      <?php get_template_part( 'partials/content', get_post_format() );  ?>
				         </div>
				      
				         <?php  endwhile; endif; ?>







					</div>








		</div>
		
				
				
			

		</div>

		<!-- END MAIN -->
</div>


<?php get_footer(); ?>