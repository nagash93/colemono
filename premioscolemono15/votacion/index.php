<?php

/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */
/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */


/** Loads the WordPress Environment and Template */

require('../../wp-blog-header.php');



?>


<?php
/**
* 
* The template for displaying the header.
* 
* @author : VanThemes ( http://www.vanthemes.com )
* 
*/
?>





<?php
global $royal_options;
foreach ($royal_options as $value) {
if (isset($value['id']) && get_option( $value['id'] ) === FALSE && isset($value['std'])) {
$$value['id'] = $value['std'];
}
elseif (isset($value['id'])) { $$value['id'] = get_option( $value['id'] ); }
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
<title><?php custom_titles(); ?></title>
<?php custom_description(); ?>
<?php custom_keywords(); ?>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if (get_option('op_custom_header_font') == '') { ?>
<link rel="stylesheet" id="my-font" href="//fonts.googleapis.com/css?family=<?php echo get_option('op_header_font'); ?>:400,600,700" type="text/css" media="all" />
<?php } else { ?>
<link rel="stylesheet" id="custom-font" href="//fonts.googleapis.com/css?family=<?php echo get_option('op_custom_header_font'); ?>:400,600,700" type="text/css" media="all" />
<?php } ?>	
<link rel="stylesheet" id="text-font" href="//fonts.googleapis.com/css?family=<?php echo get_option('op_text_font'); ?>:400,600,700" type="text/css" media="all" />


	
<?php wp_head(); ?>

<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<style type="text/css">
		a{text-decoration: none;}
		#mega_main_menu.main-menu > .menu_holder > .mmm_fullwidth_container {
    background: #ff005f none repeat scroll 0 0;}
 @media all and (max-width: 1024px) {

 		#main_content{overflow:visible; }
 	}
   

	</style>	
	<link rel="stylesheet" href="css/foundation.min.css">
	<link rel="stylesheet" href="css/premios14.css">  	  	
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
</head>

<?php $class='';
if ( is_single() || is_category() ) { 
    $category = get_the_category();
    $parent = $category[0]->category_parent;
    $class = get_cat_name($parent); 
}?>
<?php global $author_id; ?>

<body <?php body_class($class); ?>>

<div id="all_content" class="<?php if (get_option('op_theme_full') == 'on') { ?> boxed_width <?php } ?> <?php if (get_option('op_fixed_sidebars') == 'false') { ?>  fixed_sidebar <?php } ?> ">


<?php if (get_option('op_top_menu_position') == 'Top') { ?>
<?php if (get_option('op_header_top_menu') == 'on') { ?>
<?php get_template_part('includes/header_top_menu'); ?>
<?php } ?>	
<?php } ?>


<?php if (get_option('op_full_width_banner') == 'on') { ?>
<?php get_template_part('includes/full_width_banner'); ?>
<?php } ?>	

<?php if (get_option('op_header') == 'on') { ?>
<div id="header">
<div class="inner">

<?php if (get_option('op_header_layout') == 'Logo Center') { ?>

	<div id="title_box_center">

	<?php if (get_option('op_logo_on') == 'on') { ?>
	    <a href="http://colemono.com/premioscolemono15">
		    
		    <img src="http://colemono.com/premioscolemono15/votacion/img/logo-premios.png" alt="Logo" id="logo"/>
	    </a>
	 <?php } else { ?>
		<a class="site_title" href="<?php echo home_url() ?>/" title="<?php bloginfo('name'); ?>" rel="Home"><h1><?php bloginfo('name'); ?></h1></a>
	<?php } ?>	
	
    </div>
	
	<?php } else { ?>
	
	<div id="title_box">
	<?php if (get_option('op_logo_on') == 'on') { ?>
	    <a href="http://colemono.com/premioscolemono15">
		    <?php $logo = (get_option('op_logo') <> '') ? get_option('op_logo') : get_template_directory_uri() . '/images/logo_main.png'; ?>
		    <img src="http://colemono.com/premioscolemono15/votacion/img/logo-premios.png" alt="Logo" id="logo"/>
	    </a>
	 <?php } else { ?>
		<a class="site_title" href="<?php echo home_url() ?>/" title="<?php bloginfo('name'); ?>" rel="Home"><h1><?php bloginfo('name'); ?></h1></a>
	<?php } ?>	
    </div>
	
    <?php if (get_option('op_banner_header') == 'on') { ?>
		<?php if (get_option('op_banner_size') == '468x60px') { ?>
		<div id="banner-header">
		<?php } else { ?>
		<div id="banner_header_728">
		<?php } ?>	
		
		
		<?php if (get_option('op_banner_rotator') == 'on') { ?>
        <?php get_template_part('includes/banner_rotator'); ?>
        <?php } else { ?>
		
		<?php $header_banner = get_option("op_banner_header_code"); ?>
		<?php echo stripslashes($header_banner); ?>
		
		<?php } ?>
		
		</div>
	<?php } ?>
	
<?php } ?>		
	
</div>	
</div>
<?php } ?>	
<div class="clear"></div>

<div id="menu_box">
    <?php if ( function_exists( 'wp_nav_menu' ) ){
		wp_nav_menu( array( 'theme_location' => 'main-menu', 'container_id' => 'mainMenu', 'container_class' => 'ddsmoothmenu', 'fallback_cb'=>'primarymenu') );
		} else { primarymenu();
    } ?>
</div>





<?php if (get_option('op_top_menu_position') == 'Bottom') { ?>
<?php if (get_option('op_header_top_menu') == 'on') { ?>
<?php get_template_part('includes/header_top_menu'); ?>
<?php } ?>	
<?php } ?>	

<div class="clear"></div>





<div class="clear"></div>
<div class="inner_woo">
	


<?php 
$cat_id = get_query_var('cat');
$cat_data = get_option("category_$cat_id");
?>	

<?php 
$categories = get_the_category();
$category_id = $categories[0]->cat_ID;
$posts_found = get_option('op_posts_found');

$taxonomy = 'category';
$term_obj = get_term( $category_id, $taxonomy );
$posts_count = '<div class="cat_post_count">' . $term_obj->count  .' '. $posts_found .'</div>'; 
?>
<div id="colabar">Premiación Enero 2016 <div>colaboran</div></div>
<div id="main_content" class="<?php if (isset($cat_data['extra2'])){ echo $cat_data['extra2'];} ?>"> 

	<div id="vslider">

		<section class="slider">
						<div id="featuredContent">		        		
			        		
							<div class="nomina" id="nintendo">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565ce41fe4b00f0329a3c0b2"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey tool</div></a>
			        		</div>

			        		<div class="nomina" id="playstation">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c901ce4b00f0329a3bfc0"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">image polls</div></a>
								</div>

							<div class="nomina" id="xbox">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c903ce4b00f0329a3bfc2"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">web polls</div></a>
								</div>

							<div class="nomina" id="pc">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c9065e4b00f0329a3bfc3"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>

			        		</div>

			        		<div class="nomina" id="portatiles">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c909ce4b00f0329a3bfc4"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online surveys</div></a>


			        		</div>


			        		<div class="nomina" id="multi">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c911ae4b00f0329a3bfc7"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">image polls</div></a>
			        		</div>

			        		<div class="nomina" id="personaje">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c915fe4b00f0329a3bfc8"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">free polls</div></a>

			        		</div>

			        		<div class="nomina" id="fk">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c9185e4b00f0329a3bfca"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">free polls</div></a>

			        		</div>

			        		<div class="nomina" id="remastered">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c91b7e4b00f0329a3bfcb"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">feedback surveys</div></a>
			        		</div>

			        		<div class="nomina" id="mundoabierto">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c91ffe4b00f0329a3bfd0"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey solutions</div></a>

			        		</div>

			        		<div class="nomina" id="deporte">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c9238e4b00f0329a3bfd1"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys</div></a>
			        		</div>

							<div class="nomina" id="carreras">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c9259e4b00f0329a3bfd2"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">panel management</div></a>
								</div>
							<div class="nomina" id="peleas">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c9291e4b00f0329a3bfd3"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">customer survey</div></a>

			        		</div>
			        		
			        		<div class="nomina" id="rpg">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565ce42ce4b00f0329a3c0b3"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls</div></a>
								</div>
			        		

			        		<div class="nomina" id="fps">
								<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c92f9e4b00f0329a3bfda"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls & surveys</div></a>

			        		</div>

			        		<div class="nomina" id="indie">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565def91e4b00f0329a3c42d"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey service</div></a>
			        		</div>

			        		
			        		<div class="nomina" id="polemica">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565c937ae4b00f0329a3bfde"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online survey</div></a>

			        		</div>

			        		<div class="nomina" id="mejorjuego">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=565cb1cee4b00f0329a3c039"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online polls</div></a>

			        		</div>

			        		

			        		


			               		
						</div>
				
			</section>


 </div>

 <div id="sec2" style="margin-top:80px;">
			<div id="comparte">
					<p>COMPARTE Y COMENTA</p>
					<p>EN REDES SOCIALES</p>
					<p>#PremiosColemono</p>
			</div>
			<div id="redes">

				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://colemono.com/premioscolemono15/" data-via="Cole_mono" data-hashtags="PremiosColemono">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitter.png">
				</a>
				<a href="http://www.facebook.com/sharer.php?u=http://colemono.com/premioscolemono15/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/fb.png">
				</a>


			</div>

</div>

<div class="clear"></div>

	<script src="js/foundation.min.js"></script>
    <script src="js/app.html"></script>
	<script type="text/javascript">

    $( document ).ready(function() {
		 $('#featuredContent').orbit({ 
        timer: false,
        fluid: '16x9' 
      });
	});
    </script>
	


<?php get_footer(); ?>