<?php

/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */
/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */


/** Loads the WordPress Environment and Template */

require('../wp-blog-header.php');



?>


<?php
/**
* 
* The template for displaying the header.
* 
* @author : VanThemes ( http://www.vanthemes.com )
* 
*/
?>





<?php
global $royal_options;
foreach ($royal_options as $value) {
if (isset($value['id']) && get_option( $value['id'] ) === FALSE && isset($value['std'])) {
$$value['id'] = $value['std'];
}
elseif (isset($value['id'])) { $$value['id'] = get_option( $value['id'] ); }
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
<title>Colemono | Premios Colemono</title>
<?php custom_description(); ?>
<?php custom_keywords(); ?>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if (get_option('op_custom_header_font') == '') { ?>
<link rel="stylesheet" id="my-font" href="//fonts.googleapis.com/css?family=<?php echo get_option('op_header_font'); ?>:400,600,700" type="text/css" media="all" />
<?php } else { ?>
<link rel="stylesheet" id="custom-font" href="//fonts.googleapis.com/css?family=<?php echo get_option('op_custom_header_font'); ?>:400,600,700" type="text/css" media="all" />
<?php } ?>	
<link rel="stylesheet" id="text-font" href="//fonts.googleapis.com/css?family=<?php echo get_option('op_text_font'); ?>:400,600,700" type="text/css" media="all" />


	
<?php wp_head(); ?>


<style type="text/css">
		a{text-decoration: none;}
				#mega_main_menu.main-menu > .menu_holder > .mmm_fullwidth_container {
    background: #ff005f none repeat scroll 0 0;
}
	</style>	
	<link rel="stylesheet" href="css/foundation.min.css">
	<link rel="stylesheet" href="votacion/css/premios14.css">  	  	
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
</head>

<?php $class='';
if ( is_single() || is_category() ) { 
    $category = get_the_category();
    $parent = $category[0]->category_parent;
    $class = get_cat_name($parent); 
}?>
<?php global $author_id; ?>

<body <?php body_class($class); ?>>

<div id="all_content" class="<?php if (get_option('op_theme_full') == 'on') { ?> boxed_width <?php } ?> <?php if (get_option('op_fixed_sidebars') == 'false') { ?>  fixed_sidebar <?php } ?> ">


<?php if (get_option('op_top_menu_position') == 'Top') { ?>
<?php if (get_option('op_header_top_menu') == 'on') { ?>
<?php get_template_part('includes/header_top_menu'); ?>
<?php } ?>	
<?php } ?>


<?php if (get_option('op_full_width_banner') == 'on') { ?>
<?php get_template_part('includes/full_width_banner'); ?>
<?php } ?>	

<?php if (get_option('op_header') == 'on') { ?>
<div id="header">
<div class="inner">

<?php if (get_option('op_header_layout') == 'Logo Center') { ?>

	<div id="title_box_center">

	<?php if (get_option('op_logo_on') == 'on') { ?>
	    <a href="<?php echo home_url() ?>">
		    <?php $logo = (get_option('op_logo') <> '') ? get_option('op_logo') : get_template_directory_uri() . '/images/logo_main.png'; ?>
		    <img src="<?php echo $logo; ?>" alt="Logo" id="logo"/>
	    </a>
	 <?php } else { ?>
		<a class="site_title" href="<?php echo home_url() ?>/" title="<?php bloginfo('name'); ?>" rel="Home"><h1><?php bloginfo('name'); ?></h1></a>
	<?php } ?>	
	
    </div>
	
	<?php } else { ?>
	
	<div id="title_box">
	<?php if (get_option('op_logo_on') == 'on') { ?>
	    <a href="<?php echo home_url() ?>">
		    <?php $logo = (get_option('op_logo') <> '') ? get_option('op_logo') : get_template_directory_uri() . '/images/logo_main.png'; ?>
		    <img src="votacion/img/logo-premios.png" alt="Logo" id="logo"/>
	    </a>
	 <?php } else { ?>
		<a class="site_title" href="<?php echo home_url() ?>/" title="<?php bloginfo('name'); ?>" rel="Home"><h1><?php bloginfo('name'); ?></h1></a>
	<?php } ?>	
    </div>
	
    <?php if (get_option('op_banner_header') == 'on') { ?>
		<?php if (get_option('op_banner_size') == '468x60px') { ?>
		<div id="banner-header">
		<?php } else { ?>
		<div id="banner_header_728">
		<?php } ?>	
		
		
		<?php if (get_option('op_banner_rotator') == 'on') { ?>
        <?php get_template_part('includes/banner_rotator'); ?>
        <?php } else { ?>
		
		<?php $header_banner = get_option("op_banner_header_code"); ?>
		<?php echo stripslashes($header_banner); ?>
		
		<?php } ?>
		
		</div>
	<?php } ?>
	
<?php } ?>		
	
</div>	
</div>
<?php } ?>	
<div class="clear"></div>

<div id="menu_box">
    <?php if ( function_exists( 'wp_nav_menu' ) ){
		wp_nav_menu( array( 'theme_location' => 'main-menu', 'container_id' => 'mainMenu', 'container_class' => 'ddsmoothmenu', 'fallback_cb'=>'primarymenu') );
		} else { primarymenu();
    } ?>
</div>





<?php if (get_option('op_top_menu_position') == 'Bottom') { ?>
<?php if (get_option('op_header_top_menu') == 'on') { ?>
<?php get_template_part('includes/header_top_menu'); ?>
<?php } ?>	
<?php } ?>	

<div class="clear"></div>





<div class="clear"></div>
<div class="inner_woo">


	


	

		<!-- BEGIN MAIN -->
			
	<div id="main-content" style="will-change: 99%;max-width:1140px; background:#FFFFFF; text-decoration:none;padding:20px;" >

			<img style="width:100%;"src="votacion/img/destacado.jpg" alt="Premios Colemono" >


	<div style="padding:20px;">
		<div id="sec1">
			<div id="fecha">
				<p>Principios 2016</p>
				<p>TRANSMISIÓN ESPECIAL</p>
				<p>VÍA<p/>	
				<img src="votacion/img/youtu.png" alt="twitch" >
			</div>

			<div id="votaaqui">
				<a href="http://colemono.com/premioscolemono15/votacion" title="Colemono" >
							<img src="votacion/img/vota.png" alt="Premios Colemono" >
						</a>
			</div>


		</div>		
		<div id="sec2">
			<div id="comparte">
					<p>COMPARTE Y COMENTA</p>
					<p>EN REDES SOCIALES</p>
					<p>#PremiosColemono</p>
			</div>
			<div id="redes">

				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://colemono.com/premioscolemono15/" data-via="Cole_mono" data-hashtags="PremiosColemono">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitter.png">
				</a>
				<a href="http://www.facebook.com/sharer.php?u=http://colemono.com/premioscolemono15/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/fb.png">
				</a>


			</div>

			<div id="texto">

				<p>	Por 3ra vez consecutiva tenemos la premiación más esperada por el público que visita nuestro portal, los Premios Colemono
					en su edición 2015, la que en esta ocasión llega junto a un buen puñado de buenos juegos que aparecieron durante el año
					y con todo el ánimo que nos caracteriza para desarrollar un show que guste a gran parte de la comunidad, grandes colaboradores y
					toda la buena onda que ponemos de nuestra parte.
				</p>
				<p>
					Para este año contamos con la misma cantidad de categorías, aunque agregamos una extra para expandir un poco más la opinión del
					público y también la nuestra a la hora de lanzar el show, el cual esperamos sea bien movido y digerible para nuestro público votante...
				</p>
				<p>
					En estos momentos estamos afinando detalles de cómo será la premiación el próximo año, a ver si sale algo más llamativo para la
					comunidad y ver si también podemos contar con invitados interesantes que nos puedan dar su opinión de todo lo que se expondrá.
				</p>
				<p>
					
					Solo podemos adelantar que este año se viene increíble el certamen :D
				</p>

				<p>Pronto más novedades al respecto.</p>

			</div>	


		</div>
		
		<div id="sec3">
			<div id="present">
				<p>PRESENTA</p>
			<p>Y PRODUCE</p>

			</div>
			<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/logo-colemono.png">
			

		</div>

		<div id="sec4">
			<div id="c6">
			<div id="colaboran">
				<p>COLABORAN</p>		

			</div>
			
			<a href="https://www.playstation.com/es-cl/"  target="_blank"><img src="votacion/img/cola/playstation.png" /></a></br>
			</div>
			<div id="c7">
		
			<a href="http://www.bandainamcoent.com/"  target="_blank" style="margin-left: 300px; margin-right: 100px;float:left;"><img src="votacion/img/cola/bandainamco.png" /></a></br>

			<a href="#"  target="_blank"><img src="votacion/img/cola/blizz.png" /></a>
		</div>
		<div id="c8">
				<a href="http://bethsoft.com/age"  target="_blank"><img id="bt"src="votacion/img/cola/bethesda.png" /></a>
			<a href="http://8bitdo.com/"  target="_blank"><img style=" display: block;float: left;margin-left: 15px;  margin-right: 15px;
    margin-top: 50px;" src="votacion/img/cola/8bitdo.png" /></a>


		</div>
	
			<a href="http://www.ea.com/es"  target="_blank"><img id="ea" src="votacion/img/cola/ea.png" /></a>
			
			<!--
			<img src="<?php #echo get_template_directory_uri(); ?>/images/premios14/movistar.png">
			<img src="<?php #echo get_template_directory_uri(); ?>/images/premios14/nintendo.png">	
			
			<img src="<?php #echo get_template_directory_uri(); ?>/images/premios14/festigame.png">	
							
			<a href="http://www.nintendo.com"  target="_blank"><img src="<?php //echo get_template_directory_uri(); ?>/images/premios14/ninty.png"></a>
			
			<a href="http://www.razerzone.com/es-es/store"  target="_blank"><img src="<?php //echo get_template_directory_uri(); ?>/images/premios14/razer.png"></a>
			<a href="http://www.xbox.cl"  target="_blank"><img src="<?php //echo get_template_directory_uri(); ?>/images/premios14/xbox.png"></a>
			<a href="http://www.ea.com/"  target="_blank"><img src="<?php //echo get_template_directory_uri(); ?>/images/premios14/ea.png"></a><br/>
			<a href="http://8bitdo.com/"  target="_blank"><img src="<?php //echo get_template_directory_uri(); ?>/images/premios14/8bit.png"></a>
			<a href="http://klu.cl/"  target="_blank"><img id="klu"src="<?php //echo get_template_directory_uri(); ?>/images/premios14/klu.png"></a>
			<a href="http://mangacorta.cl/"  target="_blank"><img src="<?php //echo get_template_directory_uri(); ?>/images/premios14/manga.png"></a>
			-->	
		</div>

		<div id="sec5">

					<div id="not"></div>


				<div id="Noticias">
						<h1>Noticias</h1>
				
							  <?php echo do_shortcode( "[vc_row][vc_column][vc_home_gallery_posts grid_categories=\"premios-colemono-15\" grid_teasers_count=\"10\"][/vc_column][/vc_row]");
														
														?>




				         	<div class="ultimas-entradas">

    


					</div>










		</div>
		
				
				
			

		</div>
	</div>

		<!-- END MAIN -->
</div>


<?php get_footer(); ?>