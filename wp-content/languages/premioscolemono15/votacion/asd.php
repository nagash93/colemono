<!doctype html>
	<html>
		
<!-- Mirrored from www.nocontinue.cl/gfa/nominados.html by HTTrack Website Copier/3.x [XR&CO'2010], Fri, 02 Jan 2015 15:46:42 GMT -->
<head>
			<meta charset="utf-8">
			<title>
				Nocontinue's GFA 2013
			</title>

			<!-- ESTILOS GENERALES -->
			<link rel="stylesheet" type="text/css" href="css/reset.css">
			<link rel="stylesheet" type="text/css" href="css/estilos.css">
			<!-- ESTILOS SLIDER -->
			<link rel="stylesheet" href="css/foundation.min.css">  
			<link rel="stylesheet" href="css/stage.css">
			<!-- JAVASCRIPT -->
			<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
		</head>

		<body>

			<!-- REDES SOCIALES -->
			<a href="https://www.facebook.com/pages/nocontinue/106703669381624" target="blank"><aside class="btn_face">
				<img src="img/face2.png">
			 </aside>
			</a>

			<a href="https://twitter.com/nocontinue" target="blank"><aside class="btn_twitter">
				<img src="img/twitter2.png">
			 </aside>
			</a>

			<section class="wrapper">
				<!-- HEADER DEL SITIO -->
				<header>
					<section class="header_gfa">
						<a href="gfa13.html"><h1>GFA 2013</h1></a>
						<p class="txt_head">alguien tenía que <br><span class="txt_head_2">premiar el trabajo sucio<span></p>
					</section>
					<a href="http://www.nocontinue.cl/" target="blank">
					<section class="nc_logo"></section></a>
				</header>

				<!-- INFORMACIONES Y DATOS -->
				<section class="info_bar">
					<a href="https://twitter.com/intent/tweet?button_hashtag=GFA13" data-url="http://www.nocontinue.cl"><article class="hashtag">
						
					</article></a>

					<article class="info_txt">
						<p>desde el 24 de diciembre al 15 de enero, ¡no dejes de votar!</p>
					</article>
				</section>

				<!--CONTENEDOR SLIDER-->
				<section class="slider">
						<div id="featuredContent">
			        		

			        		<!-- NOMINACIÓN PEOR JUEGO PS3 -->
							<div class="ps3">
									<article class="box_img">
										<img src="img/ps3.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll2e79.js?p=52b9bfcee4b084acd8563e65"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey solutions</div></a>	
			        		</div>

			        		<!-- NOMINACIÓN PEOR JUEGO XBOX360 -->
							<div class="xbox">
									<article class="box_img">
										<img src="img/360.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPolld3a4.js?p=52b9d9eae4b084acd8563e8c"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PEOR JUEGO WIIU -->
							<div class="wiiu">
									<article class="box_img">
										<img src="img/wiiu.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll971b.js?p=52b9f88ae4b084acd8563ebd"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey service</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PEOR JUEGO PC -->
							<div class="pc">
									<article class="box_img">
										<img src="img/pc.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPolla956.js?p=52b9dc68e4b084acd8563e90"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls & surveys</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PEOR JUEGO PORTATIL -->
							<div class="portatil">
									<article class="box_img">
										<img src="img/portatil.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPollae9d.js?p=52b9fa9ee4b084acd8563ebf"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PEOR NEXTGEN -->
							<div class="nextgen">
									<article class="box_img">
										<img src="img/nextgen.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll3e59.js?p=52b9deace4b084acd8563e93"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey service</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PEOR COVER ART -->
							<div class="art">
									<article class="box_img">
										<img src="img/art.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll9875.js?p=52b9dfcbe4b084acd8563e94"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey solution</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PEOR POKEMON -->
							<div class="poke">
									<article class="box_img">
										<img src="img/poke.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPollde9f.js?p=52b9e07ce4b084acd8563e97"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys</div></a>
			        		</div>

			        		<!-- NOMINACIÓN COMIDO POR EL HYPE -->
							<div class="hype">
									<article class="box_img">
										<img src="img/hype.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll02d1.js?p=52b9e543e4b084acd8563e9a"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">free polls</div></a>
			        		</div>

			        		<!-- NOMINACIÓN HPSTER -->
							<div class="hipster">
									<article class="box_img">
										<img src="img/hipster.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPollcd01.js?p=52b9e787e4b084acd8563ea2"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">customer surveys</div></a>
			        		</div>


			        		<!-- NOMINACIÓN NADIE ME PESCÓ -->
							<div class="nadie">
									<article class="box_img">
										<img src="img/nadie.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll2268.js?p=52b9e815e4b084acd8563ea3"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PERSONAJE FEMENINO -->
							<div class="mijita">
									<article class="box_img">
										<img src="img/mijita.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPolla6d3.js?p=52b9e891e4b084acd8563ea4"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">web survey</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PERSONAJE MASCULINO -->
							<div class="wachin">
									<article class="box_img">
										<img src="img/wachin.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll2376.js?p=52b9e8ffe4b084acd8563ea5"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys</div></a>
			        		</div>

			        		<!-- NOMINACIÓN PERSONAJE NEGRO -->
							<div class="negro">
									<article class="box_img">
										<img src="img/negro.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPolle22d.js?p=52b9e973e4b084acd8563ea6"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls & surveys</div></a>
			        		</div>

			        		<!-- NOMINACIÓN MACHISTA -->
							<div class="machista">
									<article class="box_img">
										<img src="img/machista.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPollfced.js?p=52b9e9f0e4b084acd8563ea9"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls</div></a>
			        		</div>

			        		<!-- NOMINACIÓN POLEMICA -->
							<div class="polemica">
									<article class="box_img">
										<img src="img/polemica.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll939a.js?p=52b9ea86e4b084acd8563eab"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey tools</div></a>
			        		</div>

			        		<!-- NOMINACIÓN ESTUDIO -->
							<div class="estudio">
									<article class="box_img">
										<img src="img/estudio.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPolle599.js?p=52ba026fe4b084acd8563ec9"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>
			        		</div>

			        			<!-- NOMINACIÓN CASI GOTY -->
							<div class="casi">
									<article class="box_img">
										<img src="img/casi.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPollefa7.js?p=52b9edc6e4b084acd8563eb2"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey tools</div></a>
			        		</div>

			        			<!-- NOMINACIÓN SHITTY -->
							<div class="shitty">
									<article class="box_img">
										<img src="img/shitty.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPoll2846.js?p=52b9ee2ce4b084acd8563eb3"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls & surveys</div></a>
			        		</div>

			        			<!-- NOMINACIÓN SHITTY -->
							<div class="goty">
									<article class="box_img">
										<img src="img/goty.png">
									</article>
									<!--  ENCUESTA -->
									<script type="text/javascript" src="../../www.easypolls.net/ext/scripts/emPollc091.js?p=52b9eea2e4b084acd8563eb4"></script><a class="OPP-powered-by" href="https://www.murvey.com/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online surveys</div></a>
			        		</div>



			        		
								

									
			               		
						</div>
				</section>
			</section>

	<!-- Included JS Files (Compressed) -->
  	<!--<script src="js/jquery.js"></script>-->
  	<script src="js/foundation.min.js"></script>
  
  	<!-- Initialize JS Plugins -->
  	<script src="js/app.html"></script>
	<script type="text/javascript">

    $( document ).ready(function() {
		 $('#featuredContent').orbit({ 
        timer: false,
        fluid: '16x9' 
      });
	});
    </script>
		<body>
	
<!-- Mirrored from www.nocontinue.cl/gfa/nominados.html by HTTrack Website Copier/3.x [XR&CO'2010], Fri, 02 Jan 2015 15:49:49 GMT -->
</html>
















