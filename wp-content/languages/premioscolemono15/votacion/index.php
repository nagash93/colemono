﻿<?php

/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */
/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */


/** Loads the WordPress Environment and Template */

require('../../wp-blog-header.php');


$van_page_type = van_page_type(); 
?>


<?php
/**
* 
* The template for displaying the header.
* 
* @author : VanThemes ( http://www.vanthemes.com )
* 
*/
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	    <link rel="apple-touch-icon" href="touch-icon-iphone.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="touch-icon-ipad-retina.png" />
    <meta property="og:image" content="http://colemono.com/wp-content/uploads/2013/12/logo-enl.png" />     
     <meta property="og:title" content="Premios Colemono" /> 
    <meta property="og:url" content="http://colemono.com/premioscolemono14" /> 
 	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:creator" content="@cole_mono">
	<meta name="twitter:site" content="@cole_mono">
	<meta name="twitter:title" content="Participa y gana con los Premios Colemono 2014 ">
	<meta name="twitter:description" content="Ya llegó la premiación más esperada por los usuarios de Colemono, nos referimos a la primera edición de los Premios Colemono en su edición 2014 donde junto a nuestra comunidad de usuarios seleccionaremos a lo mejor que ha aparecido este año.
">
	<meta name="twitter:domain" content="Colemono.com"/>
	<meta name="twitter:image" content="http://colemono.com/wp-content/uploads/2013/04/juego-ano1.jpg">
	<title>Premios Colemono</title>



	<?php wp_head(); ?>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/premios14.css" type="text/css"  />
	<style type="text/css">
		a{text-decoration: none;}
	</style>	
	<link rel="stylesheet" href="css/foundation.min.css">  	
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
</head>

<body  >
	<!-- Full Screen Background -->
	<div class="full-screen-bg"><div class="screen-inner"></div></div>
	
	<!-- HEADER -->
	<div class="header-nav">
					<div id="headernav" class="container">

						<?php wp_nav_menu( array('theme_location' => 'HeaderNav', 'menu_class'  => 'clearfix','fallback_cb' => 'van_nav_alert') ); ?>
					</div>	
				</div>


	<header id="main-header">
			

		<div id="top-bar">
			<div id="wlogo" class="container clearfix">

				<div id="logo">
					<h1>
						<a href="http://colemono.com/" title="Colemono" >
							<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/logo.png" alt="Colemono" class="retina" data-retina="http://colemono.com/wp-content/uploads/2014/03/retina-logo.png" >
						</a>
					</h1>
					</div><!-- #logo -->
				
				<?php if ( van_get_option("header_social") ): ?>
					<div id="header-social">
						<?php van_social_networks(); ?>
					</div><!-- #header-social -->						
				<?php endif; ?>

			</div><!-- .container -->
		</div><!-- #top-bar -->

		<div id="main-nav-wrap" class="container content " style="overflow:auto;">
			<div id="premiacion">
				<p>PREMIACIÓN EN VIVO, SÁBADO 31 DE ENERO</p>
			</div>

			<div id="colab">
				COLABORAN: <img src="<?php echo get_template_directory_uri(); ?>/images/premios14/todas.png" alt="premios colemono" >

			</div>

		</div><!-- #main-nav-wrap -->

	</header><!-- #main-header -->

	<!-- MAIN -->

<div id="vslider">

		<section class="slider">
						<div id="featuredContent">		        		
			        		
							<div class="nomina" id="juego">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0767e4b0cdaa8af2e2a2"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey tools</div></a>
							</div>

			        		<div class="nomina" id="ninty">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab077ae4b0cdaa8af2e2a9"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online surveys</div></a>

			        		</div>

			        		<div class="nomina" id="ps">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0789e4b0cdaa8af2e2ae"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">feedback surveys</div></a>


			        		</div>

			        		<div class="nomina" id="xbox">
								<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0792e4b0cdaa8af2e2af"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online surveys</div></a>


			        		</div>

			        		<div class="nomina" id="pc">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab07c3e4b0cdaa8af2e2c9"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey software</div></a>


			        		</div>

			        		<div class="nomina" id="por">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab07e4e4b0cdaa8af2e2d9"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>


			        		</div>


			        		<div class="nomina" id="multi">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab07f1e4b0cdaa8af2e2dd"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls</div></a>

			        		</div>

			        		<div class="nomina" id="pj">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab25a4e4b0cdaa8af2eb34"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey hosting</div></a>


			        		</div>

			        		<div class="nomina" id="fk">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0808e4b0cdaa8af2e2e5"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>


			        		</div>

			        		<div class="nomina" id="phd">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0814e4b0cdaa8af2e2ea"></script><a class="OPP-powered-by" href="https://www.easypolls.net/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">web polls</div></a>


			        		</div>

			        		<div class="nomina" id="pole">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0820e4b0cdaa8af2e2ee"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey hosting</div></a>


			        		</div>

			        		<div class="nomina" id="pes">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab252ce4b0cdaa8af2eb1e"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">online survey</div></a>


			        		</div>

			        		<div class="nomina" id="race">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0838e4b0cdaa8af2e2f9"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey services</div></a>


			        		</div>

			        		<div class="nomina" id="pele">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab084be4b0cdaa8af2e306"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>



			        		</div>

			        		<div class="nomina" id="rpg">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0864e4b0cdaa8af2e30d"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">survey service</div></a>
			        		</div>

			        		<div class="nomina" id="fps">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0876e4b0cdaa8af2e314"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls</div></a>

			        		</div>

			        		<div class="nomina" id="indie">
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab0894e4b0cdaa8af2e324"></script><a class="OPP-powered-by" href="https://www.murvey.com" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">surveys & polls</div></a>

			        		</div>

			        		<div class="nomina" id="p2015">
									
									<script type="text/javascript" src="http://www.easypolls.net/ext/scripts/emPoll.js?p=54ab089de4b0cdaa8af2e328"></script><a class="OPP-powered-by" href="http://www.objectplanet.com/opinio/" style="text-decoration:none;"><div style="font: 9px arial; color: gray;">polls</div></a>

			        		</div>

			        		<div class="nomina" id="voto">
									
			        		</div>


			               		
						</div>
				</section>
			</section>


 </div>
	 							
<div id="background">	
<div id="wrap-container">
<div id="main-wrap" class="container <?php echo van_sidebar_layout(); ?>" style="margin-top:-50px;">

		<!-- BEGIN MAIN -->
			
	<div id="main-content" style="width: 100%; background:#FFFFFF; text-decoration:none;" >

		<div id="sec2">
			<div id="comparte">
					<p>COMPARTE Y COMENTA</p>
					<p>EN REDES SOCIALES</p>
					<p>#PremiosColemono</p>
			</div>
			<div id="redes">

				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://colemono.com/premioscolemono14/" data-via="Cole_mono" data-hashtags="PremiosColemono">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/twitter.png">
				</a>
				<a href="http://www.facebook.com/sharer.php?u=http://colemono.com/premioscolemono14/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/premios14/fb.png">
				</a>


			</div>

	
  	<script src="js/foundation.min.js"></script>
    <script src="js/app.html"></script>
	<script type="text/javascript">

    $( document ).ready(function() {
		 $('#featuredContent').orbit({ 
        timer: false,
        fluid: '16x9' 
      });
	});
    </script>
	






		</div>

		<!-- END MAIN -->
</div>


<?php get_footer(); ?>