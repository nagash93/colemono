function inicio() {
    var valor = leerCookie("fondo");

    if (valor == "negro") {
         negro();
    }
}

function fondo() {
    var valor = leerCookie("fondo");
    if (valor == "negro") {
        document.cookie = "fondo=blanco;expires=31 Dec 2015 23:59:59 GMT";
        blanco();

    } else {

        document.cookie = "fondo=negro;expires=31 Dec 2015 23:59:59 GMT";
        
        negro();
       
    }

}

function negro(){

 var x = document.getElementsByClassName("content");
        var y;
        for (y = 0; y < x.length; y++) {
            x[y].style.backgroundColor = "black";
        }
        document.getElementById("wrap-container").style.background = "black";
        document.getElementById("headernav").style.background = "black";
        document.getElementById("background").style.background = "#636363";
        document.body.style.background = "black";

}

function blanco(){

       var x = document.getElementsByClassName("content");
        var y;
        for (y = 0; y < x.length; y++) {
            x[y].style.backgroundColor = "#fff";
        }
        document.getElementById("wrap-container").style.background = "#fff";
        document.getElementById("headernav").style.background = "#fff";
        document.getElementById("background").style.background = "#ededed";
        document.body.style.background = "#ededed";

}




function leerCookie(nombre) {
    var lista = document.cookie.split(";");
    for (var i in lista) {
        var busca = lista[i].search(nombre);
        if (busca > -1) {
            micookie = lista[i];
        }
    }
    var igual = micookie.indexOf("=");
    var valor = micookie.substring(igual + 1);
    return valor;
}