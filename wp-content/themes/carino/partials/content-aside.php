<?php 
/**
* The template for displaying posts in the standard post format.
*
* @author : VanThemes ( http://www.vanthemes.com )
* 
*/
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( array('content', 'post-inner', 'standard-format') ); ?>  >





<?php if ( is_home() ) { ?>
    	<?php van_ribbon(); ?>
	<div class="asidemini" >
	<?php van_entry_media(); ?>

	<div class="entry-container asidecontainer" >

		<header class="entry-header">

			<?php if ( is_single() ): ?>

				<h1 class="entry-title"><?php the_title(); ?></h1><!-- .entry-title -->

			<?php else: ?>

				<h2 class="entry-title">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'van' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h2><!-- .entry-title -->

			<?php endif; ?>
			
			<?php van_entry_meta(); ?>

		</header>

		<?php van_entry_content(); ?>

		<?php van_entry_footer(); ?>

	</div>
</div>
<?php  } else { ?>


	<?php van_ribbon(); ?>
	
	



		<div class="entry-media">
			<a href="<?php the_permalink(); ?>">
			<?php van_thumb(620,330 ); ?>
			<div class="thumb-overlay"></div>
		</a>
			<a data-gal="prettyPhoto" href="<?php echo  esc_url( van_post_thumbnail("large") );  ?>"  class="zoom"></a>
			<a  href="<?php the_permalink(); ?>" class="link"></a>
	
		</div><!-- .entry-media -->




	<div class="entry-container">

		<header class="entry-header">

			<?php if ( is_single() ): ?>

				<h1 class="entry-title"><?php the_title(); ?></h1><!-- .entry-title -->

			<?php else: ?>

				<h2 class="entry-title">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'van' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h2><!-- .entry-title -->

			<?php endif; ?>
			
			<?php van_entry_meta(); ?>

		</header>

		<?php van_entry_content(); ?>

		<?php van_entry_footer(); ?>

	</div>

<?php
    
}
?>
















</article>