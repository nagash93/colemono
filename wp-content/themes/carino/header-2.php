<?php
/**
* @author :
*/
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta name="theme-color" content="#0d93d0">
	<?php wp_head(); ?>		<?php flush(); ?>

 
</head>
<body <?php body_class();?>>

	<!-- Full Screen Background -->
	<div class="full-screen-bg"><div class="screen-inner"></div></div>	
	<!-- HEADER -->
	<div class="header-nav">
					<div id="headernav" class="container">
						<?php wp_nav_menu( array('theme_location' => 'HeaderNav', 'menu_class'  => 'clearfix','fallback_cb' => 'van_nav_alert') ); ?>
					</div>	
				</div>
	<header id="main-header">	
		<div id="top-bar">
			<div id="wlogo" class="container clearfix">
				<?php van_logo();?>	
				<div id="banner-top"><a href="#" TARGET="_blank" ></a></div>			
				<?php if ( van_get_option("header_social") ): ?>
				<?php if (is_home()){ echo '';}?>
					<div id="header-social">						
						<?php van_social_networks(); ?>
					</div><!-- #header-social -->						
				<?php endif; ?>
			</div><!-- .container -->
		</div><!-- #top-bar -->

	</header>

		
	<nav>		
		<div id="main-nav-wrap" class="container content clearfix <?php echo !van_get_option("sticky_nav") ? 'disabled-sticky' : ''; ?>" >
			<nav id="main-navigation" role="navigation">
				<div class="mobile-nav">
					<?php van_menu_select(array( 'menu_name' => 'PrimaryNav', 'id' => 'PrimaryNav' )); ?>
				</div>				
				<div class="main-nav">
					<?php wp_nav_menu( array('theme_location' => 'PrimaryNav', 'menu_class'  => 'clearfix','fallback_cb' => 'van_nav_alert') ); ?>
				</div>
			</nav><!-- #main-navigation -->
			<?php if ( van_get_option("nav_search") ): ?>
				<div id="header-search">
					<form method="get" class="searchform clearfix" action="<?php echo esc_url( home_url() ); ?>/" role="search">
						<a href="#" class="search-icn icon icon-search"></a>
						<input type="text" name="s" placeholder="<?php _e('Type and hit enter to search...','van') ?>">
					</form>            	
				</div><!-- #header-search -->					
			<?php endif; ?>
		</div><!-- #main-nav-wrap -->
		
	</nav><!-- #main-header -->
	<!-- MAIN -->
	
			<?php if (is_home()){ echo do_shortcode('<div id="slider">[masterslider id="2"] ');}?>


	 
	 	<?php if ( have_posts() ) : ?>
			
				<?php if (   (is_home()) && ($paged <= "1")  ) {
				 if ( van_get_option("home_carousel") ) {
					get_template_part('templates/main-carousel');}}?>
				
		
		<?php endif; echo "</div>" ?>

	 



<div id="background"> 	
	
<div id="wrap-container">
	
<div id="main-wrap" class="container <?php echo van_sidebar_layout(); ?>">


		