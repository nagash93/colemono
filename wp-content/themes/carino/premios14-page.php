<?php
/*
 * Template Name: Premios colemono
 * Description: Premios colemono.
 */
?>



<?php
/**
* @author :
*/
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>		<?php flush(); ?>

 <script type="text/javascript" src="http://colemono.com/wp-content/themes/carino/js/blackstyle.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/premios14.css" type="text/css"  />

</head>
<body <?php body_class();?> style="margin-top: -30px;">

	<!-- Full Screen Background -->
	<div class="full-screen-bg"><div class="screen-inner"></div></div>	
	<!-- HEADER -->
	<div class="header-nav">
					<div id="headernav" class="container">
						<?php wp_nav_menu( array('theme_location' => 'HeaderNav', 'menu_class'  => 'clearfix','fallback_cb' => 'van_nav_alert') ); ?>
					</div>	
				</div>
	<header id="main-header">	
		<div id="top-bar">
			<div id="wlogo" class="container clearfix">
				<?php van_logo();?>	
				<div id="banner-top"></div>			
				<?php if ( van_get_option("header_social") ): ?>
				<?php if (is_home()){ echo '';}?>
					<div id="header-social">						
						<?php van_social_networks(); ?>
					</div><!-- #header-social -->						
				<?php endif; ?>
			</div><!-- .container -->
		</div><!-- #top-bar -->

	</header>

		
	
	


	 
<div id="background">	
	<div id="slider"> <?php van_entry_media(); ?></div>
<div id="wrap-container">



<div id="main-wrap" class="container <?php echo van_sidebar_layout(); ?>">



<div id="main-content"  class="<?php echo $van_page_type['type'] . ' ' . $van_page_type['container']; ?>">

	<div id="single-outer">


		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( array('content','post-inner') ); ?>>
					

	
				
					<div class="entry-container">

						<header id="entry-header">
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1><!-- .entry-title -->
						</header>

						<div class="entry-content">

							 <?php the_content(); ?>
							 
							<?php wp_link_pages(array('before' => '<p><strong>'.__( 'Pages:','van').'</strong> ', 'after' => '</p>')); ?>										
						
							<?php edit_post_link( __( '(Edit)', 'van' ), '<span class="edit-post">', '</span>' ); ?>
				
						</div><!-- .entry-content -->
						
					</div><!-- .entry-container -->

				</article>

			<?php endwhile; ?>

		<?php endif;  ?>

	

	</div> <!-- #single-outer -->

</div><!-- #main-content -->


