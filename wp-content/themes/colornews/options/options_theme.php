<?php 

$themename = "Colornews Options";
$shortname = "op";

$cats_array = get_categories('hide_empty=0');
$pages_array = get_pages('hide_empty=0');
$site_pages = array();
$site_cats = array();

foreach ($pages_array as $pagg) {
	$site_pages[$pagg->ID] = htmlspecialchars($pagg->post_title);
	$pages_ids[] = $pagg->ID;
}

foreach ($cats_array as $categs) {
	$site_cats[$categs->cat_ID] = $categs->cat_name;
	$cats_ids[] = $categs->cat_ID;
}
 
$op_categories_obj = get_categories('hide_empty=1');
$op_categories = array();
foreach ($op_categories_obj as $op_cat) {
$op_categories[$op_cat->cat_ID] = $op_cat->category_nicename;
}
$categories_tmp = array_unshift($op_categories, "Select a category:");	

$google_fonts_header =  array('Oswald','Lato','Open Sans','Open Sans Condensed','Droid Sans','Raleway','Droid Serif','Bitter','PT Sans', 'PT Sans Narrow', 'PT Sans Caption', 'Abel','Lora','Nobile','Crimson Text','Arvo','Tangerine','Cuprum','Cantarell','Philosopher','Josefin Sans','Dancing Script','Raleway','Bentham','Goudy Bookletter 1911','Quattrocento','Ubuntu');
$google_fonts =  array('Open Sans','Open Sans Condensed','Droid Sans','Oswald','Lato','Droid Serif','Bitter','PT Sans', 'PT Sans Narrow', 'PT Sans Caption', 'Abel','Lora','Nobile','Crimson Text','Arvo','Tangerine','Cuprum','Cantarell','Philosopher','Josefin Sans','Dancing Script','Raleway','Bentham','Goudy Bookletter 1911','Quattrocento','Ubuntu');

$royal_options = array (
	
array( "type" => "tabs-open",),
		
array( "name" => "General_1",
       "type" => "tab",
	   "desc" => "General settings"),

array( "name" => "General_2",
		"type" => "tab",
		"desc" => "Featured"),	
		
array( "name" => "General_3",
		"type" => "tab",
		"desc" => "Pages"),
		
array( "name" => "General_4",
		"type" => "tab",
		"desc" => "Single post"),			

array( "name" => "General_41",
		"type" => "tab",
		"desc" => "Single page"),			
		
array( "name" => "General_5",
		"type" => "tab",
		"desc" => "Colorization"),

array( "name" => "General_52",
		"type" => "tab",
		"desc" => "Ads & Social"),	
		
array( "name" => "General_6",
		"type" => "tab",
		"desc" => "SEO"),	

array( "name" => "General_7",
		"type" => "tab",
		"desc" => "Translation"),	
	
array( "name" => "General_8",
		"type" => "tab",
		"desc" => "Layout"),		
	
array( "type" => "tabs-close",),
		
		
array( "name" => "General_1",
		"type" => "content-open",),						
		
array( "name" => "Custom Style (Must be enabled!)",
        "id" => $shortname."_custom_colors",
        "type" => "checkbox",
        "std" => "on",
        "desc" => "Enable this option to display custom styles theme"),	

array( "type" => "clearfix",),	
	
array( "name" => "Disable Default Menu Scripts (Only if you have enabled menu plugin, ex: Mega Main Menu)",
        "id" => $shortname."_menu_disable",
        "type" => "checkbox",
        "std" => "false",
        "desc" => "Enable this option to Disable Default Menu Scripts"),	

array( "type" => "clearfix",),

array( "name" => "Favicon",
		"id" => $shortname."_favicon",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to the image favicon <br/>(size: 16x16px, format: .ico)"),		
	
array( "name" => "Logo",
        "id" => $shortname."_logo_on",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "If logo is turned off, then will be displayed name and site description."),
			
array( "type" => "clearfix",),
		
array( "name" => "Show Logo in Featured area",
        "id" => $shortname."_logo_feat_on",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Show logo in Featured area instead in header."),
			
array( "type" => "clearfix",),
		
array( "name" => "Logo image",
		"id" => $shortname."_logo",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to the image"),			 							

array( "type" => "clearfix",),			

array( "name" => "Header Cufon Font",
		"id" => $shortname."_header_font",
		"type" => "select",
		"std" => "Kreon",
		"options" => $google_fonts_header,
		"desc" => "Select Cufon font for h1-h6 headers.",),	

array( "name" => "Custom Header Cufon Font",
        "id" => $shortname."_custom_header_font",
        "type" => "text",
		"std" => "",
		"desc" => "Write your own Cufon Font for Headers"),				
		
array( "name" => "Text Cufon Font",
		"id" => $shortname."_text_font",
		"type" => "select",
		"std" => "Kreon",
		"options" => $google_fonts,
		"desc" => "Select Cufon font for text.",),	
			
array( "type" => "clearfix",),	
		
array( "name" => "Header top panel",
        "id" => $shortname."_header_top_menu",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Header top panel."),
		
array( "type" => "clearfix",),		
	
array( "name" => "Header (Logo and Banner)",
        "id" => $shortname."_header",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option to display Header."),
		
array( "type" => "clearfix",),	

array( "name" => "Show login panel in header",
        "id" => $shortname."_header_login",
        "type" => "checkbox",
        "std" => "false",
        "desc" => "Enable this option to display login panel in header"),		
	
array( "type" => "clearfix",),

array( "name" => "Fixed sidebars",
        "id" => $shortname."_fixed_sidebars",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to make sidebars fixed position."),

array( "type" => "clearfix",),		

array( "name" => "Most popular posts in Footer sidebar",
        "id" => $shortname."_footer_popular_posts",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Most popular posts in Footer sidebar."),

array( "type" => "clearfix",),

array( "name" => "Footer sidebar",
        "id" => $shortname."_footer_sidebar",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display footer sidebar."),

array( "type" => "clearfix",),

array( "name" => "Display info - Designed by author in Footer",
        "id" => $shortname."_designed_by",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option to display Designed by author."),

array( "type" => "clearfix",),	
	
array( "name" => "Google Analytics Code",
        "id" => $shortname."_ga_code",
		"std" => "",
        "type" => "textarea",
		"desc" => "Insert GA Code which will be displayed in footer."),	
		
array( "type" => "clearfix",),			

array( "name" => "Custom CSS",
        "id" => $shortname."_custom_css",
		"std" => "",
        "type" => "textarea",
		"desc" => "Write your own custom styles."),		
		
		
		
array( "type" => "clearfix",),				   
array( "name" => "General_1",
		"type" => "content-close",),
		
		
array( "name" => "General_2",
		"type" => "content-open",),								

		
array( "name" => "News ticker",
        "id" => $shortname."_news_ticker",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display News ticker."),	
		
array( "type" => "clearfix",),
		
array( 	"name" => "News ticker - recent posts or featured category",
		"id" => $shortname."_recent_news_ticker",
		"type" => "select",
		"std" => "",
        "options" => array('Recent posts', 'Featured category'),
		"desc" => "Select recent posts or featured category posts for News ticker."),	
		
array( 	"name" => "Header ticker category",
		"desc" => "Select the category for ticker.",
		"id" => $shortname."_ticker_cat",
		"std" => "Select a category:",
		"type" => "select",
		"options" => $op_categories),
		
array( "type" => "clearfix",),	
		
array( 	"name" => "Header ticker - number of posts",
		"id" => $shortname."_ticker_slides",
		"type" => "select",
		"std" => "",
        "options" => array('2','3', '4', '5', '6', '7', '8', '9', '10'),
		"desc" => "Select the number posts."),		
		

array( "name" => "Featured area",
        "id" => $shortname."_featured_area",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display featured area on home page."),	
		
array( "type" => "clearfix",),			
	
array( 	"name" => "Home Slider/Carousel - Recent posts or Featured category",
		"id" => $shortname."_recent_featured_flex",
		"type" => "select",
		"std" => "",
        "options" => array('Recent posts', 'Featured category'),
		"desc" => "Select recent posts or featured category posts for Slider/Carousel slider."),		
	
array( 	"name" => "Home Slider/Carousel - Featured category",
		"desc" => "Select the category for Slider/Carousel slider.",
		"id" => $shortname."_feat_cat",
		"std" => "Select a category:",
		"type" => "select",
		"options" => $op_categories),
		
array( "type" => "clearfix",),

array( 	"name" => "Home Slider/Carousel - Number of slides",
		"id" => $shortname."_feat_slides",
		"type" => "select",
		"std" => "",
        "options" => array('3', '4', '5', '6', '7', '8', '9', '10', '13', '14'),
		"desc" => "Select the number of slides."),				
		
array( 	"name" => "Slider - Variant of time",
		"id" => $shortname."_slider_time_variant",
		"type" => "select",
		"std" => "",
        "options" => array('Standard', 'Time ago'),
		"desc" => "Select Variant of time for slider."),		
	

	
array( "type" => "clearfix",),	
array( "name" => "General_2",
		"type" => "content-close",),

	
array( "name" => "General_3",
		"type" => "content-open",),	
			
array( "name" => "Breadcrumbs in pages",
        "id" => $shortname."_crumbs",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display breadcrumbs on pages"),	
		
array( "type" => "clearfix",),	
							
array( "name" => "Breadcrumbs in categories",
        "id" => $shortname."_crumbs_cats",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display breadcrumbs on categories"),	
		
array( "type" => "clearfix",),	
		
array( "name" => "Category name and description",
        "id" => $shortname."_cats_line",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display Category name and description on categories"),	
		
array( "type" => "clearfix",),			
		
		
array( "name" => "Archive page - Time",
        "id" => $shortname."_index_time",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post time in archives, categories"),	
		
array( "type" => "clearfix",),			
		
array( "name" => "Archive page - Author",
        "id" => $shortname."_index_author",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post author in archives, categories"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Archive page - Categories",
        "id" => $shortname."_index_categories",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post categories in archives, categories"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Archive page - Comments",
        "id" => $shortname."_index_comments",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post comments in archives, categories"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Archive page - Likes",
        "id" => $shortname."_index_likes",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post likes in archives, categories"),		
		
array( "type" => "clearfix",),		
		
		
		
array( "name" => "Email for contact form",
        "id" => $shortname."_contact_email",
        "type" => "text",
		"std" => "",
		"desc" => "Enter your email for contact form"),			
	
array( "name" => "Text for contact page",
		"id" => $shortname."_cf_text",
		"type" => "textarea",
		"std" => "",
		"desc" => "Enter the text what will be displayed on contact page",),	
		
array( "name" => "Contact page map code",
		"id" => $shortname."_cf_map",
		"type" => "textarea",
		"std" => "",
		"desc" => "Enter the map code what will be displayed on contact page<br />(recomend size: 300x320px)",),	
		
array( "type" => "clearfix",),
array( "name" => "General_3",
	    "type" => "content-close",),		

		
		
		

array( "name" => "General_4",
		"type" => "content-open",),				
		
array( "name" => "Title font size",
        "id" => $shortname."_title_size",
        "type" => "select",
		"std" => "",
		"options" => array('17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'),
		"desc" => "Select title font size for single posts and pages."),				
	
array( "name" => "Breadcrumbs",
        "id" => $shortname."_crumbs_single",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display breadcrumbs in single posts"),	
		
array( "type" => "clearfix",),				
	
array( "name" => "Custom post excerpt on top of the text",
        "id" => $shortname."_custom_single_text",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display Custom post excerpt"),
		
array( "type" => "clearfix",),	
	
array( "name" => "Single post meta line",
        "id" => $shortname."_single_meta_line",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display meta line (date, category) in single post."),		

array( "type" => "clearfix",),	

array( "name" => "Single post - Time",
        "id" => $shortname."_single_time",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post time in single post"),	
		
array( "type" => "clearfix",),			
		
array( "name" => "Single post - Author",
        "id" => $shortname."_single_author",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post author in single post"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Single post - Categories",
        "id" => $shortname."_single_categories",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post categories in single post"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Single post - Comments",
        "id" => $shortname."_single_post_comments",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post comments in single post"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Single post - Likes",
        "id" => $shortname."_single_likes",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display post likes in single post"),		
		
array( "type" => "clearfix",),		
		
array( "name" => "Featured Similar posts",
        "id" => $shortname."_feat_similar",
        "type" => "checkbox",
        "std" => "false",
        "desc" => "Enable this option for display similar posts in single posts Full width image"),
				   
array( "type" => "clearfix",),		
	
array( "name" => "Tags",
        "id" => $shortname."_tags",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option for display tags in single posts."),		   
	
array( "type" => "clearfix",),
	
array( "name" => "Author box",
        "id" => $shortname."_author_box",
        "type" => "checkbox",
        "std" => "false",
        "desc" => "Enable this option for display Author box in single posts"),
				   
array( "type" => "clearfix",),			
			
		
array( "name" => "Navigation - variant",
        "id" => $shortname."_nav_variant",
		"std" => "",
        "type" => "select",
		"options" => array("After Text", "Sticky"),
		"desc" => "Select a Navigation variant"),			
		
array( "name" => "Comments",
        "id" => $shortname."_single_comments",
        "type" => "checkbox",
        "std" => "on",
        "desc" => "Enable this option for display comments in single posts"),	   
			
array( "type" => "clearfix",),

array( "name" => "Comments variant",
        "id" => $shortname."_comments_variant",
		"std" => "",
        "type" => "select",
		"options" => array("Simple comments","Disqus"),
		"desc" => "Select a comments variant"),	
	
array( "name" => "Discus ID",
        "id" => $shortname."_discus",
        "type" => "text",
		"std" => "",
		"desc" => "Enter your Discus ID"),	

array( "name" => "General_4",
	    "type" => "content-close",),		
	
	
	
	
	
array( "name" => "General_41",
		"type" => "content-open",),			
	
array( "name" => "Breadcrumbs",
        "id" => $shortname."_crumbs_page",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display breadcrumbs in single page"),	
		
array( "type" => "clearfix",),		
	
array( "name" => "Single page meta line",
        "id" => $shortname."_page_meta_line",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option for display meta line (date, category) in single page."),		

array( "type" => "clearfix",),		
	
array( "name" => "Comments",
        "id" => $shortname."_single_page_comments",
        "type" => "checkbox",
        "std" => "on",
        "desc" => "Enable this option for display comments in single pages"),	   	
	
	
array( "type" => "clearfix",),

array( "name" => "General_41",
	    "type" => "content-close",),	
		
		
array( "name" => "General_5",
		"type" => "content-open",),	
		
		
array( "name" => "Theme - Main colors",
		"id" => $shortname."_theme_colors",
		"type" => "textcolorpopup",
		"std" => "fff000",
		"desc" => "Choose a Main colors for all Theme",),		
	
array( "name" => "Header - background color",
		"id" => $shortname."_header_bg",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a Header background color",),	
		
array( "name" => "Top panel (Top menu) - background color",
		"id" => $shortname."_header_top_panel_bg",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a background color for Top menu",),		

array( "name" => "Top panel - color of links",
		"id" => $shortname."_header_top_panel_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a Top panel - color of links",),	
		
array( "name" => "Headers color h1 - h6 (For single posts/pages only)",
		"id" => $shortname."_headers_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a headers color",),	

array( "name" => "Site title - color",
		"id" => $shortname."_site_title_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a site title and description color",),

array( "name" => "Site title - hover color",
		"id" => $shortname."_site_title_color_hover",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a site title hover color",),
		
array( "name" => "News Ticker - background color",
		"id" => $shortname."_news_ticker_bg",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a background color for News Ticker",),

array( "name" => "News Ticker title - color",
		"id" => $shortname."_news_ticker_title",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a color of title for News Ticker",),		
		
array( "name" => "News Ticker links - color",
		"id" => $shortname."_news_ticker_links",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a color of links for News Ticker",),
		
array( "name" => "News Ticker category - color",
		"id" => $shortname."_news_ticker_category",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a color of category for News Ticker",),	
		
array( "name" => "Categories in Featured area - color",
		"id" => $shortname."_custom_cat_class_color",
		"type" => "textcolorpopup",
		"std" => "fff000",
		"desc" => "Choose a color of categories in Featured area",),	

array( "name" => "Sidebar headers - color",
		"id" => $shortname."_sidebar_headers_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a sidebar headers color",),			

array( "name" => "Sidebar footer headers - color",
		"id" => $shortname."_sidebar_footer_headers_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a sidebar footer headers color",),	
	
array( "name" => "Breadcrumbs - color",
		"id" => $shortname."_crumbs_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a color for Breadcrumbs",),	
		
array( "name" => "Breadcrumbs - color on hover",
		"id" => $shortname."_crumbs_color_hover",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a color for Breadcrumbs on hover",),		
	
	

array( "name" => "Footer Popular posts - color",
		"id" => $shortname."_footer_popular_posts_bg",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a Popular posts background color",),		
		
array( "name" => "Footer - background color",
		"id" => $shortname."_footer_background",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a footer background color",),		

array( "name" => "Footer bottom line - background color",
		"id" => $shortname."_footer_bottom_background",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a footer bottom line background color",),	
	
	
	
array( "name" => "Main menu (Default menu) - background color",
		"id" => $shortname."_main_menu_background",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main menu background",),		
	
array( "name" => "Main menu - color",
		"id" => $shortname."_main_menu_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main menu color",),		
		
array( "name" => "Main menu - color on hover",
		"id" => $shortname."_main_menu_hover_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main menu color on hover",),	
		
array( "name" => "Main menu active & hover - background colors",
		"id" => $shortname."_active_menu_background",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main menu active & hover links background color",),			

array( "name" => "Main menu - active & hover colors",
		"id" => $shortname."_active_menu_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main menu active & hover links color",),	
		
array( "name" => "Main sub menu - color",
		"id" => $shortname."_main_sub_menu_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main sub menu color",),		
		
array( "name" => "Main sub menu - color on hover",
		"id" => $shortname."_main_sub_menu_hover_color",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main sub menu color on hover",),			
	
array( "name" => "Main sub menu - background color",
		"id" => $shortname."_main_sub_menu_background",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main sub menu background color",),		
		
array( "name" => "Main sub menu - background color on hover",
		"id" => $shortname."_main_sub_menu_hover_background",
		"type" => "textcolorpopup",
		"std" => "",
		"desc" => "Choose a main sub menu background color on hover",),	
			
	
array( "type" => "clearfix",),
array( "name" => "General_5",
	    "type" => "content-close",),
		

		
array( "name" => "General_52",
		"type" => "content-open",),	

array( "name" => "Header banner (728px max)",
        "id" => $shortname."_banner_header",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display banner (468x60px) in header."),
			
array( "type" => "clearfix",),		
	
array( "name" => "Banner size",
        "id" => $shortname."_banner_size",
		"std" => "",
        "type" => "select",
		"options" => array("468x60px","728x90px"),
		"desc" => "Select a banner size"),		
	
array( "name" => "Header - banner code",
        "id" => $shortname."_banner_header_code",
		"std" => "",
        "type" => "textarea",
		"desc" => "Enter code for banner in header."),
	
	
	
array( "name" => "Header banner - Full width (1140px max)",
        "id" => $shortname."_full_width_banner",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Full width banner (1140px max wdith) in header."),
			
array( "type" => "clearfix",),			
	
array( "name" => "Banner (Full width) - layout",
        "id" => $shortname."_banner_fw_layout",
		"std" => "",
        "type" => "select",
		"options" => array("Boxed","Full width"),
		"desc" => "Select a banner layout"),	
	
	
array( "name" => "Header banner (Full width) - Code",
        "id" => $shortname."_full_width_code",
		"std" => "",
        "type" => "textarea",
		"desc" => "Enter code for Full width banner in header."),
	
	

array( "name" => "Header banner rotator (instead banner 728px)",
        "id" => $shortname."_banner_rotator",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display banner rotator in header."),
			
array( "type" => "clearfix",),
		
array( "name" => "Header banner rotator - Timer",
        "id" => $shortname."_rotator_timer",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option to display banner rotator Timer."),
			
array( "type" => "clearfix",),	


array( "name" => "Banner rotator - Image 1",
		"id" => $shortname."_rotator_image_1",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to Banner rotator image 1"),			 							

array( "type" => "clearfix",),	

array( "name" => "Banner rotator - Title 1",
        "id" => $shortname."_br_title_1",
        "type" => "text",
		"desc" => "Write Title for Banner 1"),	
	
array( "name" => "Banner rotator - Link 1",
        "id" => $shortname."_br_link_1",
        "type" => "text",
		"desc" => "Write Link for Banner 1"),	

array( "name" => "Banner rotator - Image 2",
		"id" => $shortname."_rotator_image_2",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to Banner rotator image 2"),			 							

array( "type" => "clearfix",),	

array( "name" => "Banner rotator - Title 2",
        "id" => $shortname."_br_title_2",
        "type" => "text",
		"desc" => "Write Title for Banner 2"),	
	
array( "name" => "Banner rotator - Link 2",
        "id" => $shortname."_br_link_2",
        "type" => "text",
		"desc" => "Write Link for Banner 2"),	
		
array( "name" => "Banner rotator - Image 3",
		"id" => $shortname."_rotator_image_3",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to Banner rotator image 3"),			 							

array( "type" => "clearfix",),	

array( "name" => "Banner rotator - Title 3",
        "id" => $shortname."_br_title_3",
        "type" => "text",
		"desc" => "Write Title for Banner 3"),	
	
array( "name" => "Banner rotator - Link 3",
        "id" => $shortname."_br_link_3",
        "type" => "text",
		"desc" => "Write Link for Banner 3"),	

array( "name" => "Banner rotator - Image 4",
		"id" => $shortname."_rotator_image_4",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to Banner rotator image 4"),			 							

array( "type" => "clearfix",),	

array( "name" => "Banner rotator - Title 4",
        "id" => $shortname."_br_title_4",
        "type" => "text",
		"desc" => "Write Title for Banner 4"),	
	
array( "name" => "Banner rotator - Link 4",
        "id" => $shortname."_br_link_4",
        "type" => "text",
		"desc" => "Write Link for Banner 4"),	

array( "name" => "Banner rotator - Image 5",
		"id" => $shortname."_rotator_image_5",
		"type" => "upload",
		"std" => "",
		"desc" => "Enter the full path to Banner rotator image 5"),			 							

array( "type" => "clearfix",),	

array( "name" => "Banner rotator - Title 5",
        "id" => $shortname."_br_title_5",
        "type" => "text",
		"desc" => "Write Title for Banner 5"),	
	
array( "name" => "Banner rotator - Link 5",
        "id" => $shortname."_br_link_5",
        "type" => "text",
		"desc" => "Write Link for Banner 5"),	



array( "name" => "Category banner (728x90px)",
        "id" => $shortname."_banner_index",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display banner (728x90px) in category."),
			
array( "type" => "clearfix",),		
	
array( "name" => "Category - banner code",
        "id" => $shortname."_banner_index_code",
		"std" => "",
        "type" => "textarea",
		"desc" => "Enter code for banner in category."),		
		

array( "name" => "Single banner (728x90px)",
        "id" => $shortname."_banner_single",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display banner (728x90px) in Single post."),
			
array( "type" => "clearfix",),		
	
array( "name" => "Single - banner code",
        "id" => $shortname."_banner_single_code",
		"std" => "",
        "type" => "textarea",
		"desc" => "Enter code for banner in Single post."),		
		
				
		

array( "name" => "Footer banner",
        "id" => $shortname."_banner_footer",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display banner (728x90px) in footer."),
			
array( "type" => "clearfix",),		
	
array( "name" => "Footer - banner code",
        "id" => $shortname."_banner_footer_code",
		"std" => "",
        "type" => "textarea",
		"desc" => "Enter code for banner in Footer."),
	
array( "name" => "Social bookmarks in header",
        "id" => $shortname."_social",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display social bookmarks."),
		
array( "type" => "clearfix",),				
		
array( "name" => "Twitter subscribe",
        "id" => $shortname."_twitter",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Twitter subscribe."),
			
array( "type" => "clearfix",),	
	
array( "name" => "Twitter path",
        "id" => $shortname."_twitter_id",
        "type" => "text",
		"desc" => "Enter the full path to your Twitter account"),	
		
array( "name" => "Facebook subscribe",
        "id" => $shortname."_facebook",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Facebook subscribe."),
			
array( "type" => "clearfix",),		
		
array( "name" => "Facebook path",
        "id" => $shortname."_facebook_id",
        "type" => "text",
		"desc" => "Enter the full path to your Facebook account"),	
	
array( "name" => "Pinterest subscribe",
        "id" => $shortname."_pinterest",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Pinterest subscribe."),
			
array( "type" => "clearfix",),		
		
array( "name" => "Pinterest path",
        "id" => $shortname."_pinterest_id",
        "type" => "text",
		"desc" => "Enter the full path to your Pinterest account"),	
	

array( "name" => "Google+ subscribe",
        "id" => $shortname."_google_plus",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Google+ subscribe."),
			
array( "type" => "clearfix",),		
		
array( "name" => "Google+ path",
        "id" => $shortname."_google_plus_id",
        "type" => "text",
		"desc" => "Enter the full path to your Google+ account"),		

array( "name" => "Linkedin subscribe",
        "id" => $shortname."_linkedin",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Linkedin subscribe."),	
		
array( "type" => "clearfix",),		
	
array( "name" => "Linkedin path",
        "id" => $shortname."_linkedin_id",
        "type" => "text",
		"desc" => "Enter the full path to your Linkedin account"),	
	
array( "name" => "Vimeo subscribe",
        "id" => $shortname."_vimeo",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Vimeo subscribe."),		

array( "type" => "clearfix",),			
	
array( "name" => "Vimeo path",
        "id" => $shortname."_vimeo_id",
        "type" => "text",
		"desc" => "Enter the full path to your Vimeo account"),	
	
array( "name" => "Flickr subscribe",
        "id" => $shortname."_flickr",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Flickr subscribe."),		

array( "type" => "clearfix",),			
	
array( "name" => "Flickr path",
        "id" => $shortname."_flickr_id",
        "type" => "text",
		"desc" => "Enter the full path to your Flickr account"),			
		
array( "name" => "Youtube subscribe",
        "id" => $shortname."_youtube",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Youtube subscribe."),		

array( "type" => "clearfix",),			
	
array( "name" => "Youtube path",
        "id" => $shortname."_youtube_id",
        "type" => "text",
		"desc" => "Enter the full path to your Youtube account"),	

array( "name" => "Skype subscribe",
        "id" => $shortname."_skype",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Skype subscribe."),		

array( "type" => "clearfix",),			
	
array( "name" => "Skype path",
        "id" => $shortname."_skype_id",
        "type" => "text",
		"desc" => "Enter the full path to your Skype account"),			
			
array( "name" => "Instagram subscribe",
        "id" => $shortname."_instagram",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display Instagram subscribe."),		

array( "type" => "clearfix",),			
	
array( "name" => "Instagram path",
        "id" => $shortname."_instagram_id",
        "type" => "text",
		"desc" => "Enter the full path to your Instagram account"),	
	
array( "name" => "VKontakte subscribe",
        "id" => $shortname."_vkontakte",
        "type" => "checkbox",
        "std" => "false",
		"desc" => "Enable this option to display VKontakte subscribe."),		

array( "type" => "clearfix",),			
	
array( "name" => "VKontakte path",
        "id" => $shortname."_vkontakte_id",
        "type" => "text",
		"desc" => "Enter the full path to your VKontakte account"),	
		
array( "type" => "clearfix",),
array( "name" => "General_52",
	    "type" => "content-close",),


		
array( "name" => "General_6",
		"type" => "content-open",),			
		
array( "name" => "Homepage custom title ",
		"id" => $shortname."_seo_home_title",
		"type" => "checkbox",
		"std" => "false",
		"desc" => "Enable this option for display custom title on home page",),

array( "type" => "clearfix",),

array( "name" => "Homepage meta description",
		"id" => $shortname."_seo_home_description",
		"type" => "checkbox",
		"std" => "false",
		"desc" => "Enable this option for display custom meta description on home page",),

array( "type" => "clearfix",),

array( "name" => "Homepage meta keywords",
		"id" => $shortname."_seo_home_keywords",
		"type" => "checkbox",
		"std" => "false",
		"desc" => "Enable this option for display custom meta keywords on home page",),

array( "type" => "clearfix",),
		
array( "name" => "Homepage custom title",
		"id" => $shortname."_seo_home_titletext",
		"type" => "text",
		"std" => "",
		"desc" => "",),

array( "name" => "Homepage meta description",
		"id" => $shortname."_seo_home_descriptiontext",
		"type" => "textarea",
		"std" => "",
		"desc" => "",),

array( "name" => "Homepage meta keywords",
		"id" => $shortname."_seo_home_keywordstext",
		"type" => "text",
		"std" => "",
		"desc" => "",),

array( "name" => "Single custom titles",
		"id" => $shortname."_seo_single_title",
		"type" => "checkbox",
		"std" => "false",
		"desc" => "Enable this option for display custom title on single post and pages",),

array( "name" => "Single custom description",
		"id" => $shortname."_seo_single_description",
		"type" => "checkbox",
		"std" => "false",
		"desc" => "Enable this option for display custom description on single post and pages",),

array( "type" => "clearfix",),

array( "name" => "Single custom keywords",
		"id" => $shortname."_seo_single_keywords",
		"type" => "checkbox",
		"std" => "false",
		"desc" => "Enable this option for display custom keywords on single post and pages",),
				   
				   
array( "type" => "clearfix",),
array( "name" => "General_6",
	    "type" => "content-close",),			
	
		
	
array( "name" => "General_7",
		"type" => "content-open",),				
		
array( "name" => "News ticker text",
        "id" => $shortname."_ticker_text",
		"std" => "Hot News",
        "type" => "text",
		"desc" => "Enter a title for News ticker"),		
			
array( "name" => "Hi text (for login/register form)",
        "id" => $shortname."_hi_text",
		"std" => "Hi",
        "type" => "text",
		"desc" => "Enter a Hi text for login/register form"),		
		
array( "name" => "Logout text (for login/register form)",
        "id" => $shortname."_logout_text",
		"std" => "Logout",
        "type" => "text",
		"desc" => "Enter a Logout text for login/register form"),		
		
array( "name" => "Go to profile (for login/register form)",
        "id" => $shortname."_go_profile_text",
		"std" => "Go to profile",
        "type" => "text",
		"desc" => "Enter a Go to profile text for login/register form"),		
		
array( "name" => "Login (for login/register form)",
        "id" => $shortname."_login_text",
		"std" => "Login",
        "type" => "text",
		"desc" => "Enter a Login text for login/register form"),		
		
array( "name" => "Login or register (for login/register form)",
        "id" => $shortname."_log_or_reg_text",
		"std" => "Login or register",
        "type" => "text",
		"desc" => "Enter a Login or register text for login/register form"),		
		
array( "name" => "Welcome new user! (for login/register form)",
        "id" => $shortname."_welcome_text",
		"std" => "Welcome new user!",
        "type" => "text",
		"desc" => "Enter a Welcome new user! text for login/register form"),	
		
array( "name" => "Sign up (for login/register form)",
        "id" => $shortname."_sign_up_text",
		"std" => "Sign up",
        "type" => "text",
		"desc" => "Enter a Sign up text for login/register form"),		
		
array( "name" => "Back (for login/register form)",
        "id" => $shortname."_back_text",
		"std" => "Back",
        "type" => "text",
		"desc" => "Enter a Back text for login/register form"),		
		
array( "name" => "Username (for login/register form)",
        "id" => $shortname."_username_text",
		"std" => "Username",
        "type" => "text",
		"desc" => "Enter a Username text for login/register form"),		
		
		
array( "name" => "Password (for login/register form)",
        "id" => $shortname."_password_text",
		"std" => "Password",
        "type" => "text",
		"desc" => "Enter a Password text for login/register form"),		
		
array( "name" => "Your Email (for login/register form)",
        "id" => $shortname."_email_text",
		"std" => "Your Email",
        "type" => "text",
		"desc" => "Enter a Your Email text for login/register form"),		
			
array( "name" => "Remember me on this computer (for login/register form)",
        "id" => $shortname."_remember_text",
		"std" => "Remember me on this computer",
        "type" => "text",
		"desc" => "Enter a Remember me on this computer text for login/register form"),		
			
array( "name" => "Lost your password? (for login/register form)",
        "id" => $shortname."_lost_pass_text",
		"std" => "Lost your password?",
        "type" => "text",
		"desc" => "Enter a Lost your password? text for login/register form"),		
	
		
	
array( "name" => "Most Popular posts",
        "id" => $shortname."_popular_posts",
		"std" => "Most Popular posts",
        "type" => "text",
		"desc" => "Enter a Most Popular posts text for Footer Popular posts"),			
		
array( "name" => "Authors",
        "id" => $shortname."_authors_text",
		"std" => "Authors",
        "type" => "text",
		"desc" => "Enter a Authors for Header"),	
		
array( "name" => "View all",
        "id" => $shortname."_view_all",
		"std" => "View all",
        "type" => "text",
		"desc" => "Enter a View all for Header authors box"),			
			
array( "name" => "Search",
        "id" => $shortname."_search",
		"std" => "Search",
        "type" => "text",
		"desc" => "Enter a Search text for search form"),																

array( "name" => "Browse all categories",
        "id" => $shortname."_browse_cats",
		"std" => "Browse all categories",
        "type" => "text",
		"desc" => "Enter a Browse all categories text for Select categories form"),									
			
array( "name" => "Read more",
        "id" => $shortname."_read_more",
		"std" => "Read more",
        "type" => "text",
		"desc" => "Enter a Read more text for posts"),		
	
array( "name" => "View more",
        "id" => $shortname."_view_more",
		"std" => "View more",
        "type" => "text",
		"desc" => "Enter a View more text for Homepage shortcodes title"),	

array( "name" => "View more in",
        "id" => $shortname."_view_more_in_category",
		"std" => "View more in",
        "type" => "text",
		"desc" => "Enter a View more text for Homepage shortcodes categories"),	
		
		
array( "name" => "Home",
        "id" => $shortname."_home_text",
		"std" => "Home",
        "type" => "text",
		"desc" => "Enter a Home text for breadcrumbs"),		

array( "name" => "Ago",
        "id" => $shortname."_time_ago",
		"std" => "ago",
        "type" => "text",
		"desc" => "Enter a ago text for all dates"),			
		
array( "name" => "Posts Found",
        "id" => $shortname."_posts_found",
		"std" => "Posts Found",
        "type" => "text",
		"desc" => "Enter a Posts Found for all homepage shortcodes"),				
		
array( "name" => "Search results for",
        "id" => $shortname."_search_result",
		"std" => "Search results for",
        "type" => "text",
		"desc" => "Enter a Search results text for breadcrumbs"),				
		
array( "name" => "Posts tagged",
        "id" => $shortname."_posts_tagged",
		"std" => "Posts tagged",
        "type" => "text",
		"desc" => "Enter a Posts tagged text for breadcrumbs"),			

array( "name" => "Articles posted by",
        "id" => $shortname."_posted_author",
		"std" => "Articles posted by",
        "type" => "text",
		"desc" => "Enter a Articles posted by text for breadcrumbs"),	
		
array( "name" => "Error 404",
        "id" => $shortname."_error_404",
		"std" => "Error 404",
        "type" => "text",
		"desc" => "Enter a Error 404 text for breadcrumbs"),		
	
array( "name" => "Page (for navigation)",
        "id" => $shortname."_page_navi",
		"std" => "Page",
        "type" => "text",
		"desc" => "Enter a Page text for breadcrumbs"),		
		
array( "name" => "Comments",
        "id" => $shortname."_comments_text",
		"std" => "Comments",
        "type" => "text",
		"desc" => "Enter a Comments text for posts"),				

array( "name" => "Author:",
        "id" => $shortname."_signle_author",
		"std" => "Author:",
        "type" => "text",
		"desc" => "Enter a Author text"),			
		
array( "name" => "Total posts:",
        "id" => $shortname."_total_posts",
		"std" => "Total posts:",
        "type" => "text",
		"desc" => "Enter a Total posts"),	
		
array( "name" => "Author",
        "id" => $shortname."_author",
		"std" => "Written by",
        "type" => "text",
		"desc" => "Enter a Written by text"),	
	

array( "name" => "View all posts (for Single post)",
        "id" => $shortname."_view_athor_posts_single",
		"std" => "View all posts",
        "type" => "text",
		"desc" => "Enter a View all posts text"),	
		
array( "name" => "View all posts by",
        "id" => $shortname."_view_athor_posts",
		"std" => "View all posts by",
        "type" => "text",
		"desc" => "Enter a View all posts by text"),	

array( "name" => "Read next",
        "id" => $shortname."_read_next",
		"std" => "Read next",
        "type" => "text",
		"desc" => "Write a Read next text for single posts pagination"),	
			
array( "name" => "Read previous",
        "id" => $shortname."_read_previous",
		"std" => "Read previous",
        "type" => "text",
		"desc" => "Write a Read previous text for single posts pagination"),		
		
array( "name" => "Previous article",
        "id" => $shortname."_prev_post",
		"std" => "Previous article",
        "type" => "text",
		"desc" => "Write a Previous article text for single posts"),
		
array( "name" => "Next article",
        "id" => $shortname."_next_post",
		"std" => "Next article",
        "type" => "text",
		"desc" => "Write a Next article text for single posts"),
		
array( "name" => "Tags",
        "id" => $shortname."_tags_simple_text",
		"std" => "Tags",
        "type" => "text",
		"desc" => "Write a Tags text for single posts"),
			
array( "name" => "Similar posts",
        "id" => $shortname."_more_news",
		"std" => "Similar posts",
        "type" => "text",
		"desc" => "Enter a Similar posts text for single posts"),			
	
array( "name" => "30 Recent Posts (sitemap)",
        "id" => $shortname."_30_recent_posts",
		"std" => "30 Recent Posts",
        "type" => "text",
		"desc" => "Enter a 30 Recent Posts text for sitemap"),		
	
array( "name" => "Error 404 - Page not found",
        "id" => $shortname."_page_not_found",
		"std" => "Error 404 - Page not found!",
        "type" => "text",
		"desc" => "Enter a Error 404 - Page not found text for 404 page"),			
	
array( "name" => "Try searching",
        "id" => $shortname."_try_searching",
		"std" => "Try searching:",
        "type" => "text",
		"desc" => "Enter a Try searching text for 404 page"),

array( "name" => "Or look in the archives",
        "id" => $shortname."_look_archives",
		"std" => "Or look in the archives:",
        "type" => "text",
		"desc" => "Enter a Or look in the archives text for 404 page"),		

array( "name" => "Nothing Found",
        "id" => $shortname."_nothing_found",
		"std" => "Nothing Found!",
        "type" => "text",
		"desc" => "Enter a Nothing Found text for search page"),	
		
array( "name" => "Text for search page",
		"id" => $shortname."_nothing_found_text",
		"type" => "textarea",
		"std" => "Sorry, but nothing matched your search criteria. Please try again with some different keywords.",
		"desc" => "Enter the text what will be displayed on search page",),			

array( "name" => "Your name (contact form)",
        "id" => $shortname."_your_name",
		"std" => "Your name: *",
        "type" => "text",
		"desc" => "Enter a Your name text for contact form"),			
		
array( "name" => "Your email (contact form)",
        "id" => $shortname."_your_email",
		"std" => "Your email: *",
        "type" => "text",
		"desc" => "Enter a Your email text for contact form"),		
	
array( "name" => "Your message (contact form)",
        "id" => $shortname."_your_message",
		"std" => "Your message: *",
        "type" => "text",
		"desc" => "Enter a Your message text for contact form"),		
		
array( "name" => "Submit - button (contact form)",
        "id" => $shortname."_contact_submit",
		"std" => "Submit",
        "type" => "text",
		"desc" => "Enter a Submit text for contact form"),	
	
array( "name" => "Your email was successfully sent (popup)",
        "id" => $shortname."_successfully_sent",
		"std" => "Your email was successfully sent!",
        "type" => "text",
		"desc" => "Enter a Successfully sent text for contact form"),		

array( "name" => "Wrong data (popup)",
        "id" => $shortname."_wrong_data",
		"std" => "Please enter your name, a message and a valid email address!",
        "type" => "text",
		"desc" => "Enter a Successfully sent text for contact form"),				
	
array( "name" => "Footer copy text",
        "id" => $shortname."_footer_copy",
		"std" => "All rights reserved",
        "type" => "text",
		"desc" => "Enter copy text for footer."),	
	
array( "type" => "clearfix",),			
array( "name" => "General_7",
	    "type" => "content-close",),
				
		
		
array( "name" => "General_8",
		"type" => "content-open",),			
		
array( "name" => "Boxed layout",
        "id" => $shortname."_theme_full",
        "type" => "checkbox",
        "std" => "on",
		"desc" => "Enable this option to display Full width Theme layout."),
			
array( "type" => "clearfix",),	

array( 	"name" => "Boxed layout - Top margin",
		"id" => $shortname."_top_margin",
		"type" => "select",
		"std" => "",
        "options" => array('0px', '10px', '20px', '30px', '40px', '50px', '60px', '70px', '80px', '90px', '100px', '150px', '200px'),
		"desc" => "Select the position for Top menu."),			
		
array( 	"name" => "Header - Layout",
		"id" => $shortname."_header_layout",
		"type" => "select",
		"std" => "",
        "options" => array('Logo Center', 'Logo + Banner'),
		"desc" => "Select a variant for header."),		
		
		
array( 	"name" => "Secondary menu (Top menu) - Position",
		"id" => $shortname."_top_menu_position",
		"type" => "select",
		"std" => "",
        "options" => array('Top', 'Bottom'),
		"desc" => "Select the position for Top menu."),	
		
		
array( 	"name" => "News Ticker - Position",
		"id" => $shortname."_ticker_position",
		"type" => "select",
		"std" => "",
        "options" => array('Bottom', 'Top'),
		"desc" => "Select the position for News Ticker."),	
		
		
		
array( 	"name" => "News Ticker - Layout",
		"id" => $shortname."_newsticker_layout",
		"type" => "select",
		"std" => "",
        "options" => array('Simple', 'With Images'),
		"desc" => "Select a variant for News Ticker."),	
		
array( 	"name" => "Single post - Layout",
		"id" => $shortname."_single_post_layout",
		"type" => "select",
		"std" => "",
        "options" => array('With 1 sidebar', 'Full width'),
		"desc" => "Select a Layout for the Single post."),		
		
array( 	"name" => "Color gradient for images - Colors",
		"id" => $shortname."_gradient_color",
		"type" => "select",
		"std" => "",
        "options" => array('Yellow-Green', 'Green-Yellow', 'Orange-Red', 'Red-Orange', 'Green-Blue', 'Blue-Green', 'Red-Purple', 'Purple-Red', 'Grey'),
		"desc" => "Select a gradient for images."),			
		
		
		
array( "type" => "clearfix",),			
array( "name" => "General_8",
	    "type" => "content-close",),
		
		
//-------------------------------------------------------------------------------------//	
			
);

function custom_colors_css(){
	global $shortname; ?>
	
<style type="text/css">

<?php if (get_option('op_custom_header_font') == '') { ?>

h1, h2, h3, h4, h5, h6, .post-like, .photo_content_box, .lightslider_caption, .cat_title_box, .car_title_descr, .popular_posts_title, .big_sim_title_box, .short_post_author a, .mt-label, .header_post_time, .sw-skin3 .sw-content .sw-caption h4, #skin_3 p, #wowslider-container1 .ws-title span, .home_custom_block_title, .title_lp, #header_login_box, .cat_view_more,  .lof-slidecontent .slider-description .slider-meta, .widget_title_third, .widget_two_meta, #top_title_box, .image_carousel_post, .site_title h1, #homeSlider .rsTmb, .fn, .menu-item, .cat_post_count, .tagcloud a, .author_description span, #tags_simple a, .tags_simple_text, #post_tags li a, .custom_read_more, .product_list_widget li a, .product-categories, .post-single-rate, .widget_title, .full_widget_title, .widget_title_two, #home_carousel .jcarousel-skin-tango .carousel_post h1 a, .home_posts_title h2, #header_top_menu .logout, #header_top_menu .login, #mainMenu ul li a, #mega_main_menu .link_text, #secondaryMenu, .mt-news .ticker_title, .slide_time, .post h1 a, .product h1 a, .cat_meta, .category_time, .post_views, .cat_author, .custom_cat_class, .post_meta_line, .par_post_meta_line, .post_time, .latest_title_box, #single_recent_posts .recent_posts_title, .prev_link_title a, .next_link_title a, #navigation_images span, .author_posts_title, #archive .arch_title, .fn a, .comment-meta, #submit, #content_bread_panel, #contact input[type="submit"], h2.widgettitle, .widget.widget_nav_menu li a, .widget_menu_title, .yop-poll-container label, .yop_poll_vote_button, .widget_footer_title, .wpt_widget_content .tab_title a, #comments-tab-content .wpt_comment_meta a, .user-rate-wrap, #bbp_search_submit, .forum-titles, .bbp-forum-title, .entry-title, .bbp-topic-permalink, .bbp-submit-wrapper button, .wpb_row h1, .wpb_row h2, .wpb_row h3, .wpb_row h4, .wpb_row h5, .ts-icon-title-text, .home_posts_time, .home_masonry_posts .home_posts_time, .video_post .masonry_title, .line_title, .video_post .video_time, .home_video_posts_time, .video_title, .column_post:first-child .column_title, .sp-caption-container, .column_title, .blog_title, .mega-hovertitle, .ms-videogallery-template .ms-layer.video-title, .ms-videogallery-template .ms-layer.video-author, .sb-modern-skin .showbiz-title, .masonry_title, .main_title, .pbc_title, .sb-retro-skin .showbiz-title a, #similar-post, .widget_date, .full_widget_date, .widget_date_third, .popular_widget_title, .widget_title_small, .widget_title_small_second, .comment-reply-link, .comments_count_simple_box, .blog_fashion_comments, .tp-poll-container .choice-content, .tp-poll-container .buttons button, #commentform label, .logged-in-as, .header_title, .single_text .single_thumbnail_small ul li a {font-family:<?php echo(get_option($shortname.'_header_font')); ?>;}

<?php } else { ?>

h1, h2, h3, h4, h5, h6, .post-like, .photo_content_box, .lightslider_caption, .cat_title_box, .car_title_descr, .popular_posts_title, .big_sim_title_box, .short_post_author a, .mt-label, .header_post_time, .sw-skin3 .sw-content .sw-caption h4, #skin_3 p, #wowslider-container1 .ws-title span, .home_custom_block_title, .title_lp, #header_login_box, .cat_view_more,  .lof-slidecontent .slider-description .slider-meta, .widget_title_third, .widget_two_meta, #top_title_box, .image_carousel_post, .site_title h1, #homeSlider .rsTmb, .fn, .menu-item, .cat_post_count, .tagcloud a, .author_description span, #tags_simple a, .tags_simple_text, #post_tags li a, .custom_read_more, .product_list_widget li a, .product-categories, .post-single-rate, .widget_title, .full_widget_title, .widget_title_two, #home_carousel .jcarousel-skin-tango .carousel_post h1 a, .home_posts_title h2, #header_top_menu .logout, #header_top_menu .login, #mainMenu ul li a, #mega_main_menu .link_text, #secondaryMenu, .mt-news .ticker_title, .slide_time, .post h1 a, .product h1 a, .cat_meta, .category_time, .post_views, .cat_author, .custom_cat_class, .post_meta_line, .par_post_meta_line, .post_time, .latest_title_box, #single_recent_posts .recent_posts_title, .prev_link_title a, .next_link_title a, #navigation_images span, .author_posts_title, #archive .arch_title, .fn a, .comment-meta, #submit, #content_bread_panel, #contact input[type="submit"], h2.widgettitle, .widget.widget_nav_menu li a, .widget_menu_title, .yop-poll-container label, .yop_poll_vote_button, .widget_footer_title, .wpt_widget_content .tab_title a, #comments-tab-content .wpt_comment_meta a, .user-rate-wrap, #bbp_search_submit, .forum-titles, .bbp-forum-title, .entry-title, .bbp-topic-permalink, .bbp-submit-wrapper button, .wpb_row h1, .wpb_row h2, .wpb_row h3, .wpb_row h4, .wpb_row h5, .ts-icon-title-text, .home_posts_time, .home_masonry_posts .home_posts_time, .video_post .masonry_title, .line_title, .video_post .video_time, .home_video_posts_time, .video_title, .column_post:first-child .column_title, .sp-caption-container, .column_title, .blog_title, .mega-hovertitle, .ms-videogallery-template .ms-layer.video-title, .ms-videogallery-template .ms-layer.video-author, .sb-modern-skin .showbiz-title, .masonry_title, .main_title, .pbc_title, .sb-retro-skin .showbiz-title a, #similar-post, .widget_date, .full_widget_date, .widget_date_third, .popular_widget_title, .widget_title_small, .widget_title_small_second, .comment-reply-link, .comments_count_simple_box, .blog_fashion_comments, .tp-poll-container .choice-content, .tp-poll-container .buttons button, #commentform label, .logged-in-as, .header_title, .single_text .single_thumbnail_small ul li a {font-family:<?php echo(get_option($shortname.'_custom_header_font')); ?>;}

<?php } ?>

	#wowslider-container1 .ws-title div, .feat_post_box p, .right-widget, .home_custom_block_desc, .post p, .product p, .single_text, .featured_area_content_text, .bx-wrapper .bxslider_quote, #home_content, #container, .post_one_column h1, .post_mini_one_column h1, .post_two_column h1, .post_three_column h1, .video_widget { font-family:<?php echo(get_option($shortname.'_text_font')); ?>; }	
    
	<?php if (get_option($shortname.'_headers_color') !== "") { ?>
	.single_text h1, .single_text h2, .single_text h3, .single_text h4, .single_text h5, .single_text h6 { color: #<?php echo(get_option($shortname.'_headers_color')); ?>!important; }
	<?php } ?>
    <?php if (get_option($shortname.'_site_title_color') !== "") { ?>
	.site_title h1 { color: #<?php echo(get_option($shortname.'_site_title_color')); ?>!important; } 
	<?php } ?>
    <?php if (get_option($shortname.'_site_title_color_hover') !== "") { ?>
	.site_title h1:hover { color: #<?php echo(get_option($shortname.'_site_title_color_hover')); ?>!important; } 
    <?php } ?>
	
    <?php if (get_option($shortname.'_sidebar_headers_color') !== "") { ?>
	.right-heading h4 { color: #<?php echo(get_option($shortname.'_sidebar_headers_color')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_sidebar_footer_headers_color') !== "") { ?>
	.footer-heading h4 { color: #<?php echo(get_option($shortname.'_sidebar_footer_headers_color')); ?>!important; } 
    <?php } ?>
	
    <?php if (get_option($shortname.'_crumbs_color') !== "") { ?>
	#crumbs, #crumbs a{ color: #<?php echo(get_option($shortname.'_crumbs_color')); ?>!important; }
    <?php } ?>
    <?php if (get_option($shortname.'_crumbs_color_hover') !== "") { ?>
	#crumbs a:hover { color: #<?php echo(get_option($shortname.'_crumbs_color_hover')); ?>!important; }
    <?php } ?>
	
    <?php if (get_option($shortname.'_news_ticker_bg') !== "") { ?>
	.ticker_box { background-color: #<?php echo(get_option($shortname.'_news_ticker_bg')); ?>!important; }
    <?php } ?>

    <?php if (get_option($shortname.'_news_ticker_title') !== "") { ?>
	.ticker_box .mt-label { color: #<?php echo(get_option($shortname.'_news_ticker_title')); ?>!important; }
    <?php } ?>
	
    <?php if (get_option($shortname.'_news_ticker_links') !== "") { ?>
	.mt-news .ticker_title { color: #<?php echo(get_option($shortname.'_news_ticker_links')); ?>!important; }
    <?php } ?>

    <?php if (get_option($shortname.'_news_ticker_category') !== "") { ?>
	.ticker_box .custom_cat_class { color: #<?php echo(get_option($shortname.'_news_ticker_category')); ?>!important; }
    <?php } ?>
	
    <?php if (get_option($shortname.'_custom_cat_class_color') !== "") { ?>
    .footer_popular_posts_box:first-child:hover .custom_cat_class, .footer_popular_posts_box .custom_cat_class, .feat_post_box:first-child:hover .custom_cat_class, .feat_post_box .custom_cat_class, .feat_post_box_one:first-child:hover .custom_cat_class, .feat_post_box_one .custom_cat_class, .feat_post_box_two:first-child:hover .custom_cat_class, .feat_post_box_two .custom_cat_class, .feat_post_box_three:first-child:hover .custom_cat_class, .feat_post_box_three .custom_cat_class, .feat_post_box_four:first-child:hover .custom_cat_class, .feat_post_box_four .custom_cat_class, #wowslider-container1 .category_box .custom_cat_class:hover, #wowslider-container1 .category_box .custom_cat_class{ color: #<?php echo(get_option($shortname.'_custom_cat_class_color')); ?>!important; }
    <?php } ?>

    <?php if (get_option($shortname.'_header_bg') !== "") { ?>
	#header, #dc_jqaccordion_widget-7-item ul ul li a { background-color: #<?php echo(get_option($shortname.'_header_bg')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_header_top_panel_bg') !== "") { ?>
	#header_top_menu { background-color: #<?php echo(get_option($shortname.'_header_top_panel_bg')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_header_top_panel_color') !== "") { ?>
	#secondaryMenu ul li a, #header_top_menu .user_profile, #header_top_menu .user_profile_logout, #header_top_menu #modal_trigger { color: #<?php echo(get_option($shortname.'_header_top_panel_color')); ?>!important; } 
    <?php } ?>	
	
    <?php if (get_option($shortname.'_footer_popular_posts_bg') !== "") { ?>
	#footer_popular_posts { background-color: #<?php echo(get_option($shortname.'_footer_popular_posts_bg')); ?>!important; } 
    <?php } ?>		

	
    <?php if (get_option($shortname.'_main_menu_color') !== "") { ?>
	#mainMenu ul li a { color: #<?php echo(get_option($shortname.'_main_menu_color')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_main_menu_hover_color') !== "") { ?>
	#mainMenu ul li a:hover, #mainMenu ul li.current-menu-parent > a, #mainMenu ul li.current_page_item > a, #mainMenu ul li.current-menu-ancestor > a, #mainMenu ul li.current-menu-item > a, #mainMenu ul li a:hover { color: #<?php echo(get_option($shortname.'_main_menu_hover_color')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_active_menu_background') !== "") { ?>
	#mainMenu ul li.current-menu-parent > a, #mainMenu ul li.current_page_item > a, #mainMenu ul li.current-menu-ancestor > a, #mainMenu ul li.current-menu-item > a, #mainMenu ul li a:hover { background: #<?php echo(get_option($shortname.'_active_menu_background')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_active_menu_color') !== "") { ?>
	#mainMenu ul li.current-menu-parent > a, #mainMenu ul li.current_page_item > a, #mainMenu ul li.current-menu-ancestor > a, #mainMenu ul li.current-menu-item > a, #mainMenu ul li a:hover { color: #<?php echo(get_option($shortname.'_active_menu_color')); ?>!important; } 	
    <?php } ?>
    <?php if (get_option($shortname.'_main_sub_menu_color') !== "") { ?>
	#mainMenu.ddsmoothmenu ul li ul li a { color: #<?php echo(get_option($shortname.'_main_sub_menu_color')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_main_sub_menu_hover_color') !== "") { ?>
	#mainMenu.ddsmoothmenu ul li ul li a:hover { color: #<?php echo(get_option($shortname.'_main_sub_menu_hover_color')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_main_sub_menu_background') !== "") { ?>
	#mainMenu.ddsmoothmenu ul li ul li a, #mainMenu.ddsmoothmenu ul li ul li.current-menu-ancestor > a, #mainMenu.ddsmoothmenu ul li ul li.current-menu-item > a { background: #<?php echo(get_option($shortname.'_main_sub_menu_background')); ?>!important; } 
    <?php } ?>
    <?php if (get_option($shortname.'_main_sub_menu_hover_background') !== "") { ?>
	#mainMenu.ddsmoothmenu ul li ul li a:hover { background: #<?php echo(get_option($shortname.'_main_sub_menu_hover_background')); ?>!important; } 
    <?php } ?>

    <?php if (get_option($shortname.'_footer_background') !== "") { ?>
	#footer_box, .footer-heading h3 { background: #<?php echo(get_option($shortname.'_footer_background')); ?>!important; }
    <?php } ?>
    <?php if (get_option($shortname.'_footer_bottom_background') !== "") { ?>
	#footer_bottom { background: #<?php echo(get_option($shortname.'_footer_bottom_background')); ?>!important; }
    <?php } ?>

	.single_title h1 { font-size: <?php echo(get_option($shortname.'_title_size')); ?>px!important; } 
	
	.post .custom_read_more a, .feat_post_title .gallery_meta .home_posts_time, .feat_post_title .gallery_meta .home_posts_cats_box .custom_cat_class, .big_similar_post .short_image_shadow, .cat_feat_post .feat_post_shadow_box, .post .short_image_shadow, .cat_feat_post:hover .feat_post_shadow_box, .cat_feat_post .post-single-rate, .cat_feat_post .post-single-rate-points, .cat_feat_post .post-single-rate-stars, .post .post-single-rate, .post .post-single-rate-stars, .post .post-single-rate-points, .image_carousel .prev, .image_carousel .next, #authorlist .view_all_contr_text, .timer > #slice > .pie { background-color: #<?php echo(get_option($shortname.'_theme_colors')); ?>!important; }
    .cat_title_box, .right-heading h4, .timer > #slice > .pie, .archive_title h3 { border-color: #<?php echo(get_option($shortname.'_theme_colors')); ?>!important; }
	
	#all_content.boxed_width { margin-top: <?php echo(get_option($shortname.'_top_margin')); ?>!important; } 	
	
    <?php if (get_option($shortname.'_gradient_color') !== "") { ?>
	.photo_bg, .widget_menu_title, .full_width_posts_widget li:first-child .full_widget_shadow, .widget_two_shadow, .car_image_caption{ 
	background: url("<?php echo get_template_directory_uri() . '/images/' . (get_option($shortname.'_gradient_color')); ?>.png") repeat-x center center!important; }	
    <?php } ?>
	

</style>

<?php }; ?>
