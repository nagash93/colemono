<?php
/*
Template Name: Error 404
*/
?>

<?php get_header(); ?>

<div id="main_content"> 

<?php if (get_option('op_crumbs') == 'on') { ?>
<?php if (get_option('op_boxed_menu_ticker') == 'on') { ?> 
<div class="inner boxed_ticker_menu">
<?php } ?>
<div id="content_bread_panel">	
<div class="inner">
<?php if (function_exists('wp_breadcrumbs')) wp_breadcrumbs(); ?>
</div>
</div>
<?php if (get_option('op_boxed_menu_ticker') == 'on') { ?> 
</div>
<?php } ?>
<?php } ?>

<div class="inner">

<?php if (get_option('op_ss_position') == 'Left') { ?>
<?php if (get_option('op_timeline_ss') == 'Timeline') { ?>
<?php get_template_part('includes/left_side'); ?>
<?php } else { ?>
<?php get_sidebar('left'); ?>	
<?php } ?>
<?php } ?>

<div id="content" style="width:auto;"class="content_with_one_sidebar">

<div class="error404_content">
	<iframe style="width:100%;height:100vh"src="//www.youtube.com/embed/-niF87ZZjlY?rel=0&autoplay=1&autohide=2&controls=0&fs=0&iv_load_policy=3&disablekb=1&modestbranding=1&rel=0" frameborder="0" allowfullscreen></iframe>

<h1 class="error404"><?php echo get_option('op_page_not_found'); ?></h1>

<h1><?php echo get_option('op_try_searching'); ?></h1>
<?php get_search_form(); ?>

<h1><?php echo get_option('op_look_archives'); ?></h1>

<?php get_template_part('includes/archive_layout'); ?>

</div>		 

</div>

</div>
</div>

<div class="clear"></div>
	
<?php get_footer(); ?>

