
<div id="header_top_menu">
<div class="inner">

	<?php if ( function_exists( 'wp_nav_menu' ) ){
        wp_nav_menu( array('theme_location' => 'top-menu', 'container_id' => 'secondaryMenu', 'fallback_cb'=>'secondarymenu' )); 
        } else { secondarymenu();
	} ?>
	
	
	
    <?php if (get_option('op_social') == 'on') { ?>

	<div class="social_box">
	<?php if (get_option('op_google_plus') == 'on') { ?>
    <div>
	<a href="<?php echo get_option('op_google_plus_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/google.png'; ?>" alt="Google+ " title="Google+" width="32" height="32" />
	</a>
	</div>
	<?php } ?>
	
    <?php if (get_option('op_twitter') == 'on') { ?>
	<div>
	<a href="<?php echo get_option('op_twitter_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/twitter.png'; ?>" alt="Twitter " title="Twitter" width="32" height="32" 
	/>
	</a>
	</div>
	<?php } ?>
	
    <?php if (get_option('op_facebook') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_facebook_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/facebook.png'; ?>" alt="Facebook " title="Facebook" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
	<?php if (get_option('op_instagram') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_instagram_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/instagram.png'; ?>" alt="Instagram " title="Instagram" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
    <?php if (get_option('op_pinterest') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_pinterest_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/pinterest.png'; ?>" alt="Pinterest " title="Pinterest" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
    <?php if (get_option('op_linkedin') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_linkedin_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/linkedin.png'; ?>" alt="Linkedin " title="Linkedin" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
    <?php if (get_option('op_youtube') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_youtube_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/youtube.png'; ?>" alt="Youtube " title="Youtube" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
	<?php if (get_option('op_vimeo') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_vimeo_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/vimeo.png'; ?>" alt="Vimeo " title="Vimeo"	width="32" height="32" />
	</a>
	</div>
    <?php } ?>

	<?php if (get_option('op_flickr') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_flickr_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/flickr.png'; ?>" alt="Flickr " title="Flickr" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
	<?php if (get_option('op_skype') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_skype_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/skype.png'; ?>" alt="Skype " title="Skype" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
	<?php if (get_option('op_vkontakte') == 'on') { ?>
	<div>
    <a href="<?php echo get_option('op_vkontakte_id') ?>">
	<img class="xyz tip" src="<?php echo get_template_directory_uri() . '/images/vkontakte.png'; ?>" alt="VKontakte " title="VKontakte" width="32" height="32" />
	</a>
	</div>
    <?php } ?>
	
    </div>

    <?php } ?>
	
	
	<?php if (get_option('op_header_login') == 'on') { ?>
	
	<?php if ( is_user_logged_in() ) { ?>
	<div id="header_login_box">
	<a class="user_profile tip" href="<?php echo get_edit_user_link(); ?>" title="<?php echo get_option('op_go_profile_text'); ?>" target="_blank">
			<?php
			echo get_option('op_hi_text');
			echo " ";
            global $current_user;
            get_currentuserinfo();
            echo  $current_user->user_login; 
			echo "!";
			?>
	</a>
	<a class="user_profile_logout tip" href="<?php echo wp_logout_url( home_url() ); ?>" title="<?php echo get_option('op_logout_text'); ?>"> <?php echo get_option('op_logout_text'); ?> &raquo;</a>
    </div>
	<?php } else { ?>	
    <div id="header_login_box">
	<a id="modal_trigger" href="#modal" class="btn tip" title="<?php echo get_option('op_log_or_reg_text'); ?>"><?php echo get_option('op_login_text'); ?></a>
    </div>
	<div id="modal" class="popupContainer" style="display:none;">
		<header class="popupHeader">
			<span class="header_title"><?php echo get_option('op_log_or_reg_text'); ?></span>
			<span class="modal_close"><i class="fa fa-times"></i></span>
		</header>
		
		<div class="popupBody">

			<div class="social_login">

				<div class="centeredText">
					<span><?php echo get_option('op_welcome_text'); ?></span>
				</div>

				<div class="action_btns">
					<div class="one_half"><a href="#" id="login_form" class="btn"><?php echo get_option('op_login_text'); ?></a></div>
					<div class="one_half last"><a href="#" id="register_form" class="btn"><?php echo get_option('op_sign_up_text'); ?></a></div>
				</div>
			</div>

			<div class="user_login">
				<form name="loginform" id="loginform" action="<?php echo esc_url( home_url() ); ?>/wp-login.php" method="post">
					<label><?php echo get_option('op_username_text'); ?></label>
					<input type="text" name="log" id="user_log" class="input" value=""  />
					<br />

					<label><?php echo get_option('op_password_text'); ?></label>
					<input type="password" name="pwd" id="user_pass" class="input" value=""  />
					<br />

					<div class="checkbox">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" />
						<label for="remember"><?php echo get_option('op_remember_text'); ?></label>
					</div>

					<div class="action_btns">
						<div class="one_half"><a href="#" class="btn back_btn"><?php echo get_option('op_back_text'); ?></a></div>
						<div class="one_half last">
		                <input type="submit" class="btn btn_red" name="wp-submit" id="wp-submit" value="<?php echo get_option('op_login_text'); ?>" />
		                <input type="hidden" name="redirect_to" value="<?php  echo esc_url( home_url() ); ?>#respond" />
		                <input type="hidden" name="testcookie" value="1" />
                        </div>
						
					</div>
				</form>		
	
            <a href="<?php  echo esc_url( home_url() ); ?>/wp-login.php?action=lostpassword" class="forgot_password" title="<?php echo get_option('op_lost_pass_text'); ?>"><?php echo get_option('op_lost_pass_text'); ?></a>
			
			</div>

			<div>
				<form class="user_register" method="post" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>">
					<label for="user_login"><?php echo get_option('op_username_text'); ?>: </label>
					<input type="text" name="user_login" value="<?php echo esc_attr(stripslashes($user_login)); ?>" id="user_login" />
					
					<br />

					<label for="user_email"><?php echo get_option('op_email_text'); ?>: </label>
					<input type="text" name="user_email" value="<?php echo esc_attr(stripslashes($user_email)); ?>" id="user_email" />

					<br />

					<div class="action_btns">
						<div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i><?php echo get_option('op_back_text'); ?></a></div>
						<div class="one_half last">
						
					    <?php do_action('register_form'); ?>
					    <input class="btn btn_red" type="submit" name="user-submit" value="<?php echo get_option('op_sign_up_text'); ?>" />
					    <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?register=true" />
					    <input type="hidden" name="user-cookie" value="1" />

						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>	
	
	<?php } ?>	
	
	<?php wp_enqueue_script('leanModal', BASE_URL . 'js/jquery.leanModal.min.js', false, '', true); ?>
	
    <script type="text/javascript">
	jQuery(document).ready(function($) {
	$("#modal_trigger").leanModal({top : 200, overlay : 0.6, closeButton: ".modal_close" });

		$("#login_form").click(function(){
			$(".social_login").hide();
			$(".user_login").show();
            $(".header_title").text('<?php echo get_option('op_login_text'); ?>');
			return false;
		});

		$("#register_form").click(function(){
			$(".social_login").hide();
			$(".user_register").show();
            $(".header_title").text('<?php echo get_option('op_sign_up_text'); ?>');
			return false;
		});

		$(".back_btn").click(function(){
			$(".user_login").hide();
			$(".user_register").hide();
			$(".social_login").show();
			$(".header_title").text('<?php echo get_option('op_log_or_reg_text'); ?>');
			return false;
		});

	});
    </script>
	
<?php } ?>	
	
</div>
</div>