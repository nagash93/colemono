
<div id="cat_feat_posts">
<?php global $cat_data; ?>

	<?php 
	$cat = get_query_var('cat');
    $yourcat = get_category ($cat);
	$curent_cat = $yourcat->slug;
	$slides = $cat_data['extra1'];
	$my_query = new WP_Query('showposts=1&category_name='. $curent_cat .'');	

    if ($my_query->have_posts()) :
    ?>					

    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; $i++; ?>		
	
    <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	
	
	<?php if ($cat_data['extra2'] == 'hide_sidebar'){ ?>	 
	<?php $image = aq_resize( $thumbnailSrc, 1140, 500, true ); ?>
	<?php } else { ?>
	<?php $image = aq_resize( $thumbnailSrc, 770, 450, true ); ?>
	<?php } ?>
	

	<?php
	
		if(function_exists('taqyeem_get_score')) { 
					
			global $post;
					
            $total_score_data = get_post_meta($post->ID, 'taq_review_score', true);
            $post_score = round($total_score_data/1, 1 ); 
					
            $user_points = get_post_meta($post->ID, 'tie_user_rate', true);
					
			$post_total_score = get_post_meta($post->ID, 'taq_review_total', true);
			$post_score_position = get_post_meta($post->ID, 'taq_review_position', true);
			
			$cat_rating_layout = $cat_data['extra5'];		
					
					
			if ($cat_rating_layout == '') {
            $post_score_box =  '<span title="'.$post_total_score.'" class="post-single-rate hide'. $post_score_position.'" style="background-color: '.$rate_bg.';">
			<span>'.$post_score.'</span><span class="rate_procent">%</span><span class="rate_title_final">'.$post_total_score.'</span></span>';
			}
			if ($cat_rating_layout == 'Stars') {
            $post_score_box =  '<span title="'.$post_total_score.'" class="post-single-rate-stars post-small-rate stars-small hide'. $post_score_position.'" style="background-color: '.$rate_bg.';">
			<span style="width:'.$post_score.'%"></span></span>';
		    }
			if ($cat_rating_layout == 'Points') {
            $post_score_box =  '<span title="'.$post_total_score.'" class="post-single-rate-points hide'. $post_score_position.'">
			<span style="background-color: '.$rate_bg.';">'.$user_points.'</span></span>';
		    }
		};
	
	?>
	
    <div class="cat_feat_post">

	<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">
    <img src="<?php echo $image ?>" alt="" alt="<?php the_title(); ?>"/>
	<div class="feat_post_shadow_box"></div>
	</a>


	
	
    <div class="feat_post_title">
	<div class="gallery_meta">
	    <?php if (get_option('op_single_time') == 'on') { ?>
		<div class="home_posts_time"><?php the_time('j M, Y'); ?></div>	
		<?php } ?>

        <?php if (get_option('op_single_categories') == 'on') { ?>
	    <?php $categories_string = '';
	    foreach((get_the_category()) as $category) {
        $category_term = $category->category_nicename . '';
	    $categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	    } 
	    $categories_string = trim($categories_string); 
	    ?>
        <div class="home_posts_cats_box"><?php echo $categories_string ?></div>
		<?php } ?>
	</div>
	
	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
	</div>

    <?php echo $post_score_box; ?> 
    </div>

    <?php endwhile; wp_reset_query(); ?> 
    <?php endif; ?>  

</div>
