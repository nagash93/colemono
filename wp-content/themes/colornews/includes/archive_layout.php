    

	<div id="archive">
	
	<div class="archive_title"><h3><?php echo get_option('op_30_recent_posts'); ?></h3></div> 
    <ul class="archive_ul">
	
	<?php $archive_query = new WP_Query('showposts=30');  
        while ($archive_query->have_posts()) : $archive_query->the_post(); ?>  
		
  	    <?php 
		$format = get_post_format(); if ( false === $format ) { 
		$post_format_image = ''; 
		}	
		if(has_post_format('video')) { 
		$post_format_image = '<div class="post_format_video"></div>';
		}		
		if(has_post_format('image')) {
		$post_format_image = '<div class="post_format_image"></div>';
		}
		if(has_post_format('audio')) {
		$post_format_image = '<div class="post_format_audio"></div>';
		}
        ?>
		
        <li class="archive_ul_li">  
		<?php if(has_post_thumbnail()) { ?>
		<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>		
		
	
		<div class="post_img_box">
        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
		
		<?php $image = aq_resize( $thumbnailSrc, 300, 200, true); ?>
		
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
		<span class="short_image_shadow"></span>
		</a>
		<?php echo $post_format_image; ?>
		</div>	
		
        <?php } ?>
		
        <a class="arch_title" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>  
		
	   </li>  
    <?php endwhile; ?>  
    </ul>
	
	

	
	
    </div>
	
<?php wp_enqueue_script('gridalicious', BASE_URL . 'js/jquery.grid-a-licious.min.js', false, '', true); ?>
<script type="text/javascript">
jQuery(document).ready(function($){  
$(".archive_ul").gridalicious({
selector: '.archive_ul_li',
width: 220,
gutter: 0,
animate: false,
    animationOptions:{
    queue: false,
    speed: 0,
    duration: 0,
    }
});
});

(function($){ 
$(window).load(function(){ 
$("#archive").css({ visibility: "visible" });
})
})(jQuery);
</script>
