
<div class="ticker_box <?php if (get_option('op_newsticker_layout') == 'With Images') { ?>ticker_layout_two<?php } ?>">
<div class="inner">
	
	<div class="ticker1 modern-ticker mt-square">
	<div class="mt-body">
	<div class="mt-label"><?php echo (get_option('op_ticker_text')) ?></div>
	<div class="mt-news">
	<ul>
	
	<?php 
    $featucat = get_option('op_ticker_cat');
	$slides = get_option('op_ticker_slides');
    if (get_option('op_recent_news_ticker') == 'Recent posts') {
	$my_query = new WP_Query('showposts='. $slides .'');	
	} else {
    $my_query = new WP_Query('showposts='. $slides .'&category_name='. $featucat .'');	
	}
	
    if ($my_query->have_posts()) :
    ?>					
		
    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; ?>	
	
	<li>	
	
        <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>		
        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
		<?php $image = aq_resize( $thumbnailSrc, 100, 50, false); ?>
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
		</a>
	
	<a class="ticker_title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	
		<?php $category = get_the_category();
        if ($category) {
        echo '<a class="custom_cat_class ' . $category[0]->category_nicename.'" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "%s", "my-text-domain" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
        }
        ?>
	
	</li>
	
	<?php endwhile; wp_reset_query(); ?> 
    <?php endif; ?>   
	
	</ul>
	</div>
	<div class="mt-controls"><div class="mt-prev"></div><div class="mt-play"></div><div class="mt-next"></div></div>
	</div>
	</div>
	
</div>
</div>

<?php wp_enqueue_script('newsticker', BASE_URL . 'js/jquery.modern-ticker.min.js', false, '', true); ?>

<script type="text/javascript">
jQuery(document).ready(function($){  
$(".ticker1").modernTicker({
effect:"scroll",
scrollType:"continuous",
scrollStart:"inside",
scrollInterval:20,
transitionTime:500,
autoplay:true
});
});
</script>