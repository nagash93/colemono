<div id="footer_popular_posts">

    <div class="inner">	
    <div class="popular_posts_title"><?php echo get_option('op_popular_posts'); ?></div>
	</div>
	
    <div class="inner">	

	<?php 
    $my_query = new WP_Query('showposts=5&meta_key=votes_count&orderby=meta_value_num');	
    if ($my_query->have_posts()) :
	$i = 0;
    ?>					

    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; $i++; ?>	
	
    <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	
	
	<?php if ($i == 1) {
		$image = aq_resize( $thumbnailSrc, 520, 352, true );
		} else {
		$image = aq_resize( $thumbnailSrc, 280, 190, true );
        }
	?>
	
    <div class="footer_popular_posts_box">

	<a href="<?php the_permalink(); ?>">
    <img src="<?php echo $image ?>" alt="<?php the_title(); ?>"/>
	<div class="feat_post_shadow_box"></div>
	</a>

	<div class="popular_posts_title_box">
	<div class="cats_and_formats_box">
		<?php $category = get_the_category();
        if ($category) {
        echo '<a class="custom_cat_class ' . $category[0]->category_nicename.'" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "%s", "my-text-domain" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
        }
        ?>
	</div>
    <div class="clear"></div>
    <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
    </div>

    </div>
	
    <?php endwhile; wp_reset_query(); ?> 
    <?php endif; ?>  
	</div>
</div>

<div class="clear"></div>