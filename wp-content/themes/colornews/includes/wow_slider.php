

<div id="wowslider-container1">
<div class="ws_images">

<ul>
	<?php 
    $featucat = get_option('op_feat_cat');
	$slides = get_option('op_feat_slides');
	if (get_option('op_recent_featured_flex') == 'Recent posts') {
	$my_query = new WP_Query('showposts='. $slides .'');	
	} else {
    $my_query = new WP_Query('showposts='. $slides .'&category_name='. $featucat .'');	
	}
    if ($my_query->have_posts()) :
    ?>					

    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; ?>		
	
    <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	
	<?php $image = aq_resize( $thumbnailSrc, 1180, 550, true ); ?>	
	
	<li>
	<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">
	<img width="1180" height="550" src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
	</a>

	<div class="myBox">
    <div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
    </div>
	
	<?php $categories_string = '';
	foreach((get_the_category()) as $category) {
    $category_term = $category->category_nicename . '';
	$categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s, 'my-text-domain'" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	} 
	$categories_string = trim($categories_string); 
	?>
	<div class="category_box"><?php echo $categories_string ?></div>
	</li>
	
	<?php endwhile; wp_reset_query(); ?> 
    <?php endif; ?>  	
</ul>


</div>
<div class="ws_bullets">
<div>
	<?php 
    $featucat = get_option('op_feat_cat');
	$slides = get_option('op_feat_slides');
	if (get_option('op_recent_featured_flex') == 'Recent posts') {
	$my_query = new WP_Query('showposts='. $slides .'');	
	} else {
    $my_query = new WP_Query('showposts='. $slides .'&category_name='. $featucat .'');	
	}
    if ($my_query->have_posts()) :
    ?>					

    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; ?>		
	
    <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	
	<?php $image = aq_resize( $thumbnailSrc, 128, 60, true ); ?>	
	
	<a href="#" title="<?php the_title(); ?>">
	<img width="128" height="60" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
	<?php the_title(); ?>
	</a>
	
    <?php endwhile; wp_reset_query(); ?> 
    <?php endif; ?>  
	
</div>
</div>
<div class="ws_shadow"></div>
</div>


<?php wp_enqueue_script('wowslider', BASE_URL . 'js/wowslider.js', false, '', true); ?>

<script type="text/javascript">
jQuery(document).ready(function($){  
jQuery('#wowslider-container1').wowSlider({
	effect:"parallax", 
	prev:"", 
	next:"", 
	duration: 23*100, 
	delay:30*100, 
	width: 1180,
	height: 550,
	autoPlay:true,
	autoPlayVideo:false,
	playPause:false,
	stopOnHover:true,
	loop:false,
	bullets:1,
	caption: true, 
	captionEffect:"fade",
	controls:true,
	responsive:1,
	fullScreen:false,
	gestures: 2,
	onBeforeStep:0,
	images:0
});

});


(function($){ 
$(window).load(function(){ 

$(".myBox").click(function() {
  window.location = $(this).find("a").attr("href"); 
  return false;
});

})
})(jQuery);
</script>