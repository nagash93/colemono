
<div id="home_feat_posts_two">

	<?php 
    $featucat = get_option('op_feat_cat');
	$slides = 3;
	if (get_option('op_recent_featured_flex') == 'Recent posts') {
	$my_query = new WP_Query('showposts='. $slides .'');	
	} else {
    $my_query = new WP_Query('showposts='. $slides .'&category_name='. $featucat .'');	
	}
    if ($my_query->have_posts()) :
	$i = 0;
    ?>					

    <?php while ($my_query->have_posts()) : $my_query->the_post();$do_not_duplicate = $post->ID; $i++; ?>		
	
    <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	
	
	<?php if ($i == 1) {
		$image = aq_resize( $thumbnailSrc, 740, 545, true );
		} else {
		$image = aq_resize( $thumbnailSrc, 395, 270, true );
        }
	?>
	
    <div class="feat_post_box_two">

	<a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">
    <img src="<?php echo $image ?>" alt="<?php the_title(); ?>"/>
	<div class="feat_post_shadow_box"></div>
	</a>

	<div class="title_author_box">
	<div class="cats_and_formats_box">
		<?php $category = get_the_category();
        if ($category) {
        echo '<a class="custom_cat_class ' . $category[0]->category_nicename.'" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "%s", "my-text-domain" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
        }
        ?>
	</div>
    <div class="clear"></div>
    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
    </div>

    </div>

    <?php endwhile; wp_reset_query(); ?> 
    <?php endif; ?>  

<?php if (get_option('op_logo_feat_on') == 'on') { ?>
<div class="logo_featured">
	<?php $logo = get_template_directory_uri() . '/images/logo.png'; ?>
	<img src="<?php echo $logo; ?>" alt="Logo"/>
</div>
<?php } ?>
	
</div>

