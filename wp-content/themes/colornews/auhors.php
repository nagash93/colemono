
<?php
/*
Template Name: All Authors
*/
?>

<?php get_header(); ?>

<div id="main_content"> 
	
<?php if (get_option('op_crumbs') == 'on') { ?>
<div id="content_bread_panel">	
<div class="inner">
<?php if (function_exists('wp_breadcrumbs')) wp_breadcrumbs(); ?>
</div>
</div>
<?php } ?>
	
<div class="inner">	

<div id="content" class="content_with_one_sidebar">

<?php
$allUsers = get_users('orderby=post_count&order=DESC');
$users = array();
foreach($allUsers as $currentUser)
{
	if(!in_array( 'subscriber', $currentUser->roles ))
	{
		$users[] = $currentUser;
	}
}
?>

<?php get_header(); ?>


	<?php foreach($users as $user) { ?>
			
	<div id="author_box">

<?php echo get_avatar( $user->user_email, '70' ); ?>
<div class="authorinfo">
<h4><a title="<?php echo get_option("op_view_athor_posts"); ?> <?php echo $user->display_name; ?>" href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo $user->display_name; ?></a><span><?php echo $op_total_posts . ' ' . count_user_posts($user->ID); ?></span>
</h4>
<ul class="author_icons">
	
	<?php
	$twitter = get_user_meta($user->ID, 'twitter_profile', true);
	if($twitter != ''){
		printf('<li class="twitter_icon"><a title="Twitter" class="tip" target="_blank" href="%s"></a></li>', $twitter, 'Twitter');
	}

	$facebook = get_user_meta($user->ID, 'facebook_profile', true);
	if($facebook != ''){
		printf('<li class="facebook_icon"><a title="Facebook" class="tip" target="_blank" href="%s"></a></li>', $facebook, 'Facebook');
	}

	$google = get_user_meta($user->ID, 'google_profile', true);
	if($google != ''){
		printf('<li class="google_icon"><a title="Google" class="tip" target="_blank" href="%s"></a></li>', $google, 'Google');
	}

	$linkedin = get_user_meta($user->ID, 'linkedin_profile', true);
	if($linkedin != ''){
		printf('<li class="linkedin_icon"><a title="Linkedin" class="tip" target="_blank" href="%s"></a></li>', $linkedin, 'LinkedIn');
	}
	
	$instagram = get_user_meta($user->ID, 'instagram_profile', true);
	if($instagram != ''){
		printf('<li class="instagram_icon"><a title="Instagram" class="tip" target="_blank" href="%s"></a></li>', $instagram, 'Instagram');
	}		
								
	?>
	
	
</ul>

<?php $user_description = get_user_meta($user->ID, 'description', true);
	if($user_description != ''){ ?>
<p><?php echo $user_description; ?></p>
<?php } ?>


</div>
		
</div>
<div class="clear"></div>
	
	<?php } ?>


</div>

<?php get_sidebar('right'); ?>	
	
</div>
</div>

<div class="clear"></div>
	
<?php get_footer(); ?>
