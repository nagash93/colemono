

<?php get_header(); ?>

<?php 
$cat_id = get_query_var('cat');
$cat_data = get_option("category_$cat_id");
?>	

<?php 
$categories = get_the_category();
$category_id = $categories[0]->cat_ID;
$posts_found = get_option('op_posts_found');

$taxonomy = 'category';
$term_obj = get_term( $category_id, $taxonomy );
$posts_count = '<div class="cat_post_count">' . $term_obj->count  .' '. $posts_found .'</div>'; 
?>

<div id="main_content" class="<?php if (isset($cat_data['extra2'])){ echo $cat_data['extra2'];} ?>"> 

<?php if ( is_category() ) { ?> 
<?php if (get_option('op_crumbs_cats') == 'on') { ?>
<div id="content_bread_panel">	
<div class="inner">
<?php if (function_exists('wp_breadcrumbs')) wp_breadcrumbs(); ?>
</div>
</div>
<?php } ?>
<?php } else { ?>
<?php if (get_option('op_crumbs') == 'on') { ?>
<div id="content_bread_panel">	
<div class="inner">
<?php if (function_exists('wp_breadcrumbs')) wp_breadcrumbs(); ?>
</div>
</div>
<?php } ?>
<?php } ?>

<?php if ( is_category() ) { ?> 
<?php if (is_category( )) {
  $cat = get_query_var('cat');
  $yourcat = get_category ($cat);
  $yourcat->slug;
 } ?> 

<?php if($cat_data['extra3'] == 'post_layout_itm'){ ?>
<style type="text/css" media="all">
.post p, .product p, .post .custom_read_more {display: none;}
</style>
<?php } ?>
<?php if($cat_data['extra3'] == 'post_layout_itt'){ ?>
<style type="text/css" media="all">
.bottom_info_box {display: none!important;}
</style>
<?php } ?>
<?php if($cat_data['extra3'] == 'post_layout_it'){ ?>
<style type="text/css" media="all">
.post p, .product p, .post .custom_read_more, .bottom_info_box {display: none!important;}
</style>
<?php } ?>
<?php } ?>

<div class="clear"></div>




<?php if ( is_category() ) { ?> 
<?php if (get_option('op_cats_line') == 'on') { ?>

<?php if($cat_data['extra6'] == ''){ ?> 
<div class="inner">
<div class="cat_title_box <?php $catid = get_query_var('cat'); $cat_top_id =  pa_category_top_parent_id ($catid); echo get_cat_slug($cat_top_id); ?> <?php echo $yourcat->slug; ?>">


<?php if ( in_category('rockygames')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/rg.png"/>';}
		elseif ( in_category('cachilupi')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/cachilupi.png"/>';}
		elseif ( in_category('mono-terapia')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/terapia.png"/>';}
		elseif ( in_category('podcast-sin-hogar')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/pod.png"/>';}
		elseif ( in_category('nota')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/nota.png"/>';}
		elseif ( in_category('columnas')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/columnas.png"/>';}
		elseif ( in_category('after-mono')){echo'<img src="http://colemono.com/wp-content/uploads/2015/11/after.png"/>';}


		else{ ?><h4>

			<?php echo single_cat_title('');
		}
		?></h4>


<span><?php echo category_description( ); ?></span>
</div>
</div>
<div class="clear"></div>


<?php } else { ?>

<?php wp_enqueue_script('dzsparallaxer', BASE_URL . 'js/dzsparallaxer.js', false, '', true); ?>

<div class="dzsparallaxer auto-init">

<div class="divimage dzsparallaxer--target " style="width: 100%; height: 624px; background-image: url(<?php if (isset($cat_data['extra6'])){ echo $cat_data['extra6'];} ?>);">
</div>
<div class="center-it">

<div class="photo_bg_shadow">
<div class="photo_bg">

<div class="inner">
<div class="photo_content_box">
<h4><?php single_cat_title(''); ?></h4> 
<div class="clear"></div>
<?php echo category_description( ); ?>
</div>
</div>

</div>
</div>
</div>

</div>
<div class="clear"></div>
<?php } ?>

<?php } ?>
<?php } ?>	



<div class="inner">	

<div id="content" class="<?php if (isset($cat_data['extra2'])){ echo $cat_data['extra2'];} ?>">	

<?php if ( is_author() ) { ?> 
<div id="author_box">

<?php echo get_avatar( get_the_author_meta( 'user_email' ), 70 ); ?>
<div class="authorinfo">
<h4><a class="tip" title="<?php echo get_option("op_view_athor_posts"); ?> <?php echo get_the_author(); ?>" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author(); ?></a></h4>
<ul class="author_icons">
    <?php                               
    $google_profile = get_the_author_meta( 'google_profile' );
    if ( $google_profile && $google_profile != '' ) {
        echo '<li class="google_icon"><a title="Google" class="tip" target="_blank" href="' . esc_url($google_profile) . '" rel="author"></a></li>';
    }
                                               
    $twitter_profile = get_the_author_meta( 'twitter_profile' );
    if ( $twitter_profile && $twitter_profile != '' ) {
        echo '<li class="twitter_icon"><a title="Twitter" class="tip" target="_blank" href="' . esc_url($twitter_profile) . '"></a></li>';
    }
                                               
    $facebook_profile = get_the_author_meta( 'facebook_profile' );
    if ( $facebook_profile && $facebook_profile != '' ) {
        echo '<li class="facebook_icon"><a title="Facebook" class="tip" target="_blank" href="' . esc_url($facebook_profile) . '"></a></li>';
    }
       
    $instagram_profile = get_the_author_meta( 'instagram_profile' );
    if ( $instagram_profile && $instagram_profile != '' ) {
        echo '<li class="instagram_icon"><a title="Instagram" class="tip" target="_blank" href="' . esc_url($instagram_profile) . '"></a></li>';
    }
	   
    $linkedin_profile = get_the_author_meta( 'linkedin_profile' );
    if ( $linkedin_profile && $linkedin_profile != '' ) {
        echo '<li class="linkedin_icon"><a title="Linkedin" class="tip" target="_blank" href="' . esc_url($linkedin_profile) . '"></a></li>';
    }
    ?>
</ul>
<div class="clear"></div>
<p><?php the_author_meta( 'description' ); ?></p>
</div>
		
</div>
<div class="clear"></div>
<?php } ?>	



<div class="index_inner <?php if (isset($cat_data['extra4'])){ echo $cat_data['extra4'];} ?>">	

    <?php if (is_category( )) { ?>
    <?php if($cat_data['extra1'] !== ''){ ?>
	<?php get_template_part('includes/feat_posts_category'); ?>
    <?php } ?>
    <?php } ?>	

	
<?php if (get_option('op_banner_index') == 'on') { ?>
<?php if($cat_data['extra7'] == ''){ ?>
<div id="banner_index_728">
<?php $index_banner = get_option("op_banner_index_code"); ?>
<?php echo stripslashes($index_banner); ?>
</div>
<?php } else {} ?>
<?php } ?>	

    <?php if (is_category( )) { ?>
    <?php if($cat_data['extra1'] !== ''){ ?>
    <?php if (isset($cat_data['extra1'])){ $exclude_count = $cat_data['extra1']; } ?>
    
    <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $posts_per_page = get_option('posts_per_page');
    $offset = $posts_per_page * ($paged - 1) + $exclude_count;
    $args = array(
    'category_name' => ''.$yourcat->slug.'',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged,
    'offset' => $offset,
    );
    query_posts($args); ?> 
	
    <?php } ?> 
    <?php } ?>	
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
 
  	<?php 
		$format = get_post_format(); if ( false === $format ) { 
		$post_format_image = ''; 
		}
					
		if(has_post_format('video')) { 
		$post_format_image = '<div class="post_format_video"></div>';
		}
					
		if(has_post_format('image')) {
		$post_format_image = '<div class="post_format_image"></div>';
		}
		
		if(has_post_format('audio')) {
		$post_format_image = '<div class="post_format_audio"></div>';
		}
    ?>	
	 
	<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">	

		<?php if(has_post_thumbnail()) { ?>
		<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>		
		
		
	    <?php
		if(function_exists('taqyeem_get_score')) { 
					
			global $post;
					
            $total_score_data = get_post_meta($post->ID, 'taq_review_score', true);
            $post_score = round($total_score_data/1, 1 ); 
					
            $user_points = get_post_meta($post->ID, 'tie_user_rate', true);
					
			$post_total_score = get_post_meta($post->ID, 'taq_review_total', true);
			$post_score_position = get_post_meta($post->ID, 'taq_review_position', true);
			
			$cat_rating_layout = $cat_data['extra5'];		
					
					
			if ($cat_rating_layout == '') {
            $post_score_box =  '<span title="'.$post_total_score.'" class="post-single-rate hide'. $post_score_position.'" style="background-color: '.$rate_bg.';">
			<span>'.$post_score.'</span><span class="rate_procent">%</span><span class="rate_title_final">'.$post_total_score.'</span></span>';
			}
			if ($cat_rating_layout == 'Stars') {
            $post_score_box =  '<span title="'.$post_total_score.'" class="post-single-rate-stars post-small-rate stars-small hide'. $post_score_position.'" style="background-color: '.$rate_bg.';">
			<span style="width:'.$post_score.'%"></span></span>';
		    }
			if ($cat_rating_layout == 'Points') {
            $post_score_box =  '<span title="'.$post_total_score.'" class="post-single-rate-points hide'. $post_score_position.'">
			<span style="background-color: '.$rate_bg.';">'.$user_points.'</span></span>';
		    }
		};
	    ?>
		
		<div class="post_img_box">
        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
		
		<?php if($cat_data['extra4'] == 'one_line'){ ?>
		<?php $image = aq_resize( $thumbnailSrc, 290, 220, true); ?>
		<?php } else { ?>
		<?php $image = aq_resize( $thumbnailSrc, 745, 400, true); ?>
		<?php } ?>
		
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
		<span class="short_image_shadow"></span>
		</a>
		<?php echo $post_format_image; ?>
        <?php echo $post_score_box; ?> 
		</div>	

        <?php } else {} ?>

		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title(); ?> &raquo;</a></h3>
		
		<div class="bottom_info_box">
		
        <?php if (get_option('op_index_time') == 'on') { ?>
		<div class="home_posts_time"><?php the_time('j M, Y'); ?></div>	
		<?php } ?>
		
        <?php if (get_option('op_index_author') == 'on') { ?>
		<?php $post_author = '<a target="_blank" href=" '.get_author_posts_url( get_the_author_meta( 'ID' ) ) .'">'.get_the_author_link().'</a>'; ?>
		<?php $post_avatar = get_avatar( get_the_author_id(), 12); ?>
		<div class="short_post_author"><?php echo $post_avatar; ?> <?php echo $post_author; ?></div>
		<?php } ?>
		
        <?php if (get_option('op_index_categories') == 'on') { ?>
	    <?php $categories_string = '';
	    foreach((get_the_category()) as $category) {
        $category_term = $category->category_nicename . '';
	    $categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	    } 
	    $categories_string = trim($categories_string); 
	    ?>
        <div class="home_posts_cats_box"><?php echo $categories_string ?></div>
		<?php } ?>
		
		
        <?php if (get_option('op_index_comments') == 'on') { ?>
		<?php $comments_count = get_comments_number(); ?>
		<?php $comments_text = get_option('op_comments_text'); ?>
		<div class="comments_count_simple_box"><?php echo $comments_count ?></div>
		<?php } ?>
		
        <?php if (get_option('op_index_likes') == 'on') { ?>
		<?php echo getPostLikeLink(get_the_ID()); ?>
		<?php } ?>
		
		</div>
    
	
		<?php $post_text = get_post_meta($post->ID, 'r_post_text', true); ?>
	    <?php if($post_text !== '') { ?>
	    <p>
	    <?php echo $post_text; ?>
        </p> 

	    <?php } else { ?>
		<?php the_excerpt(); ?>
        <?php } ?>

        <?php $custom_read_more = get_post_meta($post->ID, 'r_custom_read_more', true); ?>
	    <?php if($custom_read_more !== '') { ?>
	    <div class="custom_read_more">
					
        <?php $custom_rm_link = get_post_meta($post->ID, 'r_custom_rm_link', true); ?>
	    <?php if($custom_rm_link !== '') { ?>
		<a href="<?php echo $custom_rm_link; ?>" title="<?php the_title(); ?>" target="_blank">
		<?php } else { ?>	
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">					
	    <?php } ?>	
					
	    <?php echo $custom_read_more; ?>
		</a>
        </div>
	    <?php } else { ?>
	    <div class="custom_read_more">
					
        <?php $custom_rm_link = get_post_meta($post->ID, 'r_custom_rm_link', true); ?>
	    <?php if($custom_rm_link !== '') { ?>
		<a href="<?php echo $custom_rm_link; ?>" title="<?php the_title(); ?>" target="_blank">
		<?php } else { ?>	
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">					
	    <?php } ?>	
					
		<?php echo get_option('op_read_more'); ?>
					
		</a>
        </div>
		<?php } ?>

	</div>
	
		<?php endwhile; ?>
		<?php else : ?>
		
		<div class="post_nr">
        <h2><?php echo get_option('op_nothing_found'); ?></h2>
        <div class="single-entry">
		<?php echo get_option('op_nothing_found_text'); ?>
		<div class="clear"></div>
		<?php get_search_form(); ?>
		</div>
        </div>

	    <?php endif; ?>	 

</div>		
	
<div class="clear"></div>

        <?php if(function_exists('wp_pagenavi')) { ?>
		<div class="postnav"> 
		<?php wp_pagenavi(); ?>
        </div>
		
        <?php } else { ?>
        <?php custom_pagination(); ?>
        <?php } ?>
 
	    <div class="clear"></div>


<?php if($cat_data['extra4'] == 'one_line'){ } else { ?>
<?php wp_enqueue_script('gridalicious', BASE_URL . 'js/jquery.grid-a-licious.min.js', false, '', true); ?>
<script type="text/javascript">
jQuery(document).ready(function($){  

<?php if($cat_data['extra4'] !== 'one_column'){?>

$(".index_inner").gridalicious({
selector: '.post',
width: 350,
gutter: 0,
animate: false,
    animationOptions:{
    queue: false,
    speed: 0,
    duration: 0,
    }
});

<?php } else { ?>	

<?php if($cat_data['extra2'] == 'hide_sidebar'){ ?>
$(".index_inner.one_column").gridalicious({
selector: '.post',
width: 450,
gutter: 0,
animate: false,
    animationOptions:{
    queue: false,
    speed: 0,
    duration: 0,
    }
});
<?php } else { ?>	
$(".index_inner.one_column").gridalicious({
selector: '.post',
width: 400,
gutter: 0,
animate: false,
    animationOptions:{
    queue: false,
    speed: 0,
    duration: 0,
    }
});
<?php } ?>
<?php } ?>
});
</script>

<?php } ?>	
	
</div>

<?php if($cat_data['extra2'] == 'hide_sidebar'){ } else { ?>

<?php get_sidebar('right'); ?>	

<?php } ?>	

</div>
</div>

<div class="clear"></div>
	
<?php get_footer(); ?>

	