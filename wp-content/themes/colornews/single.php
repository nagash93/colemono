
<?php get_header(); ?>

<div id="main_content"> 

<?php if (get_option('op_crumbs_single') == 'on') { ?>
<div id="content_bread_panel">	
<div class="inner">
<?php if (function_exists('wp_breadcrumbs')) wp_breadcrumbs(); ?>
</div>
</div>
<?php } ?>

<div class="clear"></div>

<?php
global $wp_query;
$postid = $wp_query->post->ID;
$post_thumb_var = get_post_meta($postid, 'r_post_thumb_var', true);
wp_reset_query();
?>
<?php if($post_thumb_var == 'Video - Full width') { ?>
<div class="single_video_full_width">
	<div class="inner">
	
	<?php 
    $youtube = get_post_meta($post->ID, 'r_youtube', true);
    $vimeo = get_post_meta($post->ID, 'r_vimeo', true);
    ?>

	<?php if($youtube) { ?>
		<div class="video-container">
		<iframe src="//www.youtube.com/embed/<?php echo $youtube; ?>" frameborder="0" allowfullscreen></iframe>
		</div>
	<?php } ?>
		
    <?php if($vimeo) { ?>
		<div class="video-container">
		<iframe src="//player.vimeo.com/video/<?php echo $vimeo; ?>" ></iframe>
		</div> 
	<?php } ?>
	
    </div>
	
	
<?php if (get_option('op_single_meta_line') == 'on') { ?>	
<div class="big_meta_line">
<div class="inner">

        <?php if (get_option('op_single_author') == 'on') { ?>
		<?php $post_author = '<a target="_blank" href=" '.get_author_posts_url( get_the_author_meta( 'ID' ) ) .'">'.get_the_author_link().'</a>'; ?>
		<?php $post_avatar = get_avatar( get_the_author_id(), 30); ?>
		<div class="short_post_author"><?php echo $post_avatar; ?> <?php echo $post_author; ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_time') == 'on') { ?>
		<div class="home_posts_time"><?php the_time('j M, Y'); ?></div>	
		<?php } ?>
		
        <?php if (get_option('op_single_categories') == 'on') { ?>
	    <?php $categories_string = '';
	    foreach((get_the_category()) as $category) {
        $category_term = $category->category_nicename . '';
	    $categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	    } 
	    $categories_string = trim($categories_string); 
	    ?>
        <div class="home_posts_cats_box"><?php echo $categories_string ?></div>
		<?php } ?>
		
		
        <?php if (get_option('op_single_post_comments') == 'on') { ?>
		<?php $comments_count = get_comments_number(); ?>
		<?php $comments_text = get_option('op_comments_text'); ?>
		<div class="comments_count_simple_box"><?php echo $comments_count ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_likes') == 'on') { ?>
		<?php echo getPostLikeLink(get_the_ID()); ?>
		<?php } ?>
		
	    <?php if( has_tag() ) { ?>
        <?php if (get_option('op_feat_similar') == 'on') { ?>	
        <div class="big_sim_title_box"><span><?php echo $op_more_news ?></span></div>
	    <?php } ?> 	
	    <?php } ?> 	
		
</div> 	
</div>

<?php } ?>
	
</div>

	<?php if( has_tag() ) { ?>
    <?php if (get_option('op_feat_similar') == 'on') { ?>
	<div id="big_similar_posts"> 
    <div class="inner">
	
	    <?php
        $tags = wp_get_post_tags($post->ID);
        if ($tags) {
        $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

        $args=array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'showposts'=>5,
        'caller_get_posts'=>1
        );
        $my_query = new wp_query($args);
        if( $my_query->have_posts() ) {
        echo '<ul>';
        while ($my_query->have_posts()) {
        $my_query->the_post();
        ?>
		
		
  	    <?php 
		$format = get_post_format(); if ( false === $format ) { 
		$post_format_image = ''; 
		}	
		if(has_post_format('video')) { 
		$post_format_image = '<div class="post_format_video"></div>';
		}		
		if(has_post_format('image')) {
		$post_format_image = '<div class="post_format_image"></div>';
		}
		if(has_post_format('audio')) {
		$post_format_image = '<div class="post_format_audio"></div>';
		}
        ?>
		
		
		<li class="big_similar_post">
		
		<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	 
        <?php if(has_post_thumbnail()) { ?>	
		<div class="post_img_box">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" >
		<?php $image = aq_resize( $thumbnailSrc, 215, 140, true); ?>
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>					
		<span class="short_image_shadow"></span>
		<?php echo $post_format_image; ?>
		</a>
        </div>	
        <?php } else { } ?>	

		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title(); ?></a></h3>
	    </li>
		
        <?php } echo '</ul>'; } } ?>
        <?php wp_reset_query(); ?>
		
    </div>	 
    </div>	 
	<?php } ?>
	<?php } ?> 	
		
<?php } ?>

<?php
global $wp_query;
$postid = $wp_query->post->ID;
$post_thumb_var = get_post_meta($postid, 'r_post_thumb_var', true);
wp_reset_query();
?>
<?php if($post_thumb_var == 'Full width Carousel') { ?>

<?php wp_enqueue_script('sliderPro', BASE_URL . 'js/jquery.sliderPro.min.js', false, '', true); ?>

<script type="text/javascript">
(function($){ 
$(window).load(function(){ 
	$( '#example2' ).sliderPro({
	width: '70%',
	height: 500,
	aspectRatio: 2,
	visibleSize: '100%'
	});
})
})(jQuery);
</script>	
	<div id="example2" class="slider-pro">
		<div class="sp-slides">
		
		<?php 
        $slider_image_1 = get_post_meta($post->ID, 'r_slider_image_1', true);
		$slider_image_2 = get_post_meta($post->ID, 'r_slider_image_2', true);
		$slider_image_3 = get_post_meta($post->ID, 'r_slider_image_3', true);
		$slider_image_4 = get_post_meta($post->ID, 'r_slider_image_4', true);
		$slider_image_5 = get_post_meta($post->ID, 'r_slider_image_5', true);
		$slider_image_6 = get_post_meta($post->ID, 'r_slider_image_6', true);		
		$slider_image_7 = get_post_meta($post->ID, 'r_slider_image_7', true);
		$slider_image_8 = get_post_meta($post->ID, 'r_slider_image_8', true);
		$slider_image_9 = get_post_meta($post->ID, 'r_slider_image_9', true);
		$slider_image_10 = get_post_meta($post->ID, 'r_slider_image_10', true);		
        $slider_image_11 = get_post_meta($post->ID, 'r_slider_image_11', true);
		$slider_image_12 = get_post_meta($post->ID, 'r_slider_image_12', true);
		$slider_image_13 = get_post_meta($post->ID, 'r_slider_image_13', true);
		$slider_image_14 = get_post_meta($post->ID, 'r_slider_image_14', true);
		$slider_image_15 = get_post_meta($post->ID, 'r_slider_image_15', true);
		
		$title_slider_image_1 = get_post_meta($post->ID, 'r_title_slider_image_1', true);	
		$title_slider_image_2 = get_post_meta($post->ID, 'r_title_slider_image_2', true);
		$title_slider_image_3 = get_post_meta($post->ID, 'r_title_slider_image_3', true);
		$title_slider_image_4 = get_post_meta($post->ID, 'r_title_slider_image_4', true);
		$title_slider_image_5 = get_post_meta($post->ID, 'r_title_slider_image_5', true);
		$title_slider_image_6 = get_post_meta($post->ID, 'r_title_slider_image_6', true);
		$title_slider_image_7 = get_post_meta($post->ID, 'r_title_slider_image_7', true);
		$title_slider_image_8 = get_post_meta($post->ID, 'r_title_slider_image_8', true);
		$title_slider_image_9 = get_post_meta($post->ID, 'r_title_slider_image_9', true);
		$title_slider_image_10 = get_post_meta($post->ID, 'r_title_slider_image_10', true);
		$title_slider_image_11 = get_post_meta($post->ID, 'r_title_slider_image_11', true);	
		$title_slider_image_12 = get_post_meta($post->ID, 'r_title_slider_image_12', true);
		$title_slider_image_13 = get_post_meta($post->ID, 'r_title_slider_image_13', true);
		$title_slider_image_14 = get_post_meta($post->ID, 'r_title_slider_image_14', true);
		$title_slider_image_15 = get_post_meta($post->ID, 'r_title_slider_image_15', true);
        ?>
		
		<?php  if($slider_image_1 !== '') { ?>
		<?php $image = aq_resize( $slider_image_1, 1200, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_1 ?>" title="<?php echo $title_slider_image_1 ?>"/>
		<p class="sp-caption"><?php echo $title_slider_image_1 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_2 !== '') { ?>
		<?php $image = aq_resize( $slider_image_2, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_2 ?>"  title="<?php echo $title_slider_image_2 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_2 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_3 !== '') { ?>
		<?php $image = aq_resize( $slider_image_3, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_3 ?>"  title="<?php echo $title_slider_image_3 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_3 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_4 !== '') { ?>
		<?php $image = aq_resize( $slider_image_4, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_4 ?>"  title="<?php echo $title_slider_image_4 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_4 ?></p>
		</div>
        <?php } ?>		

		<?php  if($slider_image_5 !== '') { ?>
		<?php $image = aq_resize( $slider_image_5, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_5 ?>"  title="<?php echo $title_slider_image_5 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_5 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_6 !== '') { ?>
		<?php $image = aq_resize( $slider_image_6, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_6 ?>"  title="<?php echo $title_slider_image_6 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_6 ?></p>
		</div>
        <?php } ?>	
		
		<?php  if($slider_image_7 !== '') { ?>
		<?php $image = aq_resize( $slider_image_7, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_7 ?>"  title="<?php echo $title_slider_image_7 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_7 ?></p>
		</div>
        <?php } ?>	
		
		<?php  if($slider_image_8 !== '') { ?>
		<?php $image = aq_resize( $slider_image_8, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_8 ?>"  title="<?php echo $title_slider_image_8 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_8 ?></p>
		</div>
        <?php } ?>	

		<?php  if($slider_image_9 !== '') { ?>
		<?php $image = aq_resize( $slider_image_9, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_9 ?>"  title="<?php echo $title_slider_image_9 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_9 ?></p>
		</div>
        <?php } ?>	

		<?php  if($slider_image_10 !== '') { ?>
		<?php $image = aq_resize( $slider_image_10, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_10 ?>"  title="<?php echo $title_slider_image_10 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_10 ?></p>
		</div>
        <?php } ?>			

		<?php  if($slider_image_11 !== '') { ?>
		<?php $image = aq_resize( $slider_image_11, 1200, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_11 ?>" title="<?php echo $title_slider_image_11 ?>"/>
		<p class="sp-caption"><?php echo $title_slider_image_11 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_12 !== '') { ?>
		<?php $image = aq_resize( $slider_image_12, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_12 ?>"  title="<?php echo $title_slider_image_12 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_12 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_13 !== '') { ?>
		<?php $image = aq_resize( $slider_image_13, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_13 ?>"  title="<?php echo $title_slider_image_13 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_13 ?></p>
		</div>
        <?php } ?>

		<?php  if($slider_image_14 !== '') { ?>
		<?php $image = aq_resize( $slider_image_14, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_14 ?>"  title="<?php echo $title_slider_image_14 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_14 ?></p>
		</div>
        <?php } ?>		

		<?php  if($slider_image_15 !== '') { ?>
		<?php $image = aq_resize( $slider_image_15, 1280, 700, true ); ?>
		<div class="sp-slide">
        <img class="sp-image" data-src="<?php echo $image ?>" alt="<?php echo $title_slider_image_15 ?>"  title="<?php echo $title_slider_image_15 ?>" />
		<p class="sp-caption"><?php echo $title_slider_image_15 ?></p>
		</div>
        <?php } ?>
		
		
		</div>
    </div>
<?php } ?>




<?php
global $wp_query;
$postid = $wp_query->post->ID;
$post_thumb_var = get_post_meta($postid, 'r_post_thumb_var', true);
wp_reset_query();
?>
<?php if($post_thumb_var == 'Full width') { ?>

<?php
$thumb_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$image_full = aq_resize( $thumb_url, 1200, 'auto', false); ?>

<div class="big_image_cover">

<img src="<?php echo $image_full ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
<div class="photo_bg_shadow"></div>

<div class="photo_bg_inner">
<div class="inner">

	<h1><?php the_title(); ?></h1> 

</div> 	
</div>


<?php if (get_option('op_single_meta_line') == 'on') { ?>	
<div class="big_meta_line">
<div class="inner">

        <?php if (get_option('op_single_author') == 'on') { ?>
		<?php $post_author = '<a target="_blank" href=" '.get_author_posts_url( get_the_author_meta( 'ID' ) ) .'">'.get_the_author_link().'</a>'; ?>
		<?php $post_avatar = get_avatar( get_the_author_id(), 30); ?>
		<div class="short_post_author"><?php echo $post_avatar; ?> <?php echo $post_author; ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_time') == 'on') { ?>
		<div class="home_posts_time"><?php the_time('j M, Y'); ?></div>	
		<?php } ?>
		
        <?php if (get_option('op_single_categories') == 'on') { ?>
	    <?php $categories_string = '';
	    foreach((get_the_category()) as $category) {
        $category_term = $category->category_nicename . '';
	    $categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	    } 
	    $categories_string = trim($categories_string); 
	    ?>
        <div class="home_posts_cats_box"><?php echo $categories_string ?></div>
		<?php } ?>
		
		
        <?php if (get_option('op_single_post_comments') == 'on') { ?>
		<?php $comments_count = get_comments_number(); ?>
		<?php $comments_text = get_option('op_comments_text'); ?>
		<div class="comments_count_simple_box"><?php echo $comments_count ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_likes') == 'on') { ?>
		<?php echo getPostLikeLink(get_the_ID()); ?>
		<?php } ?>
		
	    <?php if( has_tag() ) { ?>
        <?php if (get_option('op_feat_similar') == 'on') { ?>	
        <div class="big_sim_title_box"><span><?php echo $op_more_news ?></span></div>
	    <?php } ?> 	
	    <?php } ?> 	
		
</div> 	
</div>

<?php } ?>

</div>

	<?php if( has_tag() ) { ?>
    <?php if (get_option('op_feat_similar') == 'on') { ?>
	<div id="big_similar_posts"> 
    <div class="inner">
	
	    <?php
        $tags = wp_get_post_tags($post->ID);
        if ($tags) {
        $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

        $args=array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'showposts'=>5,
        'caller_get_posts'=>1
        );
        $my_query = new wp_query($args);
        if( $my_query->have_posts() ) {
        echo '<ul>';
        while ($my_query->have_posts()) {
        $my_query->the_post();
        ?>
		
		
  	    <?php 
		$format = get_post_format(); if ( false === $format ) { 
		$post_format_image = ''; 
		}	
		if(has_post_format('video')) { 
		$post_format_image = '<div class="post_format_video"></div>';
		}		
		if(has_post_format('image')) {
		$post_format_image = '<div class="post_format_image"></div>';
		}
		if(has_post_format('audio')) {
		$post_format_image = '<div class="post_format_audio"></div>';
		}
        ?>
		
		
		<li class="big_similar_post">
		
		<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	 
        <?php if(has_post_thumbnail()) { ?>	
		<div class="post_img_box">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" >
		<?php $image = aq_resize( $thumbnailSrc, 215, 140, true); ?>
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>					
		<span class="short_image_shadow"></span>
		<?php echo $post_format_image; ?>
		</a>
        </div>	
        <?php } else { } ?>	

		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title(); ?></a></h3>
	    </li>
		
        <?php } echo '</ul>'; } } ?>
        <?php wp_reset_query(); ?>
		
    </div>	 
    </div>	 
	<?php } ?>	
    <?php } ?>
	
<?php } ?>



<?php
global $wp_query;
$postid = $wp_query->post->ID;
$post_thumb_var = get_post_meta($postid, 'r_post_thumb_var', true);
wp_reset_query();
?>
<?php if($post_thumb_var == 'Full width - Parallax') { ?>

<?php 
$thumb_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$image_full = aq_resize( $thumb_url, 1200, 'auto', false); ?>

<?php wp_enqueue_script('dzsparallaxer', BASE_URL . 'js/dzsparallaxer.js', false, '', true); ?>

<div class="dzsparallaxer auto-init single_photo">

<div class="divimage dzsparallaxer--target " style="width: 100%; height: 1200px; background-image: url(<?php echo $image_full ?>);">
</div>
<div class="center-it">

<div class="photo_bg_shadow">

<div class="photo_bg_inner">
<div class="inner">

	<h1><?php the_title(); ?></h1> 

</div> 	
</div>
</div>
</div> 



<?php if (get_option('op_single_meta_line') == 'on') { ?>	
<div class="big_meta_line">
<div class="inner">

        <?php if (get_option('op_single_author') == 'on') { ?>
		<?php $post_author = '<a target="_blank" href=" '.get_author_posts_url( get_the_author_meta( 'ID' ) ) .'">'.get_the_author_link().'</a>'; ?>
		<?php $post_avatar = get_avatar( get_the_author_id(), 30); ?>
		<div class="short_post_author"><?php echo $post_avatar; ?> <?php echo $post_author; ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_time') == 'on') { ?>
		<div class="home_posts_time"><?php the_time('j M, Y'); ?></div>	
		<?php } ?>
		
        <?php if (get_option('op_single_categories') == 'on') { ?>
	    <?php $categories_string = '';
	    foreach((get_the_category()) as $category) {
        $category_term = $category->category_nicename . '';
	    $categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	    } 
	    $categories_string = trim($categories_string); 
	    ?>
        <div class="home_posts_cats_box"><?php echo $categories_string ?></div>
		<?php } ?>
		
		
        <?php if (get_option('op_single_post_comments') == 'on') { ?>
		<?php $comments_count = get_comments_number(); ?>
		<?php $comments_text = get_option('op_comments_text'); ?>
		<div class="comments_count_simple_box"><?php echo $comments_count ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_likes') == 'on') { ?>
		<?php echo getPostLikeLink(get_the_ID()); ?>
		<?php } ?>
		
		<?php if( has_tag() ) { ?>
        <?php if (get_option('op_feat_similar') == 'on') { ?>	
        <div class="big_sim_title_box"><span><?php echo $op_more_news ?></span></div>
	    <?php } ?> 	
	    <?php } ?> 	
		
</div> 	
</div>

<?php } ?>
</div> 



<?php if( has_tag() ) { ?>

    <?php if (get_option('op_feat_similar') == 'on') { ?>
	<div id="big_similar_posts"> 
    <div class="inner">
	
	    <?php
        $tags = wp_get_post_tags($post->ID);
        if ($tags) {
        $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

        $args=array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'showposts'=>5,
        'caller_get_posts'=>1
        );
        $my_query = new wp_query($args);
        if( $my_query->have_posts() ) {
        echo '<ul>';
        while ($my_query->have_posts()) {
        $my_query->the_post();
        ?>
		
		
  	    <?php 
		$format = get_post_format(); if ( false === $format ) { 
		$post_format_image = ''; 
		}	
		if(has_post_format('video')) { 
		$post_format_image = '<div class="post_format_video"></div>';
		}		
		if(has_post_format('image')) {
		$post_format_image = '<div class="post_format_image"></div>';
		}
		if(has_post_format('audio')) {
		$post_format_image = '<div class="post_format_audio"></div>';
		}
        ?>
		
		
		<li class="big_similar_post">
		
		<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	 
        <?php if(has_post_thumbnail()) { ?>	
		<div class="post_img_box">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" >
		<?php $image = aq_resize( $thumbnailSrc, 215, 140, true); ?>
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>					
		<span class="short_image_shadow"></span>
		<?php echo $post_format_image; ?>
		</a>
        </div>	
        <?php } else { } ?>	

		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title(); ?></a></h3>
	    </li>
		
        <?php } echo '</ul>'; } } ?>
        <?php wp_reset_query(); ?>
		
    </div>	 
    </div>	 
	<?php } ?>	
	
<?php } ?>

<?php } ?>
	
	
	
	
	
<!---------------Single post content------------------->	
		
<div class="inner">

<?php
global $wp_query;
$postid = $wp_query->post->ID;
$post_layout = get_post_meta($postid, 'r_post_layout', true);
wp_reset_query();
?>
	

<div id="single_content" class="<?php if($post_layout == 'Full width') { ?>hide_sidebar<?php } ?> <?php if (get_option('op_single_post_layout') == 'Full width') { ?>hide_sidebar<?php } ?> <?php if (get_option('op_single_post_layout') == 'With 1 sidebar') { ?>content_with_one_sidebar<?php } ?>"> 	

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
    <div class="single_post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php $post_thumb_var = get_post_meta($post->ID, "r_post_thumb_var", true); ?>
	<?php if($post_thumb_var == '' || $post_thumb_var == 'Standard') { ?>
	
	   <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	

	   <?php if(has_post_thumbnail()) { ?>

	    <?php $post_thumbnail = get_post_meta($post->ID, "r_post_thumbnail", $single = true);
	    if($post_thumbnail !== 'on') { ?>
		
	    <div class="single_thumbnail">
		<a href="<?php echo $thumbnailSrc ?>" title="<?php the_title(); ?>" rel="prettyphoto">
	    <?php $image = aq_resize( $thumbnailSrc, 830, 'auto', false); ?>
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
	    </a>
		</div>
	    <?php } ?>
	   
	    <?php } else {} ?>
		
	<?php } ?>
	
		<?php $post_video = get_post_meta($post->ID, "r_post_video", $single = true);
	   if($post_video !== '') { ?>
	
	<?php 
        $youtube = get_post_meta($post->ID, 'r_youtube', true);
        $vimeo = get_post_meta($post->ID, 'r_vimeo', true);
    ?>

		<?php if($youtube) { ?>
		<div class="video-container">
		    <iframe src="//www.youtube.com/embed/<?php echo $youtube; ?>" frameborder="0" allowfullscreen></iframe>
		</div>
		<?php } ?>
		
        <?php if($vimeo) { ?>
		<div class="video-container">
		    <iframe src="//player.vimeo.com/video/<?php echo $vimeo; ?>" ></iframe>
		</div> 
		<?php } ?>

	<div class="clear"></div>

	<?php } else { } ?>
	
	<?php $post_slider = get_post_meta($post->ID, "r_post_slider", $single = true);
	   if($post_slider !== '') { ?>

<?php wp_enqueue_script('lightSlider', BASE_URL . 'js/jquery.lightSlider.js', false, '', true); ?>


<script type="text/javascript">   
jQuery(document).ready(function($){  	
$('#lightSlider').lightSlider({
    gallery:true,
    minSlide:1,
    maxSlide:1,
    auto:true,
    mode:"fade"
    });
});	
</script>	

    <ul id="lightSlider" class="gallery_light">

	    <?php 
        $slider_image_1 = get_post_meta($post->ID, 'r_slider_image_1', true);
		$slider_image_2 = get_post_meta($post->ID, 'r_slider_image_2', true);
		$slider_image_3 = get_post_meta($post->ID, 'r_slider_image_3', true);
		$slider_image_4 = get_post_meta($post->ID, 'r_slider_image_4', true);
		$slider_image_5 = get_post_meta($post->ID, 'r_slider_image_5', true);
		$slider_image_6 = get_post_meta($post->ID, 'r_slider_image_6', true);		
		$slider_image_7 = get_post_meta($post->ID, 'r_slider_image_7', true);
		$slider_image_8 = get_post_meta($post->ID, 'r_slider_image_8', true);
		$slider_image_9 = get_post_meta($post->ID, 'r_slider_image_9', true);
		$slider_image_10 = get_post_meta($post->ID, 'r_slider_image_10', true);	
        $slider_image_11 = get_post_meta($post->ID, 'r_slider_image_11', true);
		$slider_image_12 = get_post_meta($post->ID, 'r_slider_image_12', true);
		$slider_image_13 = get_post_meta($post->ID, 'r_slider_image_13', true);
		$slider_image_14 = get_post_meta($post->ID, 'r_slider_image_14', true);
		$slider_image_15 = get_post_meta($post->ID, 'r_slider_image_15', true);		
		
		$title_slider_image_1 = get_post_meta($post->ID, 'r_title_slider_image_1', true);	
		$title_slider_image_2 = get_post_meta($post->ID, 'r_title_slider_image_2', true);
		$title_slider_image_3 = get_post_meta($post->ID, 'r_title_slider_image_3', true);
		$title_slider_image_4 = get_post_meta($post->ID, 'r_title_slider_image_4', true);
		$title_slider_image_5 = get_post_meta($post->ID, 'r_title_slider_image_5', true);
		$title_slider_image_6 = get_post_meta($post->ID, 'r_title_slider_image_6', true);
		$title_slider_image_7 = get_post_meta($post->ID, 'r_title_slider_image_7', true);
		$title_slider_image_8 = get_post_meta($post->ID, 'r_title_slider_image_8', true);
		$title_slider_image_9 = get_post_meta($post->ID, 'r_title_slider_image_9', true);
		$title_slider_image_10 = get_post_meta($post->ID, 'r_title_slider_image_10', true);
		$title_slider_image_11 = get_post_meta($post->ID, 'r_title_slider_image_11', true);	
		$title_slider_image_12 = get_post_meta($post->ID, 'r_title_slider_image_12', true);
		$title_slider_image_13 = get_post_meta($post->ID, 'r_title_slider_image_13', true);
		$title_slider_image_14 = get_post_meta($post->ID, 'r_title_slider_image_14', true);
		$title_slider_image_15 = get_post_meta($post->ID, 'r_title_slider_image_15', true);
        ?>
		
		<?php if($slider_image_1 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_1, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_1, 1140, 650, true ); ?>
        <img src="<?php echo $image ?>"/>
		<div class="lightslider_caption"><?php echo $title_slider_image_1 ?></div>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_2 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_2, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_2, 1140, 650, true ); ?>
        <img src="<?php echo $image ?>"/>
		<div class="lightslider_caption"><?php echo $title_slider_image_2 ?></div>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_3 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_3, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_3, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_3 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_4 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_4, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_4, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_4 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_5 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_5, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_5, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_5 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_6 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_6, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_6, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_6 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>

		<?php if($slider_image_7 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_7, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_7, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_7 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>

		<?php if($slider_image_8 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_8, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_8, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_8 ?></div>	
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>

		<?php if($slider_image_9 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_9, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_9, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_9 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>

		<?php if($slider_image_10 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_10, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_10, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_10 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>		
	
	
		<?php if($slider_image_11 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_11, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_11, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_11 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_12 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_12, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_12, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_12 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_13 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_13, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_13, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_13 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_14 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_14, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_14, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_14 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>
		
		<?php if($slider_image_15 !== '') { ?>
	    <li data-thumb="<?php echo $image = aq_resize( $slider_image_15, 100, 70, true ); ?>">
	    <?php $image = aq_resize( $slider_image_15, 1140, 650, true ); ?>
		<div class="lightslider_caption"><?php echo $title_slider_image_15 ?></div>
        <img src="<?php echo $image ?>"/>
	    </li>	
        <?php } ?>

    </ul>
   
	<div class="clear"></div>
	<?php } else { } ?>

	<?php $post_thumb_var = get_post_meta($post->ID, "r_post_thumb_var", true); ?>
	<?php if($post_thumb_var == '' || $post_thumb_var == 'Standard' || $post_thumb_var == 'Small' || $post_thumb_var == 'Full width Carousel' || $post_thumb_var == 'Video - Full width') { ?>
	
	<div class="single_title">	  
	   <h1><?php the_title(); ?></h1> 
    </div>
	
	<div class="clear"></div>
	
    <?php if($post_thumb_var == 'Video - Full width') { } else { ?>
	
	<?php if (get_option('op_single_meta_line') == 'on') { ?>	
	
	<div class="post_meta_line">
	
        <?php if (get_option('op_single_time') == 'on') { ?>
		<div class="home_posts_time"><?php the_time('j M, Y'); ?></div>	
		<?php } ?>
		
        <?php if (get_option('op_single_author') == 'on') { ?>
		<?php $post_author = '<a target="_blank" href=" '.get_author_posts_url( get_the_author_meta( 'ID' ) ) .'">'.get_the_author_link().'</a>'; ?>
		<?php $post_avatar = get_avatar( get_the_author_id(), 12); ?>
		<div class="short_post_author"><?php echo $post_avatar; ?> <?php echo $post_author; ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_categories') == 'on') { ?>
	    <?php $categories_string = '';
	    foreach((get_the_category()) as $category) {
        $category_term = $category->category_nicename . '';
	    $categories_string .= '<a class="custom_cat_class '. $category_term .'" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "%s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	    } 
	    $categories_string = trim($categories_string); 
	    ?>
        <div class="home_posts_cats_box"><?php echo $categories_string ?></div>
		<?php } ?>
		
		
        <?php if (get_option('op_single_post_comments') == 'on') { ?>
		<?php $comments_count = get_comments_number(); ?>
		<?php $comments_text = get_option('op_comments_text'); ?>
		<div class="comments_count_simple_box"><?php echo $comments_count ?></div>
		<?php } ?>
		
        <?php if (get_option('op_single_likes') == 'on') { ?>
		<?php echo getPostLikeLink(get_the_ID()); ?>
		<?php } ?>
	
    </div>

	<?php } ?>
	<?php } ?>
	
	<?php } ?>
	
    <?php if (get_option('op_custom_single_text') == 'on') { ?>
	<?php $post_text = get_post_meta($post->ID, 'r_post_text', true); ?>
	<?php if($post_text !== '') { ?>
	<div class="pre_single_text">
	<?php echo $post_text; ?>
    </div>  
	<?php } ?>
	<?php } ?>
	

    <div class="single_text">
	
	<?php $post_thumb_var = get_post_meta($post->ID, "r_post_thumb_var", true); ?>
	<?php if($post_thumb_var == 'Small') { ?>
	
	   <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnails', false, '' ); $thumbnailSrc = $src[0]; ?>	

	   <?php if(has_post_thumbnail()) { ?>

	    <?php $post_thumbnail = get_post_meta($post->ID, "r_post_thumbnail", $single = true);
	    if($post_thumbnail !== 'on') { ?>
		
	    <div class="single_thumbnail_small">
		<a href="<?php echo $thumbnailSrc ?>" title="<?php the_title(); ?>" rel="prettyphoto">
	    <?php $image = aq_resize( $thumbnailSrc, 250, 'auto', false); ?>
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
	    </a>

        <?php if (get_option('op_similar') == 'on') { ?>
 
	    <?php
        $tags = wp_get_post_tags($post->ID);
        if ($tags) {
        $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

        $args=array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'showposts'=>5,
        'caller_get_posts'=>1
        );
        $my_query = new wp_query($args);
        if( $my_query->have_posts() ) {
        echo '<ul>';
        while ($my_query->have_posts()) {
        $my_query->the_post();
        ?>
		
		<li>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title(); ?></a>
	    </li>
		
        <?php } echo '</ul>'; } } ?>
        <?php wp_reset_query(); ?>
		 
	    <?php } ?>	

		</div>
	    <?php } ?>
	   
	    <?php } else {} ?>
		
	<?php } ?>
	
	<?php the_content(''); ?>
	<?php custom_wp_link_pages(); ?>	
    </div>

	<div class="clear"></div>
	
	<?php if (get_option('op_tags') == 'on') { ?>
	<?php the_tags('<ul id="tags_simple"><div class="tags_simple_text">'.get_option('op_tags_simple_text').'</div><li>','</li><li>','</li></ul>'); ?>
    <?php } ?>
	<div class="clear"></div>
	
	
	
<?php if (get_option('op_author_box') == 'on') { ?>
<div id="author_box">

<?php echo get_avatar( get_the_author_meta( 'user_email' ), 70 ); ?>
<div class="authorinfo">
<h4><a title="<?php echo get_option("op_view_athor_posts"); ?> <?php echo get_the_author(); ?>" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author(); ?></a></h4>

<ul class="author_icons">
    <?php                               
    $google_profile = get_the_author_meta( 'google_profile' );
    if ( $google_profile && $google_profile != '' ) {
        echo '<li class="google_icon"><a title="Google" class="tip" target="_blank" href="' . esc_url($google_profile) . '" rel="author"></a></li>';
    }
                                               
    $twitter_profile = get_the_author_meta( 'twitter_profile' );
    if ( $twitter_profile && $twitter_profile != '' ) {
        echo '<li class="twitter_icon"><a title="Twitter" class="tip" target="_blank" href="' . esc_url($twitter_profile) . '"></a></li>';
    }
                                               
    $facebook_profile = get_the_author_meta( 'facebook_profile' );
    if ( $facebook_profile && $facebook_profile != '' ) {
        echo '<li class="facebook_icon"><a title="Facebook" class="tip" target="_blank" href="' . esc_url($facebook_profile) . '"></a></li>';
    }
       
    $instagram_profile = get_the_author_meta( 'instagram_profile' );
    if ( $instagram_profile && $instagram_profile != '' ) {
        echo '<li class="instagram_icon"><a title="Instagram" class="tip" target="_blank" href="' . esc_url($instagram_profile) . '"></a></li>';
    }
	   
    $linkedin_profile = get_the_author_meta( 'linkedin_profile' );
    if ( $linkedin_profile && $linkedin_profile != '' ) {
        echo '<li class="linkedin_icon"><a title="Linkedin" class="tip" target="_blank" href="' . esc_url($linkedin_profile) . '"></a></li>';
    }
    ?>
</ul>
<div class="clear"></div>
<p><?php the_author_meta( 'description' ); ?></p>

</div>
		
</div>
<div class="clear"></div>
<?php } ?>	
	
	
<?php if (get_option('op_banner_single') == 'on') { ?>
<div id="banner_single_728">
<?php $index_banner = get_option("op_banner_single_code"); ?>
<?php echo stripslashes($index_banner); ?>
</div>
<div class="clear"></div>
<?php } ?>	
	
	
<?php if (get_option('op_nav_variant') == 'Sticky') { ?>	
	
<div class="nav_svg">
	<svg width="64" height="64" viewBox="0 0 64 64">
		<path id="arrow-left-1" d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z" />
	</svg>
	
	<svg width="64" height="64" viewBox="0 0 64 64">
		<path id="arrow-right-1" d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z" />
	</svg>
</div>
	
<div class="nav-slide">
	<?php $prev_post = get_previous_post(); if (!empty( $prev_post )): ?>
	<div class="sticky_post sticky_prev_post">
	<span class="icon-wrap"><svg class="icon" width="32" height="32" viewBox="0 0 64 64"><use xlink:href="#arrow-left-1"></svg></span>
	<div class="sticky_post_content">
		<h3><?php previous_post_link('%link'); ?></h3>
        <a href="<?php echo get_permalink( $prev_post->ID ); ?>">
	    <?php echo get_the_post_thumbnail($prev_post->ID, 'thumbnail', array(100,100)); ?>
	    </a>
	</div>
	</div>
	<?php endif; ?>
	
	<?php $next_post = get_next_post(); if (!empty( $next_post )): ?>
	<div class="sticky_post sticky_next_post">
	<span class="icon-wrap"><svg class="icon" width="32" height="32" viewBox="0 0 64 64"><use xlink:href="#arrow-right-1"></svg></span>
	<div class="sticky_post_content">
		<h3><?php next_post_link('%link'); ?></h3>
        <a href="<?php echo get_permalink( $next_post->ID ); ?>">
	    <?php echo get_the_post_thumbnail($next_post->ID, 'thumbnail', array(100,100)); ?>
	    </a>
	</div>
	</div>
    <?php endif; ?>
</div>
	
<?php } else { ?>	
	<div class="clear"></div>
	<div id="navigation_images">
	
	<div class="alignleft">
	<?php $prev_post = get_previous_post(); if (!empty( $prev_post )): ?>
	<div class="prev_link_title">
	<span><?php echo get_option('op_prev_post'); ?></span>
    <?php previous_post_link('%link', '%title &rarr;'); ?>
    </div>
	<?php endif; ?>
	</div> 
	
	<div class="alignright">
	<?php $next_post = get_next_post(); if (!empty( $next_post )): ?>
	<div class="next_link_title">
	<span><?php echo get_option('op_next_post'); ?></span>
	<?php next_post_link( '%link', '%title &rarr;' ); ?>
    </div>
	<?php endif; ?>
	</div>
	
	</div> 
	
	<div class="clear"></div>
<?php } ?>
	 
	
	<div class="clear"></div>

	<?php posts_nav_link(' &#183; ', 'previous page', 'next page'); ?>
	
	<?php if (get_option('op_single_comments') == 'on') { ?>
	
	<?php if (get_option('op_comments_variant') == 'Simple comments') { 
	comments_template('', true); } else { ?>
	
	<?php $discus = get_option('op_discus'); ?>
	
	<div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = '<?php echo $discus ?>'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = 'http://'+disqus_shortname+'.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	<?php } ?>
	
	<?php } ?>	
	
	<?php endwhile; ?>
	<?php else : ?>
	
	<?php endif; ?>	 

</div>
</div>	

<?php if (get_option('op_single_post_layout') == 'With 1 sidebar') { ?>
<?php if($post_layout == '' || $post_layout == 'Standard') { ?>
<?php get_sidebar('right'); ?>	
<?php } ?>	
<?php } ?>		

<div class="clear"></div>
	
<?php get_footer(); ?>


	