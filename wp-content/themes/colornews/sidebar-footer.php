
<div id="sidebar-footer">

    <?php if (get_option('op_banner_footer') == 'on') { ?>
		<div id="banner_footer_728">
		<?php $footer_banner = get_option("op_banner_footer_code"); ?>
		<?php echo stripslashes($footer_banner); ?>
		</div>
	<?php } ?>	

<?php wp_enqueue_script('gridalicious', BASE_URL . 'js/jquery.grid-a-licious.min.js', false, '', true); ?> 
<script type="text/javascript">
jQuery(document).ready(function($){  
$("#sidebar-footer").gridalicious({
selector: '.footer-widget',
width: 300,
gutter: 0,
animate: false
});
});
</script>	

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-sidebar') ) : ?>
<?php endif; ?> 	

</div>