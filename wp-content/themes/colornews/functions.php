<?php

define( 'BASE_URL', get_template_directory_uri() . '/' );

require_once(TEMPLATEPATH . '/options/options_theme.php'); 
require_once(TEMPLATEPATH . '/options/functions.php'); 	 
require_once(TEMPLATEPATH . '/includes/meta-box.php');
require_once(TEMPLATEPATH . '/includes/meta-box-single.php');
require_once(TEMPLATEPATH . '/options/seo.php'); 	
require_once(TEMPLATEPATH . '/includes/breadcrumbs.php');
require_once(TEMPLATEPATH . '/includes/contact_form.php');
require_once(TEMPLATEPATH . '/includes/aq_resizer.php');


/* ---------------Theme styles-------------- */


function theme_styles() {

wp_register_style( 'my-style', BASE_URL . 'style.css', null, false );
wp_register_style( 'shortcodes', BASE_URL . 'css/shortcodes.css', null, false );
wp_register_style( 'responsive', BASE_URL . 'css/responsive.css', null, false );
wp_enqueue_style('my-style');
wp_enqueue_style('shortcodes');
wp_enqueue_style('responsive');
		
}
add_action( 'wp_enqueue_scripts', 'theme_styles' ); 



/* ---------------Theme scripts-------------- */

function theme_scripts(){ 

if( !is_admin() ) {
wp_enqueue_script('jquery');
wp_enqueue_script('mosaicflow', BASE_URL . 'js/jquery.mosaicflow.min.js', false, '', true);
wp_enqueue_script('eqheight', BASE_URL . 'js/jquery.eqheight.js', false, '', true);
wp_enqueue_script('hoverIntent', BASE_URL . 'js/jquery.hoverIntent.minified.js', false, '', true);
wp_enqueue_script('easing', BASE_URL . 'js/jquery.easing.js', false, '', true);
wp_enqueue_script('prettyPhoto', BASE_URL . 'js/jquery.prettyPhoto.js', false, '', true);
wp_enqueue_script('tipTip', BASE_URL . 'js/jquery.tipTip.js', false, '', true);
wp_enqueue_script('modernizr_custom', BASE_URL . 'js/modernizr.custom.46884.js', false, '', true);
wp_enqueue_script('browser_selector', BASE_URL . 'js/css_browser_selector.js', false, '', true);
wp_enqueue_script('custom', BASE_URL . 'js/custom.js', false, '', true);
}

}
add_action('wp_enqueue_scripts', 'theme_scripts');


add_action( 'wp_print_scripts', 'enqueue_scripts_for_contact' );
function enqueue_scripts_for_contact() {
if( !is_page( 'Contact' ) ) 
return;
wp_enqueue_script('form', BASE_URL . 'js/jquery.form.js', false, '', true);
}

if ( is_singular() ) wp_enqueue_script( 'comment-reply' );	


register_sidebar(array('name'=>'Right sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div class="right-widget" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="right-heading"><h4>',
        'after_title' => '</h4><span></span></div> <div class="clear"></div>'
));	

register_sidebar(array('name'=>'Footer sidebar',
        'id' => 'footer-sidebar',
        'before_widget' => '<div class="footer-widget" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-heading"><h4>',
        'after_title' => '</h4></div>'
));	

		
add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');
add_theme_support( 'automatic-feed-links' );



function custom_paged_404_fix( ) {
	global $wp_query;
	if ( is_404() || !is_paged() || 0 != count( $wp_query->posts ) )
		return;
	$wp_query->set_404();
	status_header( 404 );
	nocache_headers();
}
add_action( 'wp', 'custom_paged_404_fix' );
	
$args = array(
	'default-color' => 'fff'
);
add_theme_support( 'custom-background', $args );	
	
add_theme_support( 'custom-header', $args  );	

function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

	
function pj_get_cm($key, $echo = FALSE) {
	global $post;
	$pj_custom_field = get_post_meta($post->ID, $key, true);
        if($echo == FALSE) return $pj_custom_field;
	echo $pj_custom_field;
}


	
function custom_wp_trim_excerpt($text) {
$raw_excerpt = $text;
if ( '' == $text ) {

    $text = get_the_content('');

    $text = strip_shortcodes( $text );
 
    $link_to_post = get_permalink();
 
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]&gt;', $text);
 
    $allowed_tags = '<p>,<a>,<em>'; 
    $text = strip_tags($text, $allowed_tags);

    $excerpt_word_count = 23; 

    $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
 
    $excerpt_end = ''; 
    $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);
 
    $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
    if ( count($words) > $excerpt_length ) {
        array_pop($words);
        $text = implode(' ', $words);
        $text = $text . $excerpt_more;
    } else {
        $text = implode(' ', $words);
    }
}
return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_wp_trim_excerpt');



function new_excerpt_more($more) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


add_theme_support(
	'post-formats', array(	
		'image',
		'video',
		'audio'
	)
);


if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
	add_theme_support('thumbnail');	
add_image_size( 'thumbnail', 420, 300, true );
}




if ( function_exists( 'wp_nav_menu' ) ){
	if (function_exists('add_theme_support')) {
		add_theme_support('nav-menus');
		add_action( 'init', 'register_my_menus' );
		function register_my_menus() {
			register_nav_menus(
				array(
					'main-menu' => __( 'Main menu', 'my-text-domain'),
					'top-menu' => __( 'Top menu', 'my-text-domain' )
				));
}}}

function primarymenu(){ ?>

<div id="mainMenu">
<ul>
<?php wp_list_pages('title_li='); ?>
</ul>
</div>

<?php }	

function footermenu(){ ?>

<div id="footerMenu">
<ul>
<?php wp_list_categories('hide_empty=1&exclude=1&title_li='); ?>
</ul>
</div>

<?php }	




function pd_title($amount,$echo=true) {
	$pd = get_the_title(); 
	if ( strlen($pd) <= $amount ) $echo_out = ' '; else $echo_out = ' ';
	$pd = mb_substr( $pd, 0, $amount, 'UTF-8' );
	if ($echo) {
		echo $pd;
		echo $echo_out;
	}
	else { return ($pdtitle . $echo_out); }
}




function custom_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}


function custom_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before' => '<div id="post_pagination_box"><p id="post-pagination">', 
		'after' => '</p>',
		'before_text' => '<p id="post-pagination-text">', 
		'after_text' => '</p></div>',
		
		'prev_text_before' => '<div class="post_pagination_prev">',
		'prev_text_after' => '</div>',
		
		'next_text_before' => '<div class="post_pagination_next">',
		'next_text_after' => '</div>',	
		
		'nextpagelink' => __( ''.get_option('op_read_next').'', 'my-text-domain' ),
		'previouspagelink' => __( ''.get_option('op_read_previous').'', 'my-text-domain' ),
		'pagelink' => '%',
		'echo' => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
	
			$output .= $before;
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				$output .= ' ';
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= _wp_link_page( $i );
				else
					$output .= '<span class="current-post-page">';

				$output .=  $j;
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= '</a>';
				else
					$output .= '</span>';
			}
			$output .= $after;

			if ( $more ) {
				$output .= $before_text;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $prev_text_before . $previouspagelink . $prev_text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $next_text_before . $nextpagelink . $next_text_after . '</a>';
				}
				$output .= $after_text;
			}

	}

	if ( $echo )
		echo $output;

	return $output;
}




add_filter( 'use_default_gallery_style', '__return_false' );
	
add_filter('get_comments_number', 'comment_count', 0);
function comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
return count($comments_by_type['comment']);
} else {
return $count;
}
}
	
function get_category_id($cat_name){
	$term = get_term_by('slug', $cat_name, 'category');
	return $term->term_id;
}


function pa_category_top_parent_id ($catid) {
 while ($catid) {
  $cat = get_category($catid);
  $catid = $cat->category_parent;
  $catParent = $cat->cat_ID;
 }
return $catParent;
}

function get_cat_slug($cat_id) {
	$cat_id = (int) $cat_id;
	$category = &get_category($cat_id);
	return $category->slug;
}


function category_id_class($classes) {
	global $post;
	foreach((get_the_category($post->ID)) as $category)
		$classes[] = $category->category_nicename;
	return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3;
	}
}

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9;' ), 9 );

function show_post($path) {
  $post = get_page_by_path($path);
  $content = apply_filters('the_content', $post->post_content);
  echo $content;
}


function add_to_author_profile( $contactmethods ) {
        $contactmethods['google_profile'] = 'Google Profile URL';
        $contactmethods['twitter_profile'] = 'Twitter Profile URL';
        $contactmethods['facebook_profile'] = 'Facebook Profile URL';
        $contactmethods['linkedin_profile'] = 'Linkedin Profile URL';
        $contactmethods['instagram_profile'] = 'Instagram Profile URL';
        return $contactmethods;
}
add_filter( 'user_contactmethods', 'add_to_author_profile', 10, 1);


function add_nofollow_cat( $text ) {
    $text = str_replace('rel="category"', '', $text); 
    $text = str_replace('rel="category tag"', 'rel="tag"', $text); 
    return $text;
}
add_filter( 'the_category', 'add_nofollow_cat' );

if ( ! isset( $content_width ) )
	$content_width = 710;

function mytheme_adjust_content_width() {
	global $content_width;

	if ( is_page_template( 'page_full_width.php' ) )
		$content_width = 1160;
}
add_action( 'template_redirect', 'mytheme_adjust_content_width' );


function mySearchFilter($query) {
if ($query->is_search) {
$query->set('post_type', array( 'post', 'revision', 'forum', 'topic', 'reply' ));
}
return $query;
}

add_filter('pre_get_posts','mySearchFilter');


add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action ( 'edit_category_form_fields', 'extra_category_fields');
function extra_category_fields( $tag ) {  
    $t_id = $tag->term_id;
    $cat_meta = get_option( "category_$t_id");
?>

<tr class="form-field">
<th scope="row" valign="top"><label for="extra6"><?php _e('Category image path', 'my-text-domain' ); ?></label></th>
<td>
<input type="text" name="Cat_meta[extra6]" id="Cat_meta[extra6]" size="25" style="width:60%;" value="<?php echo $cat_meta['extra6'] ? $cat_meta['extra6'] : ''; ?>"><br />
     <span class="description"><?php _e('Enter full image path to your image', 'my-text-domain'); ?></span>
</td>
</tr>

<tr class="form-field">
<th scope="row" valign="top"><label for="extra1"><?php _e('Featured posts - How many?', 'my-text-domain'); ?></label></th>
<td>
<select name="Cat_meta[extra1]" id="Cat_meta[extra1]" style="width:30%;" value="<?php echo $cat_meta['extra1'] ? $cat_meta['extra1'] : ''; ?>">
	<option value="">0</option>
	<option value="1" <?php echo ($cat_meta['extra1'] == "1") ? 'selected="selected"': ''; ?>>1</option>
	<option value="2" <?php echo ($cat_meta['extra1'] == "2") ? 'selected="selected"': ''; ?>>2</option>
	<option value="3" <?php echo ($cat_meta['extra1'] == "3") ? 'selected="selected"': ''; ?>>3</option>
</select>
    <span class="description"><?php _e('How many posts show in Featured area?', 'my-text-domain'); ?></span>
</td>
</tr>


<tr class="form-field">
<th scope="row" valign="top"><label for="extra5"><?php _e('Rating - Layout', 'my-text-domain'); ?></label></th>
<td>
<select name="Cat_meta[extra5]" id="Cat_meta[extra5]" style="width:30%;" value="<?php echo $cat_meta['extra5'] ? $cat_meta['extra5'] : ''; ?>">
	<option value="">Text</option>
	<option value="Stars" <?php echo ($cat_meta['extra5'] == "Stars") ? 'selected="selected"': ''; ?>>Stars</option>
	<option value="Points" <?php echo ($cat_meta['extra5'] == "Points") ? 'selected="selected"': ''; ?>>Points</option>
</select>
    <span class="description"><?php _e('Select layout for Rating', 'my-text-domain'); ?></span>
</td>
</tr>


<tr class="form-field">
<th scope="row" valign="top"><label for="extra2"><?php _e('Index page - Layout', 'my-text-domain'); ?></label></th>
<td>
<select name="Cat_meta[extra2]" id="Cat_meta[extra2]" style="width:30%;" value="<?php echo $cat_meta['extra2'] ? $cat_meta['extra2'] : ''; ?>">
	<option value="">With 1 Sidebar</option>
	<option value="hide_sidebar" <?php echo ($cat_meta['extra2'] == "hide_sidebar") ? 'selected="selected"': ''; ?>>Full width page</option>
</select>
    <span class="description"><?php _e('Select layout for index page', 'my-text-domain'); ?></span>
</td>
</tr>

<tr class="form-field">
<th scope="row" valign="top"><label for="extra4"><?php _e('Index posts - Columns', 'my-text-domain'); ?></label></th>
<td>
<select name="Cat_meta[extra4]" id="Cat_meta[extra4]" style="width:30%;" value="<?php echo $cat_meta['extra4'] ? $cat_meta['extra4'] : ''; ?>">
	<option value="">2 columns or 3 columns for Full width layout (default)</option>
	<option value="one_column" <?php echo ($cat_meta['extra4'] == "one_column") ? 'selected="selected"': ''; ?>>1 columns or 2 columns for Full width layout</option>
	<option value="one_line" <?php echo ($cat_meta['extra4'] == "one_line") ? 'selected="selected"': ''; ?>>Line posts (One row)</option>
</select>
    <span class="description"><?php _e('Select count of columns for index posts', 'my-text-domain'); ?></span>
</td>
</tr>

<tr class="form-field">
<th scope="row" valign="top"><label for="extra3"><?php _e('Index posts - Layout', 'my-text-domain'); ?></label></th>
<td>
<select name="Cat_meta[extra3]" id="Cat_meta[extra3]" style="width:30%;" value="<?php echo $cat_meta['extra3'] ? $cat_meta['extra3'] : ''; ?>">
	<option value="">Image - Title - Meta line - Text (default)</option>
	<option value="post_layout_itm" <?php echo ($cat_meta['extra3'] == "post_layout_itm") ? 'selected="selected"': ''; ?>>Image - Title - Meta line</option>
	<option value="post_layout_itt" <?php echo ($cat_meta['extra3'] == "post_layout_itt") ? 'selected="selected"': ''; ?>>Image - Title - Text</option>
	<option value="post_layout_it" <?php echo ($cat_meta['extra3'] == "post_layout_it") ? 'selected="selected"': ''; ?>>Image - Title</option>
</select>
    <span class="description"><?php _e('Select layout for index page - posts', 'my-text-domain'); ?></span>
</td>
</tr>

<tr class="form-field">
<th scope="row" valign="top"><label for="extra7"><?php _e('Index page - Banner', 'my-text-domain'); ?></label></th>
<td>
<select name="Cat_meta[extra7]" id="Cat_meta[extra7]" style="width:30%;" value="<?php echo $cat_meta['extra7'] ? $cat_meta['extra7'] : ''; ?>">
	<option value="">Display (default)</option>
	<option value="hide_banner" <?php echo ($cat_meta['extra7'] == "hide_banner") ? 'selected="selected"': ''; ?>>Hidden</option>
</select>
    <span class="description"><?php _e('Select featured variant for index page', 'my-text-domain'); ?></span>
</td>
</tr>

<?php
}

// save extra category extra fields hook
add_action ( 'edited_category', 'save_extra_category_fileds');
   // save extra category extra fields callback function
function save_extra_category_fileds( $term_id ) {
    if ( isset( $_POST['Cat_meta'] ) ) {
        $t_id = $term_id;
        $cat_meta = get_option( "category_$t_id");
        $cat_keys = array_keys($_POST['Cat_meta']);
            foreach ($cat_keys as $key){
            if (isset($_POST['Cat_meta'][$key])){
                $cat_meta[$key] = $_POST['Cat_meta'][$key];
            }
        }
        //save the option array
        update_option( "category_$t_id", $cat_meta );
    }
}


$timebeforerevote = 1;

add_action('wp_ajax_nopriv_post-like', 'post_like');
add_action('wp_ajax_post-like', 'post_like');

wp_enqueue_script('like_post', get_template_directory_uri().'/js/post-like.js', array('jquery'), '1.0', 1 );
wp_localize_script('like_post', 'ajax_var', array(
	'url' => admin_url('admin-ajax.php'),
	'nonce' => wp_create_nonce('ajax-nonce')
));

function post_like()
{
	$nonce = $_POST['nonce'];
 
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        die ( 'Busted!');
		
	if(isset($_POST['post_like']))
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$post_id = $_POST['post_id'];
		
		$meta_IP = get_post_meta($post_id, "voted_IP");

		$voted_IP = $meta_IP[0];
		if(!is_array($voted_IP))
			$voted_IP = array();
		
		$meta_count = get_post_meta($post_id, "votes_count", true);

		if(!hasAlreadyVoted($post_id))
		{
			$voted_IP[$ip] = time();

			update_post_meta($post_id, "voted_IP", $voted_IP);
			update_post_meta($post_id, "votes_count", ++$meta_count);
			
			echo $meta_count;
		}
		else
			echo "already";
	}
	exit;
}

function hasAlreadyVoted($post_id)
{
	global $timebeforerevote;

	$meta_IP = get_post_meta($post_id, "voted_IP");
	$voted_IP = $meta_IP[0];
	if(!is_array($voted_IP))
		$voted_IP = array();
	$ip = $_SERVER['REMOTE_ADDR'];
	
	if(in_array($ip, array_keys($voted_IP)))
	{
		$time = $voted_IP[$ip];
		$now = time();
		
		if(round(($now - $time) / 86400000) > $timebeforerevote)
			return false;
			
		return true;
	}
	
	return false;
}

function getPostLikeLink($post_id)
{

	$vote_count = get_post_meta($post_id, "votes_count", true);
    if (empty($vote_count)) { $vote_count = 0;}
	$output = '<div class="post-like">';
	if(hasAlreadyVoted($post_id))
		$output .= ' <span title="'.__('I like this article', $themename).'" class="tip like alreadyvoted"></span>';
	else
		$output .= '<a href="#" data-post_id="'.$post_id.'">
					<span title="'.__('I like this article', $themename).'" class="tip like"></span>
				</a>';
	$output .= '<span class="count">'.$vote_count.'</span></div>';
	
	return $output;
}

remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );

include(TEMPLATEPATH . '/includes/widgets/banners.php');
include(TEMPLATEPATH . '/includes/widgets/recent_posts.php');
include(TEMPLATEPATH . '/includes/widgets/recent_category_posts.php');
include(TEMPLATEPATH . '/includes/widgets/recent_category_posts_menu_tabs.php');
include(TEMPLATEPATH . '/includes/widgets/recent_category_posts_second.php');
include(TEMPLATEPATH . '/includes/widgets/recent_category_posts_menu.php');
include(TEMPLATEPATH . '/includes/widgets/recent_category_carousel.php');
include(TEMPLATEPATH . '/includes/widgets/recent_category_slider.php');
include(TEMPLATEPATH . '/includes/widgets/full_width_posts.php');
include(TEMPLATEPATH . '/includes/widgets/popular_posts.php');
include(TEMPLATEPATH . '/includes/widgets/flickr.php');
include(TEMPLATEPATH . '/includes/widgets/video.php');

?>
