
</div>
<div class="clear"></div>

<?php if (get_option('op_footer_popular_posts') == 'on') { ?>
<?php get_template_part('includes/footer_popular_posts'); ?>
<?php } ?>	

<?php if (get_option('op_footer_sidebar') == 'on') { ?>

<div id="footer_box"> 
    <div class="inner">	
    <?php if (get_option('op_footer_top_sidebar') == 'on') { ?>
    <?php get_sidebar('footer-top'); ?>
    <?php } ?>

    <?php get_sidebar('footer'); ?>
    </div>
</div>

<?php } else { ?>
<br />
<?php } ?>
<div class="clear"></div>
<div id="footer_bottom"> 

    <div id="credit">     
	  &copy; <?php $footer_copy = get_option("op_footer_copy"); ?>
	  <?php echo stripslashes($footer_copy); ?> <?php echo date('Y'); ?> -
	  <a href="<?php echo home_url() ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a> | Diseño Paulo Muñoz -  
      <a href="http://uzweb.cl" title="UltimaZeta">  Estructura UltimaZeta. </a>.
	  <?php if (get_option('op_designed_by') == 'on') { ?>
	  &nbsp;  Designed <a href="http://themeforest.net/user/RoyalwpThemes/portfolio?ref=RoyalwpThemes" target="blank"> by RoyalwpThemes</a>
	  <?php } ?>
    </div>	


</div>

<?php if (get_option('op_fixed_sidebars') == 'on') { ?>

<?php wp_enqueue_script('sticky_sidebar', BASE_URL . 'js/theia-sticky-sidebar.js', false, '', true); ?>
<script type="text/javascript">
jQuery(document).ready(function($){
$('#home_content, #sidebar-right, #content, #single_content').theiaStickySidebar({
	additionalMarginTop: 20,
	additionalMarginBottom: 20
});
});
</script>

<?php } ?>


<?php if (get_option('op_menu_disable') == 'false') { ?>

<?php wp_enqueue_script('ddsmoothmenu', BASE_URL . 'js/ddsmoothmenu.js', false, '', true); ?>
<script type="text/javascript">
jQuery(document).ready(function($){
ddsmoothmenu.init({
mainmenuid: "mainMenu", 
orientation: 'h',
classname: 'ddsmoothmenu', 
contentsource: "markup"
});

$("<select />").appendTo("#mainMenu");

$("<option />", {
   "selected": "selected",
   "value"   : "",
   "text"    : "Go to..."
}).appendTo("#mainMenu select");

$("#mainMenu a").each(function() {
 var el = $(this);
 $("<option />", {
     "value"   : el.attr("href"),
     "text"    : el.text()
 }).appendTo("#mainMenu select");
});

$("#mainMenu select").change(function() {
  window.location = $(this).find("option:selected").val();
});
});
</script>

<?php } ?>

<script type="text/javascript">
jQuery(document).ready(function($){

	var $ele = $('#oz-scroll');
    $(window).scroll(function() {
        $(this).scrollTop() >= 200 ? $ele.show(10).animate({ right : '15px' }, 10) : $ele.animate({ right : '-80px' }, 10);
    });
    $ele.click(function(e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 600);
    });
});
</script>  

<a id="oz-scroll" class="style1 tip" title="Go top" href="#"></a>

</div>

<?php if (get_option('op_author_enable') == 'false') { ?>
<style type="text/css">
.home_post_author, .short_post_author, .cat_author, .widget_two_time span, .widget_date_third span, .widget_four_time span, .full_widget_date span {display: none!important;}
</style>
<?php } ?>

<?php if (get_option("op_custom_css") !== "") { ?>
<style type="text/css">
<?php $custom_css = get_option("op_custom_css"); ?>
<?php echo stripslashes($custom_css); ?>
</style>
<?php echo get_option("op_ga_code"); ?>
<?php } ?>

<?php wp_footer(); ?>
</body>
</html>


