
<?php
/*
Template Name: Home page with Carousel
*/
?>

<?php get_header(); ?>

<div id="main_content" class="home_page"> 

<?php $home_sidebars_count = get_post_meta($post->ID, "r_home_sidebars_count", $single = true); ?>

<?php if (get_option('op_featured_area') == 'on') { ?>
<?php $r_home_slider_layout = get_post_meta($post->ID, "r_home_slider_layout", $single = true); ?>

<?php if($r_home_slider_layout == 'Full width') { ?>
<?php get_template_part('includes/home_big_carousel'); ?>
<?php } ?>

<?php if($r_home_slider_layout == 'Boxed width') { ?>
<div class="inner">
<?php get_template_part('includes/home_big_carousel'); ?>
</div>
<?php } ?>
<?php } ?>


<div class="inner">

<div id="home_content" class="<?php if($home_sidebars_count == 'With 1 sidebar') { ?>content_with_one_sidebar<?php } ?> <?php if($home_sidebars_count == 'Full width') { ?>content_full_width<?php } ?>">	

<div id="home_content_inner">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php the_content(''); ?>

<?php endwhile; ?>
<?php endif; ?> 	
</div>
	
</div>

<?php if($home_sidebars_count == 'With 1 sidebar') { ?>
<?php get_sidebar('right'); ?>	
<?php } ?>	

</div>	
</div>	
<div class="clear"></div>
	
<?php get_footer(); ?>


